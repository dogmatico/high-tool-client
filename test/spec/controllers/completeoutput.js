'use strict';

describe('Controller: CompleteoutputCtrl', function () {

  // load the controller's module
  beforeEach(module('highToolClientApp'));

  var CompleteoutputCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CompleteoutputCtrl = $controller('CompleteoutputCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
