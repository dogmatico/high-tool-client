'use strict';

describe('Service: APIURL', function () {

  // load the service's module
  beforeEach(module('highToolClientApp'));

  // instantiate service
  var APIURL;
  beforeEach(inject(function (_APIURL_) {
    APIURL = _APIURL_;
  }));

  it('should be an absolute URL', function () {
    expect(APIURL).toMatch(/^https{0,1}:\/\//);
  });

});
