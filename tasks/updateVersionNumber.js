(function () {
  const fs = require('fs');
  const fileToModify = 'app/scripts/services/versionNumber.js';

  fs.readFile('package.json', 'utf8', (err, packajeJSON) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    const packageInfo = JSON.parse(packajeJSON);

    fs.readFile(fileToModify, 'utf8', (err, jsFile) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }

      const newData = jsFile.replace(/\.constant\(\'VERSION_NUMBER\', \'[a-zA-Z0-9_\.]*\'\)/, `.constant('VERSION_NUMBER', '${packageInfo.version}')`);

      fs.writeFile(fileToModify, newData, 'utf8', err => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
        process.exit(0);
      });
    });
  });
}());
