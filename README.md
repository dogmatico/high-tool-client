# HIGH-TOOL Client
This is the Web front-end of the HIGH-TOOL Interface. It's designed to consume 
the REST API of the [HIGH-TOOL Back-end](https://bitbucket.org/dogmatico/high-tool-server)

This Front-end uses Angular 1.3 and is fully static. Hence, once compiled can be served 
by any Web server.

The scafolding of this project was generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Installation
Clone this repository using git and run

```bash
npm install
```

```bash
bower install
```

It may be helpful to also install grunt-cli as a global package:

```bash
npm install grunt-cli -g
```

## Configuration
Before building the app, you need to set the API URL editing the file
*app/scripts/services/api_url.js*:

```JavaScript
    angular.module('highToolClientApp')
      .constant('APIURL', 'https://hightool.mcrit.com');
```

And run the script to update the templates with the version number:

```bash
npm run update-config
```

## Build & development

Run `grunt` or `grunt build` for building and `grunt serve` for preview.

The build will be emited to the *dist/* folder. Just copy it to the Web server.

If the build fails due to missing utilities like *imagemin*, use the flag *--force*

```bash
grunt build --force
```

## Testing

Running `grunt test` will run the unit tests with karma.

## Web server commentaries

If the back and the front-end reside in the same URL, it could be necessary to 
proxy the calls to the back-end. In particular, the back-end expects calls to 

* */api*.
* */login*.
* */logout*.
* Web Sockets to root location.

Also, the interface uses JSON Web Tokens to authenticate. Therefore, using 
HTTPS and WSS protocols is a must.