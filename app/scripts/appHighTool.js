/*jslint browser: true, indent: 2*/
/*global angular, tinyMCE*/
(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name highToolClientApp
   * @description
   * # highToolClientApp
   *
   * Main module of the application.
   */

  angular
    .module('highToolClientApp', [
      'libraries',
      'ui.router',
      'ui.bootstrap',
      'ui.tinymce',
      'ngSanitize'
    ])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      tinyMCE.baseURL = 'assets/tinymce';
      tinyMCE.suffix = '.min';

      $urlRouterProvider
        .when('/package/edit/policy', 'package/edit/policy/new')
        .when('/package/edit/policy/', 'package/edit/policy/new')
        .when('/package/edit/framework', 'package/edit/policy/framework')
        .when('/package/edit/framework/', 'package/edit/policy/framework')
        .otherwise('/');

      $stateProvider
        .state('login', {
          url: '/login',
          views: {
            'menu': {
              templateUrl: '/views/partials/topMenu.tmpl.html'
            },
            'main': {
              templateUrl: '/views/loginForm.html',
              controller: 'loginCtrl as login'
            }
          }
        })
        .state('authenticated', {
          abstract: true,
          views: {
            'menu': {
              templateUrl: '/views/partials/topMenu.tmpl.html'
            },
            'main': {
              template: '<ui-view/>'
            }
          }
        })
        .state('authenticated.main', {
          url: '/',
          templateUrl: '/views/about.html'
        })
        .state('authenticated.export', {
          abstract: true,
          url: '/export',
          template: '<ui-view/>'
        })
        .state('authenticated.export.tables', {
          url: '/tables',
          templateUrl: '/views/exportTables.html',
          controller: 'ExportTablesCtrl as exportTables'
        })
        .state('authenticated.export.tablesRun', {
          url: '/tables/:runId',
          templateUrl: '/views/exportTables.html',
          controller: 'ExportTablesCtrl as exportTables'
        })
        .state('authenticated.settings', {
          abstract: true,
          url: '/settings',
          template: '<ui-view/>'
        })
        .state('authenticated.settings.database', {
          url: '/database',
          templateUrl: '/views/databaseAdmin.html',
          controller: 'DBAdministrationCtrl as adminDatabase'
        })
        .state('authenticated.settings.owndata', {
          url: '/owndata',
          templateUrl: '/views/ownDataEdit.tmpl.html',
          controller: 'ownDataCtrl as od'
        })
        .state('authenticated.settings.modules', {
          url: '/modules',
          templateUrl: '/views/modules.tmpl.html',
          controller: 'ModulesCtrl as modulesCtrl'
        })
        .state('authenticated.admin', {
          abstract: true,
          url: '/admin',
          template: '<ui-view/>'
        })
        .state('authenticated.admin.users', {
          url: '/users',
          templateUrl: '/views/partials/adminUsers.tmpl.html',
          controller: 'usersAdministrationCtrl as adminUser'
        })
        .state('authenticated.admin.servers', {
          url: '/servers',
          templateUrl: '/views/adminServers.tmpl.html',
          controller: 'MasterServersCtrl as adminServers'
        })
        .state('authenticated.instructions', {
          url: '/instructions',
          templateUrl: '/views/instructions.html',
        })
        .state('authenticated.modelRuns', {
          url: '/runs',
          templateUrl: '/views/viewModelRuns.html',
          controller: 'ModelRunsCtrl as modelruns'
        })
        .state('authenticated.output', {
          url: '/output/:runId',
          templateUrl: '/views/viewCompleteOutput.html',
          controller: 'CompleteOutputCtrl as output',
          resolve : {
            outputParameters : ['$stateParams', 'PackageService', function ($stateParams, PackageService) {
              return PackageService.getRun($stateParams.runId);
            }],
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        })
        .state('authenticated.viewReference', {
          url: '/view-reference',
          templateUrl: '/views/viewReferenceScenario.html',
          controller: 'ViewReferenceCtrl as reference'
        })
        .state('authenticated.packageEditor', {
          url: '/package/edit',
          abstract: true,
          template: '<ui-view/>'
        })
        .state('authenticated.packageEditor.custom', {
          url: '/policy/custom/:packageId',
          templateUrl: '/views/customTPMEditor.tmp.html',
          controller: 'customPackageCtrl as editor',
          resolve : {
            schema : ['packageService', function (packageService) {
              return packageService.getSchema('custom');
            }],
            packageData : ['$stateParams', 'packageService', function ($stateParams, packageService) {
              return packageService.fetchPackageData($stateParams.packageId, 'CUSTOMTPMROOT');
            }],
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        })
        .state('authenticated.packageEditor.single_tpm', {
          url: '/policy/singleTPM/edit/:packageId',
          templateUrl: '/views/editSingleTPM.html',
          controller: 'editSingleTPMCtrl as editSingleTPM',
          resolve : {
            schema : ['packageService', function (packageService) {
              return packageService.getSchema('TPMRun');
            }],
            packageData : ['$stateParams', 'packageService', function ($stateParams, packageService) {
              return packageService.fetchPackageData($stateParams.packageId, 'TPMROOT');
            }],
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        })
        .state('authenticated.packageEditor.combined', {
          url: '/policy/combined/:packageId',
          templateUrl: '/views/combinedTPMEditor.html',
          controller: 'CombinedTPMCtrl as combinedTPM',
          resolve : {
            schema : ['CombinedTPMService', function (CombinedTPMService) {
              return CombinedTPMService.schema;
            }],
            leverList : ['CombinedTPMService', function (CombinedTPMService) {
              return CombinedTPMService.leverList;
            }],
            combinations : ['CombinedTPMService', function (CombinedTPMService) {
              return CombinedTPMService.combinations;
            }],
            packageData : ['$stateParams', 'PackageService', function ($stateParams, PackageService) {
              if ($stateParams.packageId === 'new') {
                return {};
              }
              return PackageService.getPackage($stateParams.packageId, true);
            }],
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        })
        .state('authenticated.packageEditor.defineSingleTPM', {
          url: '/policy/singleTPM/new',
          templateUrl: '/views/singleTPMOptions.html',
          controller: 'singleTPMOption as defineSingleTPM',
          resolve : {
            schema : ['packageService', function (packageService) {
              return packageService.getSchema('TPMRun');
            }],
            packageData : ['packageService', function (packageService) {
              return packageService.fetchPackageData('new', 'TPMROOT');
            }],
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        })
        .state('authenticated.run', {
          url: '/run',
          abstract: true,
          template: '<ui-view/>'
        })
        .state('authenticated.run.expertMode', {
          url: '/expertMode/:runId',
          templateUrl: '/views/expertMode.html',
          controller: 'ExpertModeCtrl as expertMode',
          resolve : {
            runInfo : ['$q', '$http', 'APIURL', '$stateParams', function ($q, $http, APIURL, $stateParams) {
              var Promise = Promise || $q;

              return new Promise(function (resolve, reject) {
                $http.get(APIURL + '/api/run/definition/' + $stateParams.runId)
                  .then(function (response) {
                    resolve(response.data);
                  })
                  .catch(function (response) {
                    reject(response.data);
                  });
              });
            }]
          }
        })
        .state('authenticated.transvisions', {
          url: '/transvisions/:packageId',
          templateUrl: '/views/viewTransvisions.html',
          controller: 'transvisionsCtrl as transvisions'
        });
    }])
    .config(['$httpProvider',
      function ($httpProvider) {
        $httpProvider.interceptors.push(['$q', '$location', '$window', 'userAuthFactory',
          function ($q, $location, $window, userAuthFactory) {
            return {
              request: function (config) {
                var token = $window.localStorage.getItem('JWT');
                config.headers = config.headers || {};
                if (token && userAuthFactory.isTokenInTime()) {
                  config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
              },
              responseError: function (response) {
                if (response.status === 401 || response.status === 403) {
                  $location.path('/login');
                }
                return $q.reject(response);
              }
            };
          }
          ]);
      }]
      )
    .run(['$rootScope', '$state', 'userAuthFactory', 'WebSocketService', 'NotificationAPIService', function ($rootScope, $state, userAuthFactory, WebSocketService, NotificationAPIService) {
      $rootScope.$on('authLogout', function () {
        $state.go('login');
      });

      if (!userAuthFactory.isAuthenticated()) {
        WebSocketService.login();
      }

      NotificationAPIService.requestPermission().then(function (permission) {
        if (permission === 'granted') {
          WebSocketService.socket.on('datastock-update', function (msg) {
            NotificationAPIService.sendNotification('Progress in model runs', msg.message);
          });

          WebSocketService.socket.on('module-update', function (msg) {
            NotificationAPIService.sendNotification('Module updated', msg.message);
          });
        }
      });

      $rootScope.$on('$stateChangeStart', function (event, toState) {
        if (toState.name !== 'login' && !userAuthFactory.isAuthenticated()) {
          event.preventDefault();
          $state.go('login');
        }
      });
    }]);
}());
