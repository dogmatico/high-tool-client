/*jslint browser: true, indent: 2, nomen: true*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc overview
   * @name vendorLibraries
   * @description
   * Create a module to inject 3rd party vendor
   * libraries as services
   *
   * Main module of the application.
   */

  var libraries = ['d3'];

  angular
    .module('libraries', [])
    .run(['$window', '$log', function ($window, $log) {
      libraries.forEach(function (library) {
        $window._libraries =  $window._libraries || {};
        if (!$window[library]) {
          $log.error(library + ' not found.');
        } else {
          $window._libraries[library] = $window[library];
        }
      });
    }]);

  libraries.forEach(function (library) {
    angular.module('libraries')
      .factory(library, ['$window', function ($window) {
        return $window._libraries[library];
      }]);
  });
}());
