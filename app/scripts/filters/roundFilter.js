/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.APIURL
   * @description
   * # APIURL
   * Constant in the highToolClientApp.
   */
  angular.module('highToolClientApp')
    .filter('roundNumber', ['$filter', function($filter) {
      return function(input) {
        return ((input === 0 || !!input) ? $filter('number')(Math.round(input * 1000) / 1000) : '');
      };
    }]);
}());
