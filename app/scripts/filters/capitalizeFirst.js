/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.APIURL
   * @description
   * # APIURL
   * Constant in the highToolClientApp.
   */
  angular.module('highToolClientApp')
    .filter('capitalizeFirst', function() {
      return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1) : '';
      };
    });
}());
