/*jslint browser: true, indent: 2*/
/*global angular, topojson*/
(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name highToolClientApp.directive:mapEditor
   * @description
   * # mapEditor
   */


  function AffineTransformation(dataTrie, imageBound) {
    var minMax = dataTrie.keys
      .filter(function (code) {
        // Remove NUTS3 from list.
        return (code.length <= 4);
      })
      .reduce(function (acum, curr) {
        var currentValue = dataTrie.get(curr),
          minVal = dataTrie.get(acum[0]),
          maxVal = dataTrie.get(acum[1]);

        if (minVal > currentValue) {
          acum[0] = curr;
        }

        if (maxVal < currentValue) {
          acum[1] = curr;
        }

        return acum;
      }, ['EU28', 'EU28'])
      .map(function (key) {
        return dataTrie.get(key);
      });

    var retObj = {
      params : [minMax[0], minMax[1], imageBound[0], imageBound[1]]
    };

    if (Math.abs(minMax[0] - minMax[1]) < 1e-4) {
      retObj.compute = function (value) {
        return value;
      };
    } else {
      retObj.compute = function (value) {
        return imageBound[0] + (value - minMax[0])*(imageBound[1] - imageBound[0])/(minMax[1] - minMax[0]);
      };
    }
    return retObj;
  }

  function appendStripeMask (svgEle, index) {
    var xmlns = 'http://www.w3.org/2000/svg';
    var defs = document.createElementNS(xmlns, 'defs');

    var pattern = document.createElementNS(xmlns, 'pattern');
    [
      ['id', 'stripe' + index],
      ['patternUnits', 'userSpaceOnUse'],
      ['patternTransform', 'rotate(45)'],
      ['width', '4'],
      ['height', '4']
    ].forEach(function (attr) {
      pattern.setAttributeNS(null, attr[0], attr[1]);
    });
    var rect = document.createElementNS(xmlns, 'rect');
    [
      ['transform', 'translate(0,0)'],
      ['fill', 'white'],
      ['width', '3'],
      ['height', '4']
    ].forEach(function (attr) {
      rect.setAttributeNS(null, attr[0], attr[1]);
    });
    pattern.appendChild(rect);


    var mask = document.createElementNS(xmlns, 'mask');
    mask.setAttributeNS(null, 'id', 'mask-stripe' + index);
    var rect2 = document.createElementNS(xmlns, 'rect');
    [
      ['x', '0'],
      ['y', '0'],
      ['height', '100%'],
      ['width', '100%'],
      ['style', 'fill: url(#stripe' + index + ')']
    ].forEach(function (attr) {
      rect2.setAttributeNS(null, attr[0], attr[1]);
    });
    mask.appendChild(rect2);

    defs.appendChild(pattern);
    defs.appendChild(mask);
    svgEle.appendChild(defs);
  }

  function controllerMapEditor($scope, $filter, $q, MapDataService, BaselineDataService, dataStructuresService, d3) {
    var Promise = Promise || $q;

    function renderedPromise() {
      return new Promise(function (resolve) {
        function check() {
          var containerUserMap = document.getElementById('containerUserMap');

          if (containerUserMap && containerUserMap.clientWidth !== 0) {
            resolve(containerUserMap.clientWidth);
          } else {
            setTimeout(check, 100);
          }
        }
        check();
      });
    }

    // Variables
    $scope.minValue = Math.round(($scope.schemaObject.target.__is_relative ? 100 : 1) * $scope.schemaObject.min * 10000) / 10000;
    $scope.maxValue = Math.round(($scope.schemaObject.target.__is_relative ? 100 : 1) * $scope.schemaObject.max * 10000) / 10000;
    $scope.stepSize = Math.round((100000 * ($scope.maxValue - $scope.minValue) / 40)) / 100000;
    $scope.allowedSpreads = [
      {id : 'baseline', name: 'Homogeneous across European countries'},
      {id : 'demo_r_d3dens', name: 'Proportional to population density'},
      {id : 'demo_r_d3dens_inv', name: 'Inverse proportional to population density'},
      {id : 'nama_r_e3gdp_PPS_HAB_EU', name: 'Proportional to GDP / Capita'},
      {id : 'nama_r_e3gdp_PPS_HAB_EU_inv', name: 'Inverse proportional to GDP / Capita'}
    ];
    $scope.centeredUser = null;
    $scope.editedValuesTrie = dataStructuresService.createTrie();
    $scope.spreadsById = {};

    $scope.nutsLevels = [{
        id : 0,
        label : 'nuts0'
      },
      {
        id : 1,
        label : 'nuts2'
      }
    ];
    $scope.zoomLevel = $scope.nutsLevels[0];



    $scope.region = null;
    $scope.selectedRegion = {
      name: 'EU28+2',
      id: 'EU28'
    };
    $scope.selectedSpread = $scope.allowedSpreads[0];
    $scope.valuesSelectedRegion = {
      user: $scope.terminalValue,
      baseline: $scope.terminalValue
    };

    // Managing default and hierachical time

    function compareTimeSeries(a, b) {
      if (!a || !b) {
        return false;
      }

      if (a.length !== b.length) {
        return false;
      }

      return a.every(function (item) {
        var distance = 0;
        for (var i = 0, compErr; i < 3; i += 1) {
          compErr = parseFloat(item[i], 10) - parseFloat(b[i], 10);
          distance += (compErr * compErr);
        }
        return (distance < 10e-6);
      });
    }

    $scope.editingCountryTime = false;
    function getFirstTimeSeries(regionId) {
      var foundNode = getFirstValueUpward(regionId, $scope.editedValuesTrie);
      while (!foundNode.timeSeries && regionId.length) {
        regionId = regionId.slice(0, -1);
        foundNode = getFirstValueUpward(regionId, $scope.editedValuesTrie);
      }

      if (!regionId.length) {
        return $scope.EU28Nodes.slice(0);
      }
      return foundNode.timeSeries.slice(0);
    }

    $scope.toggleEditCountryTime = function () {
      $scope.editingCountryTime = !$scope.editingCountryTime;
      if ($scope.editingCountryTime && $scope.selectedRegion.id !== 'EU28') {
        if (!$scope.editedValuesTrie.get($scope.selectedRegion.id)) {
          $scope.editedValuesTrie.add($scope.selectedRegion.id, {
            value : $scope.terminalValue
          });
        }

        if (!$scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries) {
          $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries = getFirstTimeSeries($scope.selectedRegion.id);
        }

        $scope.timeSeries = $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries.slice(0);
      }
    };

    $scope.setTrajectory = function () {
      if ($scope.selectedRegion.id !== 'EU28') {
        var parentTimeSeries = getFirstTimeSeries($scope.selectedRegion.id.slice(0, -1));
        if (!compareTimeSeries($scope.timeSeries, parentTimeSeries)) {
          var newValue = Math.round($scope.timeSeries[$scope.timeSeries.length - 1][1] * 1000) / 1000;
          updateColorEditedRegion(newValue);
          $scope.currentUserVal = newValue * $scope.factor;
          $scope.valuesSelectedRegion.user = newValue;
          $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries = $scope.timeSeries.slice(0);
        } else {
          delete $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries;
        }
      }
    };

    $scope.resetTrajectory = function () {
      if ($scope.selectedRegion.id !== 'EU28') {
        $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries = $scope.EU28Nodes.slice(0);
      }
      $scope.timeSeries = $scope.editedValuesTrie.get($scope.selectedRegion.id).timeSeries.slice(0);
    };


    $scope.zoomOut = function () {
      changeZoom(null);
      return;
    };

    var downloadStats = [],
      fullWidth = 579,
      height,
      maxY = $scope.schemaObject.max,
      minY = $scope.schemaObject.min,
      scaleFactor,
      svgUserDefined = [],
      toolTip = document.getElementById('tooltipD3Map'),
      width,
      color = d3.scale.linear()
                .domain([minY, maxY])
                .range(['#e6e6ff', '#000080']),
      projection,
      path;


    //Functions
    function changeZoom(d) {
      /*jshint validthis:true */
      var centroid, x, y, k, g, DOMParent;
      if (this) {
        DOMParent = this.parentNode;
      } else if (document.getElementById($scope.selectedRegion.id)) {
        DOMParent = document.getElementById($scope.selectedRegion.id).parentNode;
      } else {
        return;
      }
      g = d3.select(DOMParent);
      toolTip.style.display = 'none';
      if (d && $scope.centeredUser !== d) {
        centroid = path.centroid(d);
        x = centroid[0];
        y = centroid[1];
        k = 4;
        $scope.centeredUser = d;
        $scope.selectedRegion.name = d.properties.name;
        $scope.selectedRegion.id = d.id;
        $scope.valuesSelectedRegion.user = this.getAttribute('node-value');
        d3.select('projection-composition-borders')
          .attr('visibility', 'hidden');
      } else {
        x = width / 2;
        y = height / 2;
        k = 1;
        $scope.centeredUser = null;
        $scope.selectedRegion.name = 'EU28+2';
        $scope.selectedRegion.id = 'EU28';
        d3.select('projection-composition-borders')
          .attr('visibility', 'visible');
      }

      g.selectAll('path')
        .classed('active', $scope.centeredUser && function (d) { return d === $scope.centeredUser; });


      g.transition()
        .duration(500)
          .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')scale(' + k + ')translate(' + -x + ',' + -y + ')')
          .style('stroke-width', 1.5 / k + 'px');

      $scope.$apply();
    }

    function updateWidthDependentVariables(w) {
      width = w;
      height = Math.ceil(width * 400 / fullWidth);
      scaleFactor = Math.ceil(width * 600 / fullWidth);
      projection = d3.geo.conicConformalEurope()
        .scale(scaleFactor)
        .translate([0.8 * width, 1.9 * height / 4]);
      path = d3.geo.path().projection(projection);
      updateSVGContainerPadding(height);
      return;
    }

    function updateSVGContainerPadding(newHeight) {
      var svgContainerMaps = document.querySelectorAll('.svg-container-maps');
      for (var i = 0, ln = svgContainerMaps.length; i < ln; i += 1) {
        svgContainerMaps[i].style.paddingBottom = (newHeight - 15) + 'px';
      }
      return;
    }

    function drawLegendAndCanvas() {
      var destinationId = '#containerLegendMapEditor',
        i,
        numSquaresLegend = 8,
        widthLegend = 480,
        sideSquare = (widthLegend / numSquaresLegend) * 0.5,
        truncatedVal = (maxY - minY) / numSquaresLegend,
        squareLegendG,
        svgLegend = d3.select(destinationId)
          .append('svg')
            .attr('width', 270)
            .attr('height', 35);

      for (i = 0; i < 2; i += 1) {
        svgUserDefined[i] = d3.select('#containerUserZoom' + i).append('svg')
          .attr('preserveAspectRatio', 'xMinYMin meet')
          .attr('viewBox', '0 0 ' + width + ' ' + (height - 15))
          .classed('svg-content-responsive', true);



        appendStripeMask(svgUserDefined[i][0][0], i);
      }

      for (i = 0; i <= numSquaresLegend; i += 1) {
        squareLegendG = svgLegend
                          .append('g')
                            .attr('x',  2 * i * sideSquare)
                            .attr('width', sideSquare)
                            .attr('transform', 'translate(' + (i * sideSquare) + ', 0)')
                            .attr('overflow', 'visible');

        squareLegendG
          .append('rect')
            .attr('width', sideSquare)
            .attr('height', sideSquare / 2)
            .attr('fill', color(minY + i * truncatedVal));
        if (i % 2 === 0) {
          squareLegendG
            .append('text')
              .text($filter('roundNumber')($scope.factor * (minY + i * truncatedVal)))
              .attr('x', sideSquare / 2)
              .attr('y', sideSquare / 2 + 15)
              .attr('font-family', 'sans-serif')
              .attr('font-size', '12px')
              .attr('fill', 'black')
              .attr('text-anchor', 'middle');
        }
      }
      return;
    }

    function drawMaps(maps) {
      var g,
        different = function (a, b) {
          return a !== b;
        },
        returnId = function (d) {
          return d.id;
        },
        spinner = document.getElementById('loading-maps-spinner');
      if (spinner) {
        spinner.parentNode.removeChild(spinner);
      }

      maps.forEach(function (map, index) {
        svgUserDefined[index].selectAll('g')
          .remove();

        g = svgUserDefined[index].append('g')
              .attr('class', 'canvasMaps');

        g.selectAll('.region')
            .data(topojson.feature(map, map.objects[$scope.nutsLevels[index].label]).features)
          .enter()
            .append('path')
            .attr('class', 'region')
            .attr('d', path)
            .attr('id', returnId)
            .attr('index', index)
            .style('stroke', '#fff')
            .style('stroke-width', 0)
            .style('opacity', 1)
            .on('mousemove', setTooltipPosition)
            .on('mouseout', hideTooltip)
            .on('click', changeZoom);

        // TODO: API for layers
        // Draw borders
        // National
        g.append('path')
          .datum(topojson.mesh(maps[0], maps[0].objects.nuts0))
            .attr('class', 'svg-nuts0-borders')
            .attr('d', path);

        // Regional
        if (index === 1) {
          g.append('path')
            .datum(topojson.mesh(maps[1], maps[1].objects.nuts2), different)
              .attr('class', 'svg-nuts2-borders')
              .attr('d', path);
        }

        // Projection borders
        g.append('path')
          .classed('projection-composition-borders', true)
          .attr('d', projection.getCompositionBorders());
      });




      if ($scope.spread) {
        $scope.selectedSpread = $scope.allowedSpreads[$scope.allowedSpreads.length - 1];
      } else {
        $scope.selectedSpread = $scope.allowedSpreads[0];
      }
      updateMap($scope.selectedSpread);

      $scope.$watch('selectedSpread', resetAndUpdateMap);
      return;
    }

    function getFirstValueUpward(id, trie) {
      var originalId = id,
        value;

      while (id.length && !trie.get(id)) {
        id = id.slice(0, -1);
      }

      if (id.length === 0) {
        value = trie.get('EU28');
      } else {
        value = trie.get(id);
      }

      if (id !== originalId) {
        trie.add(originalId, value);
      }

      return value;
    }

    function hideTooltip() {
      toolTip.style.display = 'none';
      return;
    }

    function resetAndUpdateMap(newValue) {
      // Reset edited values
      $scope.editedValuesTrie = null;
      $scope.editedValuesTrie = dataStructuresService.createTrie();
      if (newValue.id === 'userDefined') {
        $scope.spread.modifiedTrie.keys.forEach(function (country) {
          var regionData = $scope.spread.modifiedTrie.get(country);
          if (regionData) {
            $scope.editedValuesTrie.add(country, {
              value : regionData.value,
              timeSeries : regionData.timeSeries.slice(0)
            });
          }
        });
      }
      updateMap(newValue);
      return;
    }

    function resizeGeoMaps() {
      var SVGContainer = document.getElementById('containerUserMap'),
        padding = Math.ceil(SVGContainer.clientWidth * 400 / fullWidth);
      updateSVGContainerPadding(padding);
      return;
    }

    function saveDataInTrie(dataArray) {
      var i, ln,
        returnAsyncAction = $q.all([
          MapDataService.getMap('EU28', 'NUTS0', 'topojson'),
          MapDataService.getMap('EU28', 'NUTS2', 'topojson')
        ]);

      $scope.allowedSpreads[0].trie = dataStructuresService.createTrie();
      $scope.allowedSpreads[0].trie.add('EU28', $scope.terminalValue);
      $scope.allowedSpreads[0].transform = new AffineTransformation($scope.allowedSpreads[0].trie, [minY, maxY]);

      for (i = 0, ln = dataArray.length; i < ln; i += 1) {
        $scope.allowedSpreads[i + 1].trie = dataArray[i].trie;
        $scope.allowedSpreads[i + 1].transform = new AffineTransformation($scope.allowedSpreads[i + 1].trie, [minY, maxY]);
      }

      if ($scope.spread) {
        for (i = 0, ln = $scope.allowedSpreads.length - 1; $scope.allowedSpreads[i].id !== $scope.spread.id && i < ln; i += 1) {
          //empty for loop
        }

        if (i === ln) {
          i = 0;
        }

        $scope.allowedSpreads[ln].trie = dataStructuresService.createTrie();
        $scope.allowedSpreads[i].trie.keys.forEach(function (key) {
          $scope.allowedSpreads[ln].trie.add(key, $scope.allowedSpreads[i].trie.get(key));
        });
        $scope.spread.modifiedNodes.forEach(function (node) {
          $scope.allowedSpreads[ln].trie.add(node.key, node.value);
        });
        $scope.allowedSpreads[ln].transform = new AffineTransformation($scope.allowedSpreads[i].trie, [minY, maxY]);
      }
      return returnAsyncAction;
    }

    function getCustomMapData() {
      //Only load statistical data if needed;
      downloadStats.push(MapDataService.getStatistics('demo_r_d3dens'));
      downloadStats.push(MapDataService.getStatistics('demo_r_d3dens_inv'));
      downloadStats.push(MapDataService.getStatistics('nama_r_e3gdp_PPS_HAB_EU'));
      downloadStats.push(MapDataService.getStatistics('nama_r_e3gdp_PPS_HAB_EU_inv'));

      if ($scope.spread) {
        $scope.spread.modifiedTrie = dataStructuresService.createTrie();
        $scope.spread.modifiedNodes.forEach(function (region) {
          $scope.spread.modifiedTrie.add(region.key, {
            value : region.value,
            timeSeries : region.timeSeries
          });
        });
        $scope.allowedSpreads.push({id : 'userDefined', name: 'Saved state'});
      }
      return;
    }

    function setFillColor() {
      /*jshint validthis:true */
      return color(this.getAttribute('node-value'));
    }

    function setTooltipPosition(d) {
      if (d !== $scope.centeredUser) {
        var coordinates = d3.mouse(document.getElementById('container-maps'));
        toolTip.style.left = (coordinates[0] + 20) + 'px';
        toolTip.style.top = (coordinates[1] - 20) + 'px';
      } else {
        toolTip.style.display = 'none';
      }
      return;
    }

    function toolTipFiller(d) {
      /*jshint validthis:true */
      toolTip.querySelector('h3').textContent = d.properties.name;
      toolTip.style.display = 'block';
      toolTip.querySelector('.popover-content').textContent = $filter('roundNumber')($scope.factor * this.getAttribute('node-value'));
      return;
    }

    function resetTimeSeries(regionId, newValue) {
      var regionData = $scope.editedValuesTrie.get(regionId);
      if (regionData) {
        regionData.timeSeries = BaselineDataService.createEmptyTimeSeries($scope.startYear, newValue, $scope.schemaObject.initialValue);
      }
    }

    function updateColorEditedRegion(newValue) {
      var editedNode = document.getElementById($scope.selectedRegion.id);
      if (editedNode) {
        var oldData = $scope.editedValuesTrie.get($scope.selectedRegion.id);
        if (oldData) {
          oldData.value = newValue;
        } else {
          $scope.editedValuesTrie.add($scope.selectedRegion.id, {
            value : newValue,
            timeSeries : BaselineDataService.createEmptyTimeSeries($scope.startYear, newValue, $scope.schemaObject.initialValue)
          });
        }

        editedNode.style.mask = 'url(#mask-stripe' + editedNode.getAttributeNS(null, 'index') + ')';

        d3.select(editedNode)
          .attr('node-value', newValue)
          .style('fill', setFillColor)
          .on('mouseover', toolTipFiller);
      }
    }

    function updateEditTable(newValue) {
      var svgNode = document.getElementById(newValue);

      if (svgNode) {
        $scope.valuesSelectedRegion.user = svgNode.getAttribute('node-value');
      } else if ($scope.allowedSpreads[0].trie && $scope.allowedSpreads[0].trie.get('EU28')) {
        $scope.valuesSelectedRegion.user = $scope.allowedSpreads[0].trie.get('EU28');
      }

      $scope.currentUserVal = $scope.valuesSelectedRegion.user * $scope.factor;

      return;
    }

    function updateMap(data) {
      $scope.affineTransformationParams = data.transform.params;
      function setNodeValue(d) {
        var value = false,
          NUTSValue = getFirstValueUpward(d.id, data.trie),
          rootValue = getFirstValueUpward('EU28', data.trie);

        if (data.id === 'userDefined' && $scope.spread.modifiedTrie.get(d.id) && $scope.spread.modifiedTrie.get(d.id).value) {
          value = $scope.spread.modifiedTrie.get(d.id).value;
        }

        if (value === false && rootValue !== false && NUTSValue !== false) {
          value = (NUTSValue ? NUTSValue : $scope.terminalValue);
          value = data.transform.compute(value);
        } else {
          value = parseFloat(value, 10);
        }

        return Math.round(value * 1000) / 1000;
      }

      d3.selectAll('.region')
        .attr('node-value', setNodeValue)
        .style('fill', setFillColor)
        .style('mask', function (d) {
          if (data.id === 'userDefined' && $scope.spread.modifiedTrie.get(d.id) && $scope.spread.modifiedTrie.get(d.id).value) {
            return 'url("#mask-stripe' + this.getAttributeNS(null, 'index') + '")';
          }
          return null;
        })
        .on('mouseover', toolTipFiller);
      return;
    }

    $scope.$watch('selectedRegion.id', updateEditTable);

    $scope.updateUserValCallback = function (currentUserVal) {
      currentUserVal = parseFloat(currentUserVal, 10);
      if (Number.isNaN(currentUserVal)) {
        currentUserVal = Math.round($scope.schemaObject.baseline * $scope.factor * 1000) / 1000;
        $scope.currentUserVal = currentUserVal;
      }

      $scope.valuesSelectedRegion.user = currentUserVal / $scope.factor;
      resetTimeSeries($scope.selectedRegion.id, $scope.valuesSelectedRegion.user);
      updateColorEditedRegion($scope.valuesSelectedRegion.user);

      if ($scope.selectedRegion.id === 'EU28') {
        $scope.terminalValue = currentUserVal / $scope.factor;
        $q
          .all(downloadStats)
          .then(saveDataInTrie)
          .then(drawMaps);
      }
    };



    // Algorithm
    renderedPromise().then(function (w) {
      updateWidthDependentVariables(w);
      if ($scope.editable === true) {
        getCustomMapData();
      }
      drawLegendAndCanvas();
      $q
        .all(downloadStats)
        .then(saveDataInTrie)
        .then(drawMaps)
        .then(function () {
          window.addEventListener('resize', resizeGeoMaps);
        })
        .catch(function (err) {
          throw err;
        });
    });

    $scope.$on('$destroy', function () {
      window.removeEventListener('resize', resizeGeoMaps);
    });
    return;

  }

  function createMapEditor(BaselineDataService) {
    return {
      retrict: 'A',
      replace: false,
      templateUrl: '/views/partials/mapEditor.tmpl.html',
      controller: controllerMapEditor,
      link : function ($scope, element, attrs) {
        var startYear = parseInt(attrs.startYear, 10);
        $scope.startYear = (Number.isNaN(startYear) ?
          BaselineDataService._MODEL_DEFAULT_FIRST_COMPUTE_YEAR :
          startYear);
      }
    };
  }

  controllerMapEditor.$inject = ['$scope', '$filter', '$q', 'MapDataService', 'BaselineDataService', 'dataStructuresService', 'd3'];
  createMapEditor.$inject = ['BaselineDataService'];
  angular.module('highToolClientApp')
    .directive('mapEditor', createMapEditor);
}());
