(function () {
  'use strict';
  angular.module('highToolClientApp')
    .directive('fileUploadButton', [function () {
      return {
        template :  '<button type="button" class="btn btn-default">' +
                      '<span class="{{innerClass}}">{{::textContent}}</span>' +
                    '</button>' +
                    '<input type="file" style="display: none;" accept="{{::accept}}"></input>',
        restrict : 'E',
        require : 'ngModel',
        scope : {
          onFileDone : '&',
          textContent : '@',
          accept : '@',
          allowMultiple : '='
        },
        link: function (scope, element, attrs, ngModel) {
          scope.textContent = scope.textContent || 'Select files to upload';
          var input = element.find('input');

          input[0].multiple = true;

          element.find('button')
            .bind('click', function () {
              input[0].click();
            });

          function copyObject(oriObj) {
            var copy = {};
            Object.keys(oriObj).forEach(function (key) {
              copy[key] = oriObj[key];
            });
            return copy;
          }


          input.bind('change', function (evt) {
            var filesToUpload = [];
            function onloadOrErrorCb(file, arrayEle, statusText) {
              return function(e) {
                var index = filesToUpload.indexOf(arrayEle);
                if (index !== -1) {
                  // Create a new reference because Angular only does reference
                  // check on $setViewValue.
                  filesToUpload[index] = copyObject(arrayEle);
                  filesToUpload[index].status = statusText;
                  if (e.target && e.target.result) {
                    var i, ln;
                    for (i = 0, ln = e.target.result.length; i < ln && e.target.result[i] !== ','; i += 1) {
                      // Empty block, search for the first comma
                    }

                    if (i === ln) {
                      filesToUpload[index].data = null;
                    } else {
                      filesToUpload[index].data = {
                        encoding : 'base64',
                        load : e.target.result.substring(i + 1)
                      };

                    }
                  } else {
                    filesToUpload[index].data = null;
                  }
                  filesToUpload[index].error = e.error || null;
                }
                ngModel.$setViewValue(filesToUpload);
              };
            }

            for (var i = 0, ln = evt.target.files.length; i < ln; i += 1) {
                var reader = new FileReader();
                filesToUpload.push({
                  mime : evt.target.files[i].type,
                  name : evt.target.files[i].name,
                  size : evt.target.files[i].size,
                  data : null,
                  status : 'loading'
                });

                reader.readAsDataURL(evt.target.files[i]);
                reader.onloadend = onloadOrErrorCb(evt.target.files[i], filesToUpload[filesToUpload.length - 1], 'loaded');
                reader.onerror = onloadOrErrorCb(evt.target.files[i], filesToUpload[filesToUpload.length - 1], 'error');

            }
            input[0].value = null;
            ngModel.$setViewValue(filesToUpload);
          });

        }
      };
    }]);
}());
