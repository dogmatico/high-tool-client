/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name highToolClientApp.directive:modalShift
   * @description
   * # modalShift. It requires data in the form:
   * {  tags: [labels of different elements],
   *    items: [{label : text, values : [Number]]
   * }
   * It draws a modal shift scaled to the greatest acculumated.
   */
  angular.module('highToolClientApp')
    .directive('modalShift', function () {
      return {
        restrict: 'E',
        replace: true,
        template: '<div></div>',
        scope: {
          width : '@',
          height : '@',
          data : '='
        },
        link: function (scope, element, attrs) {
          var i, tick, textTick, rootEle = angular.element(element)[0],
            numDivisions = attrs.divisions || 10,
            max = Math.max.apply(null,
              scope.data.items
                .map(function (item) {
                return item.values.reduce(function (a, b) {
                  return a + b;
                }, 0);
              })
              ),
            offSetAxis = 0,
            rectHeight = 60,
            colors = ['black', 'grey', '#E20020'],
            fonts = ['display: none', 'font-weight: bold; fill: white;', 'font-weight: bold; fill: white;'],
            svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
            svgFragment = document.createDocumentFragment(),
            gLegend = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
            gAxis = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
            xAxis = document.createElementNS('http://www.w3.org/2000/svg', 'line'),
            yAxis = document.createElementNS('http://www.w3.org/2000/svg', 'line'),
            gColors = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
            drawArea = (scope.data.items.length + 1) * attrs.width;

          svg.setAttributeNS(null, 'preserveAspectRatio', 'xMinYMin meet');
          svg.setAttributeNS(null, 'viewBox', '0 0 ' + ((scope.data.items.length + 1) * attrs.width) + ' ' + ((scope.data.items.length + 2) * attrs.height));
          rootEle.appendChild(svg);
          setTimeout(function () {
          // Create first the items labels to get its length. Append directly to SVG;

          scope.data.items.forEach(function (item, itemIndex) {
            var itemLabel = document.createElementNS('http://www.w3.org/2000/svg', 'text');
            itemLabel.setAttributeNS(null, 'x', 0);
            itemLabel.setAttributeNS(null, 'y', 1.2 * itemIndex * rectHeight + 0.5 * rectHeight);
            itemLabel.setAttributeNS(null, 'style', 'dominant-baseline: middle;  font-size: 1em');
            itemLabel.textContent = item.label;
            svg.appendChild(itemLabel);
            offSetAxis = Math.max(offSetAxis, itemLabel.getComputedTextLength());
          });
          offSetAxis += 2;

          drawArea -= (offSetAxis + 30);

          // Format Axis
          xAxis.setAttributeNS(null, 'x1', offSetAxis);
          xAxis.setAttributeNS(null, 'y1', 1.2 * scope.data.items.length * rectHeight);
          xAxis.setAttributeNS(null, 'x2', offSetAxis + drawArea + 10);
          xAxis.setAttributeNS(null, 'y2', 1.2 * scope.data.items.length * rectHeight);
          xAxis.setAttributeNS(null, 'style', 'stroke: grey; stroke-width: 0.5');
          gAxis.appendChild(xAxis);

          yAxis.setAttributeNS(null, 'x1', offSetAxis + 2);
          yAxis.setAttributeNS(null, 'y1', 0);
          yAxis.setAttributeNS(null, 'x2', offSetAxis + 2);
          yAxis.setAttributeNS(null, 'y2', 2 + 1.2 * scope.data.items.length * rectHeight);
          yAxis.setAttributeNS(null, 'style', 'stroke: grey; stroke-width: 0.5');
          gAxis.appendChild(yAxis);

          //Create ticks in x
          var xOff;
          for (i = 0; i <= numDivisions; i += 1) {
            tick = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            textTick = document.createElementNS('http://www.w3.org/2000/svg', 'text');
            xOff = offSetAxis + 2 + i * drawArea / numDivisions;

            tick.setAttributeNS(null, 'x1', xOff);
            tick.setAttributeNS(null, 'y1', 0);
            tick.setAttributeNS(null, 'x2', xOff);
            tick.setAttributeNS(null, 'y2', 2 + 1.2 * scope.data.items.length * rectHeight);
            tick.setAttributeNS(null, 'style', 'stroke: grey; stroke-width: 0.8');
            gAxis.appendChild(tick);

            textTick.setAttributeNS(null, 'x', xOff);
            textTick.setAttributeNS(null, 'y', 10 + 1.2 * scope.data.items.length * rectHeight);
            textTick.setAttributeNS(null, 'text-anchor', 'middle');
            textTick.setAttributeNS(null, 'style', 'dominant-baseline: middle;');
            textTick.setAttributeNS(null, 'class', 'tickSplit');
            textTick.textContent = (i === 0 ? 0 : (i * max / numDivisions).toFixed(2));
            gAxis.appendChild(textTick);
          }

          svgFragment.appendChild(gAxis);

          scope.data.items.forEach(function (item, itemIndex) {
            var gBar = document.createElementNS('http://www.w3.org/2000/svg', 'g'),
              accumWidth = 0,
              itemTotal = item.values.reduce(function (a, b) {
                return a + b;
              }, 0);

            gBar.setAttributeNS(null, 'transform', 'translate(2, ' + (1.2 * itemIndex * rectHeight) + ')');

            item.values.forEach(function (value, valueIndex) {
              var box = document.createElementNS('http://www.w3.org/2000/svg', 'rect'),
                text = document.createElementNS('http://www.w3.org/2000/svg', 'text'),
                rectWidth = Math.ceil(drawArea * value / max);

              box.setAttributeNS(null, 'width', rectWidth);
              box.setAttributeNS(null, 'height', rectHeight);
              box.setAttributeNS(null, 'x', offSetAxis + accumWidth);
              box.setAttributeNS(null, 'y', 0);
              box.setAttributeNS(null, 'style', 'fill: ' + colors[valueIndex % colors.length]);

              text.setAttributeNS(null, 'x', offSetAxis + Math.ceil(rectWidth / 2) + accumWidth);
              text.setAttributeNS(null, 'y', Math.ceil(rectHeight / 2));
              text.setAttributeNS(null, 'text-anchor', 'middle');
              text.setAttributeNS(null, 'style', 'dominant-baseline: middle; font-size: 1em; ' + fonts[valueIndex % fonts.length]);
              text.textContent = (100 * value / itemTotal).toFixed(1) + ' %';

              accumWidth += rectWidth;
              gBar.appendChild(box);
              gBar.appendChild(text);
            });
            svgFragment.appendChild(gBar);
          });

          /** Build Legend. The legend is directly attached to SVG to prevent
           * overflows
           */
          gLegend.appendChild(gColors);
          svg.appendChild(gLegend);

          var accumulatedWidth = 0;
          scope.data.tags.forEach(function (tag, index) {
            var square = document.createElementNS('http://www.w3.org/2000/svg', 'rect'),
              lengendTag = document.createElementNS('http://www.w3.org/2000/svg', 'text'),
              textSize,
              xBase;

            lengendTag.setAttributeNS(null, 'x', 0);
            lengendTag.setAttributeNS(null, 'y', 22 + rectHeight / 2);
            lengendTag.setAttributeNS(null, 'text-anchor', 'middle');
            lengendTag.textContent = tag;
            gColors.appendChild(lengendTag);
            textSize = lengendTag.getComputedTextLength();
            xBase = Math.max(rectHeight, textSize);
            lengendTag.setAttributeNS(null, 'x', accumulatedWidth + 0.5 * xBase);

            square.setAttributeNS(null, 'width', rectHeight);
            square.setAttributeNS(null, 'x', accumulatedWidth + (rectHeight > textSize ? 0 : (textSize - rectHeight) / 2));
            square.setAttributeNS(null, 'y', 10);
            square.setAttributeNS(null, 'height', rectHeight / 2);
            square.setAttributeNS(null, 'style', 'fill: ' + colors[index % colors.length]);
            accumulatedWidth += (xBase + 20);
            gColors.appendChild(square);
          });
          gColors.setAttributeNS(null, 'transform', 'translate(' + (drawArea / 2 - 0.6 * scope.data.tags.length * rectHeight) + ', 10)');
          gLegend.setAttributeNS(null, 'transform', 'translate(0, ' + (1.1 * (scope.data.items.length + 1) * rectHeight) + ')');

          svg.appendChild(svgFragment);
        }, 10);
        }
      };
    });
}());
