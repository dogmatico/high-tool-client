/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name highToolClientApp.directive:timeEditor
   * @description
   * # timeEditor
   */


  angular.module('highToolClientApp')
    .directive('timeEditor', ['d3', '$filter', '$q', 'LinearAlgebraService', function (d3, $filter, $q, LinearAlgebraService) {
      function timeEditorLink(scope, element, attr, ngModel) {
        var xmlns = 'http://www.w3.org/2000/svg';
        var Promise = Promise || $q;

        // Variables to catch the DOM elment and control movement, mouse, etc.
        var DOMElement = angular.element(element)[0],
          selectedNode = null,
          selectedNodeIndex = null,
          mousedownNode = null,
          mousedownNodeIndex = null,
          mouseupNode = null,
          lastKeyDown = -1;

        function resetMouseVars() {
          mousedownNode = null;
          mouseupNode = null;
        }

        var factor = parseFloat(attr.factor, 10);
        factor = (Number.isNaN(factor) ? 100 : factor);

        ngModel.$render = function () {
          /* Clear old SVG. TODO: Only remove the contents and keep the reusable
           * elements like axis, svg, etc.
           */

          var oldSVG = DOMElement.querySelector('svg');
          if (oldSVG) {
            oldSVG.parentNode.removeChild(oldSVG);
          }

          var nodeController = new LinearAlgebraService.HermiteSpline(ngModel.$modelValue);

          // References to d3 elements and utilities
          var svg, x, y, xAxis, yAxis, line, gView, gCircles;

          // Reference to the tooltip container;
          var tooltipOuter;

          function renderedPromise() {
            return new Promise(function (resolve) {
              function check() {
                if (DOMElement && DOMElement.clientWidth !== 0) {
                  resolve(DOMElement.clientWidth);
                } else {
                  setTimeout(check, 100);
                }
              }
              check();
            });
          }

          renderedPromise().then(function (width) {
            // Define frame
            var colors = function () {
              return '#E20020';
            },
              margin = {
                top : 20,
                bottom : 20,
                left : 40,
                right : 20
              },
              height = 300 - margin.top - margin.bottom;


            function defineD3Utilities(width) {
              // Redefine as a function of width;
              x = d3.scale.linear()
                .domain(scope.range.x)
                .range([0, width]);
              y = d3.scale.linear()
                .domain([scope.range.y[0], scope.range.y[1]])
                .range([height, 0]);

              //nodeController.setScaleFunctions(x, y);
              xAxis = d3.svg.axis()
                .scale(x)
                .ticks(10)
                .tickSize(-height)
                .tickFormat(d3.format('d'));
              yAxis = d3.svg.axis()
                .scale(y)
                .ticks(4)
                .tickFormat(function (value) {
                  return $filter('roundNumber')(value * factor);
                })
                .orient('right');
              line = d3.svg.line()
                .x(function (d) { return x(d[0]); })
                .y(function (d) { return y(d[1]); });
              return;
            }

            function updatePaths() {
              d3.select(svg).selectAll('.time-trajectory-path')
                .remove();

              var pathD = nodeController.bezierCubic.map(function (groupPoints) {
                return groupPoints.map(function (point) {
                  return [Math.round(x(point[0]) * 10)/10, Math.round(y(point[1]) * 10)/10];
                });
              }).reduce(function (acum, points, index) {
                if (index === 0) {
                  acum += 'M' + points[0][0] + ',' + points[0][1];
                }
                acum += 'C' + points[1][0] + ',' + points[1][1] + ' ';
                acum += points[2][0] + ',' + points[2][1] + ' ' + points[3][0] + ',' + points[3][1];
                return acum;
              }, '');

              var path = document.createElementNS(xmlns, 'path');
              [
                ['class', 'line time-trajectory-path'],
                ['stroke-width', 6],
                ['d', pathD]
              ].forEach(function (keyVal) {
                path.setAttributeNS(null, keyVal[0], keyVal[1]);
              });

              path.addEventListener('mouseover', function(e) {
                e.preventDefault();
                tooltipOuter.style.display = 'block';
                tooltipOuter.style.left = (e.offsetX) + 'px';
                tooltipOuter.style.top = (e.offsetY + 35) + 'px';
                tooltipOuter.querySelector('.tooltip-inner').textContent = '(' + Math.floor(x.invert(e.offsetX - margin.left)) + ', ' + $filter('roundNumber')(y.invert(e.offsetY - margin.top) * factor) + ')';
              }, true);

              path.addEventListener('mouseout', function(e) {
                e.preventDefault();
                tooltipOuter.style.display = 'none';
              }, true);

              gView.appendChild(path);

              ngModel.$setViewValue(nodeController._interpolPts);
              return;
            }



            function restart() {
              if (scope.editable !== false) {
                d3.select(svg).selectAll('.circle-group circle')
                  .remove();

                nodeController._interpolPts.forEach(function (point, index) {
                  var circle = document.createElementNS(xmlns, 'circle');
                  [
                    ['transform', 'translate(' + Math.floor(x(point[0])) + ',' + Math.floor(y(point[1])) + ')'],
                    ['class', 'node'],
                    ['r', 7],
                    ['style', (function () {
                      var retStr = 'fill: ' + ((index === selectedNodeIndex) ? d3.rgb(colors(index)).brighter().toString() : colors(index)) + '; ';
                      retStr += 'stroke: ' + d3.rgb(colors(index)).darker().toString() + ';';
                      return retStr;
                    }())]
                  ].forEach(function (keyVal) {
                    circle.setAttributeNS(null, keyVal[0], keyVal[1]);
                  });

                  circle.addEventListener('mousedown', function (e) {
                    e.stopPropagation();
                    if (mousedownNode === e.target) {
                      mousedownNode = null;
                      mousedownNodeIndex = null;
                    } else {
                      mousedownNode = e.target;
                      mousedownNodeIndex = index;
                    }
                  }, true);

                  circle.addEventListener('click', function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    if (selectedNode) {
                      selectedNode.style.fill = colors(index);
                    }

                    if (selectedNode === e.target) {
                      selectedNodeIndex = null;
                      selectedNode = null;
                      e.target.style.fill = colors(index);
                    } else {
                      selectedNode = e.target;
                      selectedNodeIndex = index;
                      e.target.style.fill = d3.rgb(colors(index)).brighter().toString();
                    }
                  }, true);

                  gCircles.appendChild(circle);
                });

              }
              updatePaths();
            }

            function drawCanvas(DOMElement) {
              var oldSVG = DOMElement.querySelectorAll('svg');
              for (var i = 0, ln = oldSVG.length; i < ln; i +=1 ) {
                DOMElement.removeChild(oldSVG[i]);
              }

              var svgEle = document.createElementNS(xmlns, 'svg');

              [['width', width + margin.left + margin.right],
              ['height', height + margin.top + margin.bottom]].forEach(function (keyVal) {
                svgEle.setAttributeNS(null, keyVal[0], keyVal[1]);
              });


              svgEle.addEventListener('mousedown', function (e) {
                if (scope.editable !== false) {
                  tooltipOuter.style.display = 'none';
                  resetMouseVars();
                  if (e.target.tagName.toLowerCase() === 'svg') {
                    nodeController.addPoint([x.invert(e.offsetX - margin.left), y.invert(e.offsetY - margin.top), 0], false);
                  }
                  restart();
                }
              }, false);

              svgEle.addEventListener('mouseup', function () {
                if (mousedownNode) {
                  mousedownNode = null;
                  mousedownNodeIndex = null;
                }
              }, false);

              svgEle.addEventListener('mousemove', function (e) {
                if (mousedownNode) {
                  var coordinates = mousedownNode.getAttributeNS(null, 'transform')
                    .match(/\((\d+),(\d+)\)/);
                  coordinates.splice(0, 1);

                  var toPolyDomain = [
                    x.invert(e.offsetX - margin.left),
                    y.invert(e.offsetY - margin.top)
                  ];

                  var newData = nodeController._interpolPts[mousedownNodeIndex].slice(0);

                  if (mousedownNodeIndex !== 0 && mousedownNodeIndex !== nodeController._interpolPts.length - 1) {
                    if (toPolyDomain[0] > nodeController._interpolPts[mousedownNodeIndex - 1][0] &&
                      toPolyDomain[0] < nodeController._interpolPts[mousedownNodeIndex + 1][0]) {
                      newData[0] = toPolyDomain[0];
                      coordinates[0] = e.offsetX - margin.left;
                    }
                  }

                  if (scope.range.y[0] <= toPolyDomain[1] && scope.range.y[1] >= toPolyDomain[1]) {
                    newData[1] = toPolyDomain[1];
                    coordinates[1] = e.offsetY - margin.top;
                  }

                  nodeController.editPoint(mousedownNodeIndex, newData);
                  mousedownNode.setAttributeNS(null, 'transform', 'translate(' + Math.floor(coordinates[0]) + ',' + Math.floor(coordinates[1]) + ')');
                  updatePaths();
                }
              }, true);

              gView = document.createElementNS(xmlns, 'g');
              gView.setAttributeNS(null, 'transform', 'translate(' + margin.left + ',' + margin.top + ')');

              svgEle.appendChild(gView);

              var gYAxis = document.createElementNS(xmlns, 'g');
              gYAxis.setAttributeNS(null, 'class', 'y axis');
              d3.select(gYAxis).call(yAxis);

              var gXAxis = document.createElementNS(xmlns, 'g');
              gXAxis.setAttributeNS(null, 'class', 'x axis');
              gXAxis.setAttributeNS(null, 'transform', 'translate(0,' + height + ')');
              d3.select(gXAxis).call(xAxis);

              gView.appendChild(gYAxis);
              gView.appendChild(gXAxis);

              gCircles = document.createElementNS(xmlns, 'g');
              gCircles.setAttributeNS(null, 'class', 'circle-group');

              gView.appendChild(gCircles);
              DOMElement.appendChild(svgEle);

              var fragment = document.createDocumentFragment(),
                tooltipInner = document.createElement('div');

              tooltipOuter = document.createElement('div');
              tooltipOuter.className = 'tooltip bottom';
              tooltipOuter.style.opacity = '90';
              tooltipOuter.style.display = 'none';
              tooltipOuter.setAttribute('role', 'tooltip');
              tooltipInner.className = 'tooltip-inner';
              tooltipOuter.appendChild(tooltipInner);
              fragment.appendChild(tooltipOuter);
              DOMElement.appendChild(fragment);


              var baselinePathLine = d3.svg.line()
                .x(function (d) {return x(d[0]); })
                .y(function (d) {return y(d[1]); });

              d3.select(gView).append('path')
                .attr('d', baselinePathLine(scope.baselineData))
                .attr('stroke', 'grey')
                .attr('stroke-width', 2)
                .attr('stroke-dasharray', '5,5')
                .attr('fill', 'none');

              return svgEle;
            }

            function keydown() {
              if (lastKeyDown !== -1) {
                return;
              }
              lastKeyDown = d3.event.keyCode;

              if (!selectedNode) {
                return;
              }

              switch (d3.event.keyCode) {
              case 8: // backspace
              case 46: // delete
                if (selectedNode && selectedNodeIndex !== 0 && selectedNodeIndex !== nodeController._interpolPts.length - 1) {
                  nodeController.deletePoint(selectedNodeIndex);
                }
                selectedNode = null;
                selectedNodeIndex = null;
                restart();
                break;
              case 171: // +
                if (selectedNode) {
                  nodeController.editPoint(selectedNodeIndex, [selectedNode[0], selectedNode[1], parseFloat(selectedNode[2]) + 0.05]);
                  updatePaths();
                }
                break;
              case 173: // -
                if (selectedNode) {
                  nodeController.editPoint(selectedNodeIndex, [selectedNode[0], selectedNode[1], parseFloat(selectedNode[2]) - 0.05]);
                  updatePaths();
                }
                break;
              case 48: // 0
                if (selectedNode) {
                  nodeController.editPoint(selectedNodeIndex, [selectedNode[0], selectedNode[1], 0]);
                  updatePaths();
                }
                break;
              default:
                return false;
              }
              scope.$apply();
              return;
            }

            function keyup() {
              lastKeyDown = -1;
            }

            //Main logic
            function main(width) {
              width = width - margin.left - margin.right;
              defineD3Utilities(width);
              resetMouseVars();

              svg = drawCanvas(DOMElement);

              scope.updatePaths = updatePaths;

              d3.select(window)
                .on('keydown', keydown)
                .on('keyup', keyup);

              restart();
            }

            main(width);

            function resizeCallback() {
              DOMElement.removeChild(DOMElement.getElementsByTagName('svg')[0]);
              main(DOMElement.clientWidth);
            }

            window.addEventListener('resize', resizeCallback);
            scope.$on('$destroy', function () {
              window.removeEventListener('resize', resizeCallback);
            });
            return;
          });
        };
      }

      return {
        restrict: 'A',
        require: 'ngModel',
        scope : {
          baselineData : '=',
          range : '=',
          editable : '=',
          timeSeries : '=',
          factor : '@'
        },
        link: timeEditorLink
      };
    }]);
}());
