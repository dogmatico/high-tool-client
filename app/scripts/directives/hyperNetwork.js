(function () {
  /*globals topojson*/
  'use strict';


  angular.module('highToolClientApp')
    .directive('hypernetwork', function () {
      return {
        require : 'E',
        transclude : true,
        scope : {
          schemaId : '@',
          modeId : '@'
        },
        controller : ['$scope', '$q', 'MapDataService', 'TablesService', function ($scope, $q, MapDataService, TablesService) {
          var Promise = Promise || $q;

          $scope.disableSave = false;

          $scope.geoData = Promise.all([
            MapDataService.getMap('EU28', 'NUTS0', 'topojson'),
            MapDataService.getMap('EU28', 'NUTS2', 'topojson'),
            MapDataService.getHyperNetwork($scope.modeId, $scope.schemaId)
          ]);

          $scope.geoData
            .then(function (data) {
              if (data[2].graph && data[2].graph.edges && data[2].graph.edges.length > 0) {
                $scope.yearsEdges = Object.keys(data[2].graph.edges[0].metadata).sort();
                $scope.yearsNodes = Object.keys(data[2].graph.nodes[0].metadata.years).sort();
                $scope.metadataKeysEdges = ['i_pd_link_time_weight', 'i_pd_link_dist_weight'];
                $scope.metadataKeysNodes = ['i_pd_ae_time_weight', 'i_pd_ae_dist_weight'];

                TablesService.queryVariables($scope.metadataKeysEdges.concat($scope.metadataKeysNodes).concat(['i_pd_link_time', 'i_pd_link_dist', 'i_pd_ae_time', 'i_pd_ae_dist']))
                  .then(function (variablesDict) {
                    $scope.variablesDict = variablesDict;
                  });
              }
            });

          $scope.selectedEdge = null;
          $scope.selectedNode = null;
          $scope.originalNodes = null;
          $scope.originalEdges = null;

          $scope.resetAll = function () {
            var i, ln;
            for (i = 0, ln = $scope.graph.edges.length; i < ln; i += 1) {
              $scope.graph.edges[i].metadata = JSON.parse(JSON.stringify($scope.originalEdges[i].metadata));
            }
            for (i = 0, ln = $scope.graph.nodes.length; i < ln; i += 1) {
              $scope.graph.nodes[i].metadata = JSON.parse(JSON.stringify($scope.originalNodes[i].metadata));
            }
            var edited = $scope.svg.getElementsByClassName('svg-hypernet-edited');
            for (i = 0, ln = edited.length; i < ln; i += 1) {
              edited[i].classList.remove('svg-hypernet-edited');
            }
          };
          $scope.resetSelected = function () {
            var id = null;
            var index;
            if ($scope.selectedEdge) {
              index = $scope.selectedEdge.index;
              Object.keys($scope.selectedEdge.metadata).forEach(function (year) {
                $scope.selectedEdge.metadata[year] = JSON.parse(JSON.stringify($scope.originalEdges[index].metadata[year]));
              });
              id = 'hypernetworkEdge_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + $scope.selectedEdge.source + '_' + $scope.selectedEdge.target;
            }
            if ($scope.selectedNode) {
              index = $scope.selectedNode.index;
              Object.keys($scope.selectedNode.metadata.years).forEach(function (year) {
                $scope.selectedNode.metadata.years[year] = JSON.parse(JSON.stringify($scope.originalNodes[index].metadata.years[year]));
              });
              id = 'hypernetworkNode_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + $scope.selectedNode.id;
            }
            if (id) {
              $scope.svg.getElementById(id).classList.remove('svg-hypernet-edited');
            }
          };
          $scope.saveGraph = function () {
            $scope.disableSave = true;
            Promise.all(['edges', 'nodes'].reduce(function (acum, type) {
              acum.push(new Promise(function (resolve, reject) {
                TablesService.updateHypernetwork($scope.schemaId, $scope.modeId, type, $scope.graph[type], function (err) {
                  if (err) {
                    reject(err);
                  } else {
                    resolve();
                  }
                });
              }));
              return acum;
            }, []))
              .then(function () {
                $scope.disableSave = false;
              })
              .catch(function (err) {
                $scope.disableSave = false;
                console.log(err);
              });
          };

          $scope.svg = null;
          $scope.markEdited = function (type, element) {
            if ($scope.svg) {
              var id = (type === 'node' ?
                'hypernetworkNode_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + element.id :
                'hypernetworkEdge_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + element.source + '_' + element.target
              );
              $scope.svg.getElementById(id).classList.add('svg-hypernet-edited');
            }
          };
        }],
        templateUrl : 'views/partials/hypernetworkEditor.tmpl.html'
      };
    });

  angular.module('highToolClientApp')
    .directive('hypernetworkMap', ['$q', 'd3', function ($q, d3) {

      function hypernetworkLink ($scope, element) {
        var DOMEle = angular.element(element)[0];
        var projection =  d3.geo.equirectangular()
          .center([10, 48.22])
          .scale(600);
        var path = d3.geo.path().projection(projection);

        $scope.graph = {};

        var xmlns = 'http://www.w3.org/2000/svg';

        var svg = document.createElementNS(xmlns, 'svg');
        svg.setAttributeNS(null, 'viewBox', '0 0 900 500');
        svg.setAttributeNS(null, 'preserveAspectRatio', 'xMidYMid meet');

        var gContainer = document.createElementNS(xmlns, 'g');

        $scope.geoData.then(function (data) {
          var gNUTS0Inner = d3.select(gContainer).append('g')
            .attr('class', 'canvasMaps');

          gNUTS0Inner.selectAll('.region')
              .data(topojson.feature(data[0], data[0].objects.nuts0).features)
            .enter()
              .append('path')
              .attr('class', 'svg-hypernet-territory-nuts0')
              .attr('d', path)
              .style('stroke', '#fff')
              .style('stroke-width', 0)
              .style('opacity', 1)
              .style('fill', '#E6E6E6');


          var gNUTS2 = d3.select(gContainer).append('g')
            .attr('class', 'canvasMaps svg-hypernet-hidden');

          gNUTS2.append('path')
            .datum(topojson.mesh(data[1], data[1].objects.nuts2))
              .attr('class', 'svg-hypernet-borders-nuts2')
              .attr('d', path);

          var gNUTS0 = d3.select(gContainer).append('g')
                .attr('class', 'canvasMaps');

          gNUTS0.append('path')
            .datum(topojson.mesh(data[0], data[0].objects.nuts0))
              .attr('class', 'svg-hypernet-borders-nuts0')
              .attr('d', path);

          var gNodes = document.createElementNS(xmlns, 'g');
          gNodes.setAttributeNS(null, 'class', 'svg-hypernet-nodes');
          $scope.graph.nodes = [];
          $scope.graph.nodesHash = {};

          data[2].graph.nodes.forEach(function (node, index) {
            $scope.graph.nodesHash[node.id] = {
              label : node.label,
              lat : node.metadata.geo.lat,
              long : node.metadata.geo.long
            };
            $scope.graph.nodes.push(node);
            var title = document.createElementNS(xmlns, 'title');
            title.textContent = node.label;

            var circle = document.createElementNS(xmlns, 'circle');
            var projected = projection([node.metadata.geo.long, node.metadata.geo.lat]);

            circle.setAttributeNS(null, 'cx', projected[0]);
            circle.setAttributeNS(null, 'cy', projected[1]);
            circle.setAttributeNS(null, 'r', '3');
            circle.setAttributeNS(null, 'class', 'svg-hypernet-node');
            circle.setAttributeNS(null, 'id', 'hypernetworkNode_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + node.id);

            circle.addEventListener('click', function () {
              $scope.selectedNode = ($scope.selectedNode === node ? null : node);
              $scope.selectedEdge = null;
              $scope.$apply();
            });

            node.index = index;
            circle.appendChild(title);
            gNodes.appendChild(circle);
          });
          $scope.originalNodes = JSON.parse(JSON.stringify(data[2].graph.nodes));

          $scope.graph.edges = [];
          var gEdges = document.createElementNS(xmlns, 'g');
          gEdges.setAttributeNS(null, 'class', 'svg-hypernet-edges');
          data[2].graph.edges.forEach(function (edge, index) {
            $scope.graph.edges.push(edge);
            var projectedSource = projection([$scope.graph.nodesHash[edge.source].long, $scope.graph.nodesHash[edge.source].lat]);
            var projectedTarget = projection([$scope.graph.nodesHash[edge.target].long, $scope.graph.nodesHash[edge.target].lat]);

            var line = document.createElementNS(xmlns, 'line');
            line.setAttributeNS(null, 'x1', projectedSource[0]);
            line.setAttributeNS(null, 'y1', projectedSource[1]);
            line.setAttributeNS(null, 'x2', projectedTarget[0]);
            line.setAttributeNS(null, 'y2', projectedTarget[1]);
            line.setAttributeNS(null, 'class', 'svg-hypernet-edge');
            line.setAttributeNS(null, 'id', 'hypernetworkEdge_schema_' + $scope.schemaId + '_mode_' + $scope.modeId + '_' + edge.source + '_' + edge.target);

            edge.index = index;
            line.addEventListener('click', function () {
              $scope.selectedEdge = ($scope.selectedEdge === edge ? null : edge);
              $scope.selectedNode = null;
              $scope.$apply();
            });

            gEdges.appendChild(line);
          });
          $scope.originalEdges = JSON.parse(JSON.stringify(data[2].graph.edges));

          gContainer.appendChild(gEdges);
          gContainer.appendChild(gNodes);
          svg.appendChild(gContainer);
          DOMEle.appendChild(svg);

          $scope.svg = svg;
          var pt = svg.createSVGPoint();
          var scaleFactor = 1;
          var lastCenter = [450, 250];
          svg.addEventListener('wheel', function (e) {
            e.preventDefault();
            pt.x = e.clientX; pt.y = e.clientY;
            var ptCoord = pt.matrixTransform(svg.getScreenCTM().inverse());

            // Undo affine Map, get original coordinates
            var coord = [ptCoord.x / scaleFactor + (scaleFactor - 1) * lastCenter[0] / scaleFactor, ptCoord.y / scaleFactor + (scaleFactor - 1) * lastCenter[1] / scaleFactor];

            // Apply map scaling from original coordinates.
            scaleFactor = (e.deltaY < 0 ? Math.min(scaleFactor + 1, 12) : Math.max(scaleFactor - 1, 1));
            gContainer.setAttributeNS(null, 'transform', 'translate(' + (-coord[0] * (scaleFactor - 1)) + ', ' + (-coord[1] * (scaleFactor - 1)) + ') scale(' + scaleFactor + ')');

            lastCenter = coord;

            var circles = gNodes.getElementsByTagName('circle');
            for (var i = 0, ln = circles.length; i < ln; i += 1) {
              circles[i].setAttributeNS(null, 'r', 3.5 / Math.sqrt(scaleFactor));
            }
            gNUTS0.select('path')
              .attr('stroke-width', (1.4 / scaleFactor) + 'px');

            gNUTS2.classed('svg-hypernet-hidden', (scaleFactor < 4));

            gNUTS2.select('path')
              .attr('stroke-width', (0.5 / scaleFactor) + 'px');
          });
        });
      }

      return {
        restrict : 'E',
        replace: false,
        link : hypernetworkLink
      };
    }]);

}());
