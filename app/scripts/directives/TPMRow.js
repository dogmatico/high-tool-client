/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc directive
   * @name highToolClientApp.directive:policyRow
   * @description
   * # policyRow
   */

  function TPMRowDirective($filter, $uibModal, $timeout) {
    var directive;
    function linkPolicyEditor($scope, element, attrs, ngModel) {
      var distance = Math.max(parseFloat($scope.schemaObject.max) - parseFloat($scope.schemaObject.baseline), parseFloat($scope.schemaObject.baseline) - parseFloat($scope.schemaObject.min));
      var step = parseFloat($scope.schemaObject.max) - parseFloat($scope.schemaObject.min) / 10;
      var factor = ($scope.schemaObject.target.__is_relative ? 100 : 1);

      $scope.editable = ($scope.viewOnly === 'true' ? false : true);
      $scope.hideTime = ($scope.hideTime === 'true');
      $scope.hideSpatial = ($scope.hideSpatial === 'true');

      function updateGradient(newValue) {
        var t = Math.abs(parseFloat(newValue, 10) - $scope.schemaObject.baseline) / distance;
        $scope.gradient = String('rgb(' + (229 - Math.ceil(229 * t)) + ', ' + (245 - Math.ceil(198 * t)) + ', '+ (255 - Math.ceil(178 * t)) + ')');
      }

      function copyObjWithJSON (obj) {
        return JSON.parse(JSON.stringify(obj));
      }

      function modifyObjDeleteTimeSeries (obj) {
        if (obj.representationaData && obj.representationaData.timeSeries) {
          obj.representationaData.timeSeries = [];
        }
        $scope.editedTimeSeries = false;
      }

      $scope.decreaseValue = function () {
        var copyVal = copyObjWithJSON(ngModel.$modelValue);
        copyVal.terminalValue = Math.max(copyVal.terminalValue - step, $scope.schemaObject.min);
        ngModel.$setViewValue(copyVal);
        updateGradient(ngModel.$modelValue.terminalValue);
        ngModel.$setDirty();
        $scope.userValue = Math.round(factor * ngModel.$modelValue.terminalValue * 1000) / 1000;
      };

      $scope.increaseValue = function () {
        var copyVal = copyObjWithJSON(ngModel.$modelValue);
        copyVal.terminalValue = Math.min(copyVal.terminalValue + step, $scope.schemaObject.max);
        ngModel.$setViewValue(copyVal);
        updateGradient(ngModel.$modelValue.terminalValue);
        ngModel.$setDirty();
        $scope.userValue = Math.round(factor * ngModel.$modelValue.terminalValue * 1000) / 1000;
      };

      $scope.changeUsePolicyCb = function (newValue) {
        var copyVal = copyObjWithJSON(ngModel.$modelValue);
        copyVal.use = newValue;
        ngModel.$setViewValue(copyVal);
        ngModel.$setDirty();
      };

      $scope.editeYearcb = function (newValue) {
        var copyVal = copyObjWithJSON(ngModel.$modelValue);
        copyVal.startYear = parseInt(newValue, 10);
        modifyObjDeleteTimeSeries(copyVal);
        ngModel.$setViewValue(copyVal);
        ngModel.$setDirty();
      };

      $scope.userValueCb = function (newValue) {
        $scope.userValue = parseFloat(newValue, 10);
        if (Number.isNaN($scope.userValue)) {
          $scope.userValue = Math.round(factor * $scope.schemaObject.baseline * 1000) / 1000;
        }

        var copyVal = copyObjWithJSON(ngModel.$modelValue);
        copyVal.terminalValue = $scope.userValue / factor;
        modifyObjDeleteTimeSeries(copyVal);
        ngModel.$setViewValue(copyVal);
        updateGradient(ngModel.$modelValue.terminalValue);
        ngModel.$setDirty();
      };

      $scope.editedTimeSeries = false;
      $scope.editedMap = false;

      $scope.resetLever = function () {
        ngModel.$setViewValue({
          representationaData : {
            timeSeries : []
          },
          startYear : 2015,
          terminalValue : $scope.schemaObject.baseline
        });
        $scope.userValue = Math.round(factor * ngModel.$modelValue.terminalValue * 1000) / 1000;
        updateGradient(ngModel.$modelValue.terminalValue);
        $scope.editedTimeSeries = false;
        $scope.editedMap = false;
        ngModel.$setPristine();
      };

      $scope.Math = Math;

      function initSlider () {
        var slider = angular.element(element)[0].querySelector('input[type=range]');
        if (angular.isDefined($scope.minValue) && angular.isDefined($scope.maxValue) && angular.isDefined(ngModel.$modelValue.terminalValue)) {
          slider.setAttribute('min', $scope.minValue);
          slider.setAttribute('max', $scope.maxValue);
          slider.setAttribute('step', $scope.stepSize);
          slider.setAttribute('value', $scope.userValue);
        } else {
          $timeout(initSlider, 100);
        }
      }

      ngModel.$render = function () {

        updateGradient(ngModel.$modelValue.terminalValue);
        $scope.userValue = Math.round(factor * ngModel.$modelValue.terminalValue * 1000) / 1000;
        $scope.startYear = ngModel.$modelValue.startYear;
        $scope.minValue = Math.round(($scope.schemaObject.target.__is_relative ? 100 : 1) * $scope.schemaObject.min * 10000) / 10000;
        $scope.maxValue = Math.round(($scope.schemaObject.target.__is_relative ? 100 : 1) * $scope.schemaObject.max * 10000) / 10000;
        $scope.stepSize = Math.round((100000 * ($scope.maxValue - $scope.minValue) / 40)) / 100000;

        initSlider();

        $scope.editedTimeSeries = (
          ngModel.$modelValue.representationaData &&
          ngModel.$modelValue.representationaData.timeSeries &&
          ngModel.$modelValue.representationaData.timeSeries.length);
        $scope.editedMap = (
          ngModel.$modelValue.representationaData &&
          ngModel.$modelValue.representationaData.spread &&
          (ngModel.$modelValue.representationaData.spread.id !== 'baseline' ||
            ngModel.$modelValue.representationaData.spread.modifiedNodes.length
          ));
        $scope.usePolicy = !!ngModel.$modelValue.use;
      };

      // Trajectory editors
      function updateModelUsingTimeSeries (timeSeries) {
        var copyModelObj = JSON.parse(JSON.stringify(ngModel.$modelValue));
        if (!copyModelObj.representationaData) {
          copyModelObj.representationaData = {};
        }
        copyModelObj.terminalValue = timeSeries[timeSeries.length - 1][1];
        copyModelObj.representationaData.timeSeries = timeSeries;
        ngModel.$setViewValue(copyModelObj);
        ngModel.$setDirty();
        updateGradient(ngModel.$modelValue.terminalValue);
        $scope.userValue = factor * ngModel.$modelValue.terminalValue;
        $scope.editedTimeSeries = true;
      }

      function copyTimeSeriesInReduce (acum, step) {
        acum.push(step.slice(0));
        return acum;
      }

      $scope.showTrajectoryEditor = function () {
        var editor =  $uibModal.open({
          templateUrl : '/views/partials/modalD3.tmpl.html',
          controller : ['$scope', '$uibModalInstance', 'BaselineDataService', 'schemaObject', 'editable', function ($scope, $uibModalInstance, BaselineDataService, schemaObject, editable) {
            $scope.baselineData = BaselineDataService.createEmptyTimeSeries(2015, schemaObject.baseline, schemaObject.initialValue);
            $scope.range = {
              x : [
                $scope.baselineData[0][0],
                $scope.baselineData[$scope.baselineData.length - 1][0]
              ],
              y : [
                Math.min($scope.baselineData[0][1], schemaObject.min),
                Math.max($scope.baselineData[0][1], schemaObject.max)
              ]
            };
            $scope.factor = (schemaObject.target.__is_relative ? 100 : 1);

            var originalNodes = (
              ngModel.$modelValue.representationaData &&
              ngModel.$modelValue.representationaData.timeSeries &&
              ngModel.$modelValue.representationaData.timeSeries.length ?
                ngModel.$modelValue.representationaData.timeSeries.reduce(copyTimeSeriesInReduce, []) :
                BaselineDataService.createEmptyTimeSeries(ngModel.$modelValue.startYear, ngModel.$modelValue.terminalValue, schemaObject.initialValue)
            );

            $scope.reset = function () {
              $scope.timeSeries = originalNodes.reduce(copyTimeSeriesInReduce, []);
            };
            $scope.reset();

            $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };

            $scope.ok = function () {
              $uibModalInstance.close($scope.timeSeries);
            };

            $scope.editable = editable;
            $scope.moveFirst = false;
          }],
          size : 'lg',
          scope : $scope,
          resolve: {
            schemaObject : function () { return $scope.schemaObject; },
            editable : function () { return $scope.editable; }
          }
        });

        editor.result.then(updateModelUsingTimeSeries);
      };

      $scope.showManualTimeEditor = function () {
        var editor =  $uibModal.open({
          templateUrl : '/views/partials/modalD3ManualTimeEditor.tmpl.html',
          controller : ['$scope', '$uibModalInstance', 'BaselineDataService', 'schemaObject', function ($scope, $uibModalInstance, BaselineDataService, schemaObject) {
            var timeSeries = (
              ngModel.$modelValue.representationaData &&
              ngModel.$modelValue.representationaData.timeSeries &&
              ngModel.$modelValue.representationaData.timeSeries.length ?
                ngModel.$modelValue.representationaData.timeSeries.reduce(copyTimeSeriesInReduce, []) :
                BaselineDataService.createEmptyTimeSeries(ngModel.$modelValue.startYear, ngModel.$modelValue.terminalValue, schemaObject.initialValue)
            );

            if (schemaObject.target.__is_relative) {
              $scope.timeNodes = timeSeries.reduce(function (acum, step) {
                acum.push([step[0], Math.round(step[1] * 10000) / 100, step[2]]);
                return acum;
              }, []);
            } else {
              $scope.timeNodes = timeSeries;
            }

            $scope.deletePoint = function($index) {
              $scope.timeNodes.splice($index, 1);
            };

            $scope.addPoint = function() {
              $scope.timeNodes.push([0, 0, 0]);
            };

            $scope.checkAndReorder = function () {
              $scope.timeNodes = $scope.timeNodes.filter(function (item) {
                return (item[0] >= 2010 && item[0] <= 2050);
              })
              .map(function (item) {
                var factor = (schemaObject.target.__is_relative ? 100 : 1);


                item[0] = Math.floor(parseInt(item[0], 10));

                if (item[1] < schemaObject.min * factor) {
                  item[1] = schemaObject.min * factor;
                }

                if (item[1] > schemaObject.max * factor) {
                  item[1] = schemaObject.max * factor;
                }
                return item;
              });

              $scope.timeNodes.sort(function (a, b) {
                return a[0] - b[0];
              });

              for (var i = $scope.timeNodes.length - 1; i > 0; i -= 1) {
                if (parseInt($scope.timeNodes[i][0], 10) === parseInt($scope.timeNodes[i - 1][0], 10)) {
                  $scope.timeNodes.splice(i, 1);
                }
              }
            };

            $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };

            $scope.ok = function () {
              $scope.checkAndReorder();
              $uibModalInstance.close($scope.timeNodes.map(function (item) {
                if (schemaObject.target.__is_relative) {
                  return [item[0], Math.round(item[1] * 1000) / 100000, 0];
                }
                return item;
              }));
            };
          }],
          size : 'lg',
          resolve: {
            schemaObject : function () { return $scope.schemaObject; },
            editable : function () { return $scope.editable; }
          }
        });
        editor.result.then(updateModelUsingTimeSeries);
      };

      // Spatial editor
      $scope.showMapEditor = function () {
        var editor = $uibModal.open({
          templateUrl : '/views/partials/modalD3Map.tmpl.html',
          controller : ['$scope', '$uibModalInstance', 'BaselineDataService', 'schemaObject', 'editable', 'spatialAndTemporalDimensions', function ($scope, $uibModalInstance, BaselineDataService, schemaObject, editable, spatialAndTemporalDimensions) {
            // Configuration of the view
            $scope.editable = editable;
            $scope.moveFirst = false;
            $scope.factor = (schemaObject.target.__is_relative ? 100 : 1);
            $scope.disableNUTS2 = (
              spatialAndTemporalDimensions[schemaObject.target.__parameters[0]].spatial === 'country'
            );

            // TODO: Legacy dependency

            $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
            };

            $scope.ok = function () {
              var modifiedNodes = [];
              $scope.editedValuesTrie.keys.forEach(function (region) {
                var regionData = $scope.editedValuesTrie.get(region);
                if (regionData) {
                  modifiedNodes.push({
                    key : region,
                    value: regionData.value,
                    timeSeries : regionData.timeSeries.map(function (item) {
                      return item.slice(0, Math.min(3, item.length));
                    })
                  });
                }
              });
              $uibModalInstance.close([modifiedNodes, $scope.selectedSpread.id, $scope.spread, $scope.terminalValue, $scope.affineTransformationParams]);
            };

            // Set the copy of the model data;
            $scope.selectedSpread = 0;
            $scope.startYear = ngModel.$modelValue.startYear;
            $scope.spread = (
              ngModel.$modelValue.representationaData &&
              ngModel.$modelValue.representationaData.spread ?
              JSON.parse(JSON.stringify(ngModel.$modelValue.representationaData.spread)) :
              null
            );
            $scope.terminalValue = ngModel.$modelValue.terminalValue;
            $scope.affineTransformationParams = null;

            // Get EU28 time series and use as default
            $scope.EU28Nodes = (
              ngModel.$modelValue.representationaData &&
              ngModel.$modelValue.representationaData.timeSeries &&
              ngModel.$modelValue.representationaData.timeSeries.length ?
                ngModel.$modelValue.representationaData.timeSeries.reduce(copyTimeSeriesInReduce, []) :
                BaselineDataService.createEmptyTimeSeries(ngModel.$modelValue.startYear, ngModel.$modelValue.terminalValue, schemaObject.initialValue)
            );
            $scope.timeSeries = $scope.EU28Nodes;
            $scope.baselineData = BaselineDataService.createEmptyTimeSeries(2015, schemaObject.baseline, schemaObject.initialValue);
            $scope.range = {
              x : [
                $scope.baselineData[0][0],
                $scope.baselineData[$scope.baselineData.length - 1][0]
              ],
              y : [
                Math.min($scope.baselineData[0][1], schemaObject.min),
                Math.max($scope.baselineData[0][1], schemaObject.max)
              ]
            };
          }],
          size : 'lg',
          scope : $scope,
          resolve: {
            schemaObject : function () { return $scope.schemaObject; },
            editable : function () { return $scope.editable; },
            spatialAndTemporalDimensions : ['PackageService', function (PackageService) {
              return PackageService.getSpatialAndTemporalDimensions();
            }]
          }
        });
        editor.result.then(function (resultsEditor) {
            var copyModelObj = JSON.parse(JSON.stringify(ngModel.$modelValue));
            if (!copyModelObj.representationaData) {
              copyModelObj.representationaData = {};
            }
            if (!copyModelObj.representationaData.timeSeries) {
              copyModelObj.representationaData.timeSeries = [];
            }
            copyModelObj.representationaData.spread = {
              modifiedNodes : resultsEditor[0],
              transformation : (resultsEditor[4] && resultsEditor[4].length > 0 ? resultsEditor[4].slice(0) : null),
              id : (resultsEditor[1] === 'userDefined' ? resultsEditor[2].id : resultsEditor[1])
            };
            copyModelObj.terminalValue = resultsEditor[3];
            ngModel.$setViewValue(copyModelObj);
            $scope.editedMap = true;
            ngModel.$setDirty();
          });
      };
    }

    linkPolicyEditor.$inject = ['$scope', '$filter'];
    directive = {
      restrict: 'A',
      scope: {
        schemaObject : '=',
        allowSelect : '=',
        viewOnly : '@',
        hideTime : '@',
        hideSpatial : '@'
      },
      require : 'ngModel',
      templateUrl : '/views/partials/TPMEditor.tmpl.html',
      replace : true,
      link : linkPolicyEditor,
      transclude : true
    };
    return directive;
  }
  TPMRowDirective.$inject = ['$filter', '$uibModal', '$timeout'];

  angular.module('highToolClientApp')
    .directive('tpmRow', TPMRowDirective);

  angular.module('highToolClientApp')
    .directive('tpmRowDeviationBars', function() {
      return {
        restrict : 'E',
        require : 'ngModel',
        scope : {
          schemaObject : '='
        },
        template : '<table>' +
          '<tr>' +
            '<td style="text-align: right;">' +
              '<span ng-show="showLowerDeviation" ng-style="{width : lowerDeviationWidth}">&nbsp;</span>' +
            '</td>' +
            '<td style="width: 1px;"></td>' +
            '<td style="text-align: left;">' +
              '<span ng-hide="showLowerDeviation" ng-style="{width : upperDeviationWidth}">&nbsp;</span>' +
            '</td>' +
          '</tr>' +
        '</table>',
        link : function ($scope, element, attrs, ngModel) {
          var factor = ($scope.schemaObject.target.__is_relative ? 100 : 1);
          ngModel.$render = function () {
            $scope.showLowerDeviation = (ngModel.$modelValue / factor < $scope.schemaObject.baseline);
            $scope.lowerDeviationWidth = (Math.min(-(ngModel.$modelValue / (factor * $scope.schemaObject.baseline) - 1) * 100, 20) * 45 / 20) + 'px';
            $scope.upperDeviationWidth = (Math.min((ngModel.$modelValue / (factor * $scope.schemaObject.baseline) - 1) * 100, 20) * 45 / 20) + 'px';
          };
        }
      };
    });

}());
