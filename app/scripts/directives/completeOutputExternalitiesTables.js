(function () {
  'use strict';

  angular.module('highToolClientApp')
    .directive('externalityAirPollution', ['APIURL', '$http', '$q', function(APIURL, $http, $q) {
      return {
        restrict : 'E',
        scope : {
          runId : '@'
        },
        templateUrl : '/views/externalityTables/airPollutionTables.tmpl.html',
        link : function ($scope) {
          $scope.pollutantKeys = [
            'nmvocPollution',
            'noxPollution',
            'pm25Pollution',
            'so2Pollution'
          ];

          $q.all([
            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/airPollution/passenger'),
            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/airPollution/freight')
          ]).then(function (responses) {
            $scope.pollutionData = responses.map(function (response, index) {
              var transformedData = {
                years : response.data.data[0].values.reduce(function (acum, curr) {
                  acum.push(curr.year);
                  return acum;
                }, []),
                description : response.data.description,
                name : response.data.name,
                unit : response.data.unit,
                modes : (index === 0 ? ['road', 'rail', 'air'] : ['road', 'rail', 'air', 'iww', 'sss'])
              };

              response.data.data = response.data.data.map(function (item) {
                item.values = item.values.map(function (valueItem) {
                  valueItem.total = 0;
                  $scope.pollutantKeys.forEach(function (key) {
                    valueItem.total += valueItem[key];
                  });
                  return valueItem;
                });
                item.values.push(item.values.reduce(function (acum, curr) {
                  [
                    'nmvocPollution',
                    'noxPollution',
                    'pm25Pollution',
                    'so2Pollution',
                    'total'
                  ].forEach(function (key) {
                    acum[key] += curr[key];
                  });

                  return acum;
                }, {
                  nmvocPollution : 0,
                  noxPollution : 0,
                  pm25Pollution : 0,
                  so2Pollution : 0,
                  total : 0
                }));
                return item;
              });

              transformedData.total = new Array(transformedData.years.length + 1).fill(0);

              response.data.data.forEach(function (mode) {
                transformedData[mode.mode] = mode.values;
                transformedData[mode.mode].forEach(function (year, index) {
                  transformedData.total[index] += year.total;
                });
              });
              return transformedData;
            });
          });
        }
      };
    }]);

    angular.module('highToolClientApp')
      .directive('externalityClimateChange', ['APIURL', '$http', '$q', function(APIURL, $http, $q) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/climateChangeTables.tmpl.html',
          link : function ($scope) {
            $scope.pollutantKeys = [
              'climateChange'
            ];

            $q.all([
              $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/climateChange/passenger'),
              $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/climateChange/freight')
            ]).then(function (responses) {
              $scope.pollutionData = responses.map(function (response, index) {
                var transformedData = {
                  years : response.data.data[0].values.reduce(function (acum, curr) {
                    acum.push(curr.year);
                    return acum;
                  }, []),
                  description : response.data.description,
                  unit : response.data.unit,
                  modes : (index === 0 ? ['road', 'rail', 'air'] : ['road', 'rail', 'air', 'iww', 'sss']),
                  shortDescription : (index === 0 ? 'Passenger' : 'Freigth')
                };

                if (index === 0) {
                  $scope.totalClimateChange = new Array(transformedData.years.length + 1).fill(0);
                }
                transformedData.total = new Array(transformedData.years.length + 1).fill(0);

                response.data.data.forEach(function (mode) {
                  transformedData[mode.mode] = mode.values;
                  transformedData[mode.mode].push(transformedData[mode.mode].reduce(function (acum, curr) {
                    acum.climateChange += curr.climateChange;
                    return acum;
                  }, {climateChange : 0}));
                  transformedData[mode.mode].forEach(function (year, index) {
                    transformedData.total[index] += year.climateChange;
                    $scope.totalClimateChange[index] += year.climateChange;
                  });
                });
                return transformedData;
              });
            });
          }
        };
      }]);

    function SingleTableThenCb(response) {
      var externalityData = {
        years : response.data.data[0].values.reduce(function (acum, curr) {
          acum.push(curr.year);
          return acum;
        }, []),
        description : response.data.description,
        name : response.data.name,
        unit : response.data.unit,
        modes : this.modes,
        modesLabel : this.modesLabel
      };

      externalityData.total = new Array(externalityData.years.length + 1).fill(0);

      response.data.data.forEach(function (mode) {
        externalityData[mode.mode] = mode.values;
        var acumObj = {};
        acumObj[this.externalityKey] = 0;

        externalityData[mode.mode].push(externalityData[mode.mode].reduce(function (acum, curr) {
          acum[this.externalityKey] += curr[this.externalityKey];
          return acum;
        }.bind(this), acumObj));
      }.bind(this));

      externalityData.modes.forEach(function (mode) {
        externalityData[mode].forEach(function (year, index) {
          externalityData.total[index] += year[this.externalityKey];
        }.bind(this));
      }.bind(this));

      return Promise.resolve(externalityData);
    }

    angular.module('highToolClientApp')
      .directive('externalityUpDownStreams', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/singleTable.tmpl.html',
          link : function ($scope) {
            $scope.externalityKey = 'updownstream';

            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/upDownstream')
              .then(SingleTableThenCb.bind({
                externalityKey : $scope.externalityKey,
                modes : ['road', 'coach', 'truck', 'railPassenger', 'railFreight', 'air', 'iww'],
                modesLabel : ['road', 'coach', 'HDV', 'rail passenger', 'rail freight', 'air', 'IWW']
              }))
              .then(function (externalityData) {
                $scope.externalityData = externalityData;
              });
          }
        };
      }]);

    angular.module('highToolClientApp')
      .directive('externalityMarginalInfrastructure', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/singleTable.tmpl.html',
          link : function ($scope) {
            $scope.externalityKey = 'marginalinfrastructurecosts';

            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/marginalInfrastructureCosts')
              .then(SingleTableThenCb.bind({
                externalityKey : $scope.externalityKey,
                modes : ['cars', 'coaches', 'hdv', 'iww'],
                modesLabel : ['cars', 'coaches', 'HDV', 'IWW']
              }))
              .then(function (externalityData) {
                $scope.externalityData = externalityData;
              });
          }
        };
      }]);

    angular.module('highToolClientApp')
      .directive('externalityAccidents', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/singleTable.tmpl.html',
          link : function ($scope) {
            $scope.externalityKey = 'fatalities';

            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/externalities/accidents')
              .then(function (response) {
                var index = null;
                for (var i = 0, found = false, ln = response.data.data.length; i < ln && !found; i += 1) {
                  if (response.data.data[i].mode === 'car') {
                    index = i;
                    found = true;
                  }
                }
                // Create a mode to put in its fatalities the accidents to reuse the table function
                if (index !== null) {
                  response.data.data[index].mode = 'carFatalities';
                  response.data.data.push({
                    mode : 'carInjured',
                    values : response.data.data[index].values.map(function (item) {
                      return {
                        year : item.year,
                        fatalities : item.severeinjuries + item.slightinjuries
                      };
                    })
                  });
                }
                return Promise.resolve(response);
              })
              .then(SingleTableThenCb.bind({
                externalityKey : $scope.externalityKey,
                modes : ['carInjured', 'carFatalities', 'bike', 'p2w', 'pedestrian', 'publicTransport', 'rail', 'air', 'truck', 'iww', 'sss'],
                modesLabel : ['Car injured', 'Car fatalities', 'Bike fatalities', 'P2w fatalities', 'Pedestrian fatalities',
                  'Public transport fatalities', 'Rail fatalities', 'Air fatalities', 'Truck fatalities', 'IWW fatalities', 'SSS fatalities']
              }))
              .then(function (externalityData) {
                $scope.externalityData = externalityData;
              });
          }
        };
      }]);

    angular.module('highToolClientApp')
      .directive('generalizedCostsPassenger', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/singleTable.tmpl.html',
          link : function ($scope) {
            $scope.externalityKey = 'generalizedcosts';

            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/generalizedCosts/passenger')
              .then(SingleTableThenCb.bind({
                externalityKey : $scope.externalityKey,
                modes : ['road', 'rail', 'coach', 'air'],
                modesLabel : ['road', 'rail', 'coach', 'air']
              }))
              .then(function (externalityData) {
                $scope.externalityData = externalityData;
              });
          }
        };
      }]);

    angular.module('highToolClientApp')
      .directive('generalizedCostsFreight', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@'
          },
          templateUrl : '/views/externalityTables/singleTable.tmpl.html',
          link : function ($scope) {
            $scope.externalityKey = 'generalizedcosts';

            $http.get(APIURL + '/api/run/results/' + $scope.runId + '/generalizedCosts/freight')
              .then(SingleTableThenCb.bind({
                externalityKey : $scope.externalityKey,
                modes : ['road', 'rail', 'iww', 'sss', 'maritime'],
                modesLabel : ['road', 'rail', 'iww', 'sss', 'maritime']
              }))
              .then(function (externalityData) {
                $scope.externalityData = externalityData;
              });
          }
        };
      }]);

}());
