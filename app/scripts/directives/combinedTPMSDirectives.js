(function () {
  'use strict';

  angular.module('highToolClientApp')
    .directive('tpmDescription', function() {
      return {
        restrict : 'E',
        require : 'ngModel',
        scope : {
          schemaObject : '='
        },
        template : '<div class="panel panel-default">' +
                      '<div class="panel-heading">' +
                      '<label style="vertical-align: middle;">'+
                        '<input type="checkbox" style="margin-right: 10px;" ng-model="selected" ng-change="changed(selected)"></input>' +
                        '<strong>{{schemaObject.label}}</strong>' +
                        '</label>' +
                      '</div>' +
                      '<div class="panel-body">' +
                      '{{schemaObject.description}}' +
                      '</div>' +
                    '</div>',
        link : function ($scope, element, attrs, ngModel) {
          ngModel.$render = function () {
            $scope.selected = ngModel.$modelValue;
          };
          $scope.changed = function (newValue) {
            ngModel.$setViewValue(newValue);
          };
        }
      };
    });
}());
