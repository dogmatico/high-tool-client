(function () {
  'use strict';

  angular.module('highToolClientApp')
    .directive('leverlessTpmList', function() {
      return {
        restrict : 'E',
        transclude : true,
        require : 'ngModel',
        scope : {
          fullSchema : '=',
          adjacencyList : '=',
          viewOnly : '@'
        },
        template : '<div class="panel panel-default">' +
          '<div class="panel-heading">' +
            '<strong>Policy without levers</strong>' +
          '</div>' +
          '<div class="table-responsive">' +
            '<table class="table">' +
              '<thead>' +
                '<tr>' +
                  '<th ng-hide="viewOnly">Use</th>' +
                  '<th>TPM Name</th>' +
                '</tr>' +
              '</thead>' +
            '<tbody>' +
              '<tr ng-repeat="localId in adjacencyList">' +
                '<td ng-hide="viewOnly"><input type="checkbox" ng-model="selectedTPM[$index].use" ng-change="changed(selectedTPM[$index].use, $index)"></input></td>' +
                '<td>' +
                  '<button style="border: none; background-color: transparent;" uib-popover-trigger="mouseenter" uib-popover-placement="right" uib-popover="{{fullSchema.nodeAttributes[localId].description}}" uib-popover-title="{{fullSchema.nodeAttributes[localId].shortLabel}}">' +
                    '<span class="glyphicon glyphicon-question-sign"></span>' +
                  '</button>' +
                  '<strong ng-bind-html="fullSchema.nodeAttributes[localId].label"></strong>' +
                '</td>' +
              '</tr>' +
              '</tbody>' +
            '</table>' +
          '</div>' +
        '</div>',
        link : function ($scope, element, attrs, ngModel) {
          $scope.viewOnly = ($scope.viewOnly === 'true' ? true : false);
          ngModel.$render = function () {
            var selectedTPM = [];
            $scope.adjacencyList.forEach(function (localId) {
              selectedTPM.push({
                localId : localId,
                use : !!ngModel.$modelValue[localId].use
              });
            });
            $scope.selectedTPM = selectedTPM;
          };
          $scope.changed = function (newValue, index) {
            var valueObj = ngModel.$modelValue;
            valueObj[$scope.selectedTPM[index].localId].use = newValue;
            ngModel.$setViewValue(valueObj);
          };
        }
      };
    });
}());
