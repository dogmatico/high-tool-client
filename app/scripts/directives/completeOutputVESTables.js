(function () {
  'use strict';

  angular.module('highToolClientApp')
    .directive('vesCountryTable', ['APIURL', '$http', '_MODEL_LAST_YEAR', '_MODEL_COUNTRIES_ORDER', function(APIURL, $http, _MODEL_LAST_YEAR, _MODEL_COUNTRIES_ORDER) {
      return {
        restrict : 'E',
        scope : {
          runId : '@',
          type : '@'
        },
        templateUrl : '/views/vesTables/vesCountryTable.tmpl.html',
        link : function ($scope) {
          var dataRequest, keysProperty;
          switch ($scope.type) {
            case 'modes':
              dataRequest = $http.get(APIURL + '/api/run/results/' + $scope.runId + '/VES/modesByCountry');
              keysProperty = 'modeTypes';
              $scope.waitingMsg = 'Waiting for modal year data';
              break;
            case 'fuels':
              dataRequest = $http.get(APIURL + '/api/run/results/' + $scope.runId + '/VES/fuelsByCountry');
              keysProperty = 'fuelTypes';
              $scope.waitingMsg = 'Waiting for fuel year data';
              break;
            default:
              throw new Error('Invalid VES type. Select modes or fuels.');
          }

          dataRequest.then(function (response) {
            $scope.keys = response.data[keysProperty];
            $scope.year = _MODEL_LAST_YEAR;
            $scope.tableHeader = response.data.description;
            var data = response.data.data.map(function (item) {
              var lastYear = item.values.filter(function (year) {
                return parseInt(year.year, 10) === _MODEL_LAST_YEAR;
              });
              item.values = (lastYear.length ? lastYear[0] : null);
              return item;
            });
            data.sort(function (a,b) {
              var aPriority = _MODEL_COUNTRIES_ORDER(a.country),
                bPriority = _MODEL_COUNTRIES_ORDER(b.country);

              return (aPriority !== bPriority ? aPriority - bPriority :
                  (a.country > b.country ? 1 : -1)
                );
            });
            $scope.data = data;
            $scope.tableUnit = '%';
          })
          .catch(function (err) {
            throw new Error(err);
          });

        }
      };
    }]);

    angular.module('highToolClientApp')
      .directive('vesYearTable', ['APIURL', '$http', function(APIURL, $http) {
        return {
          restrict : 'E',
          scope : {
            runId : '@',
            type : '@'
          },
          templateUrl : '/views/vesTables/vesYearTable.tmpl.html',
          link : function ($scope) {
            var dataRequest, keysProperty;
            switch ($scope.type) {
              case 'modes':
                dataRequest = $http.get(APIURL + '/api/run/results/' + $scope.runId + '/VES/modesByYear');
                keysProperty = 'modeTypes';
                $scope.waitingMsg = 'Waiting for modal country data';
                break;
              case 'fuels':
                dataRequest = $http.get(APIURL + '/api/run/results/' + $scope.runId + '/VES/fuelsByYear');
                keysProperty = 'fuelTypes';
                $scope.waitingMsg = 'Waiting for fuel country data';
                break;
              default:
                throw new Error('Invalid VES type. Select modes or fuels.');
            }

            dataRequest.then(function (response) {
              $scope.keys = response.data[keysProperty];
              $scope.tableHeader = response.data.description;
              $scope.data = response.data.data;
              $scope.tableUnit = '%';
            })
            .catch(function (err) {
              throw new Error(err);
            });
          }
        };
      }]);

}());
