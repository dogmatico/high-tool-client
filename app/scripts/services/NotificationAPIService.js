/*jslint browser: true, indent: 2*/
/*global angular, confirm, alert*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.NotificationAPIService
   * @description
   * # Service used to manage subscriptions and notifications
   * Service in the highToolClientApp.
   */

  function NotificationMock(title, options) {

    this.permission = 'false';

    this.requestPermission = function () {
      return {
        then : function () {
          return false;
        }
      };
    };
  }

  function NotificationService($rootScope, $window) {
    this.notificationAPI = $window.Notification || NotificationMock;
    this.$rootScope = $rootScope;
    return;
  }

  NotificationService.prototype.sendNotification = function (title, body, icon) {
    return new this.notificationAPI(title, {body : body || '', icon : icon || ''});
  };

  NotificationService.prototype.hasPermission = function () {
    return (this.notificationAPI.permission === 'granted');
  };

  NotificationService.prototype.requestPermission = function () {
    return this.notificationAPI.requestPermission();
  };

  NotificationService.$inject = ['$rootScope', '$window'];

  angular.module('highToolClientApp')
    .service('NotificationAPIService', NotificationService);
}());
