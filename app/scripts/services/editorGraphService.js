/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name highToolClientApp.editorGraphService
   * @description
   * # Injects functions required to draw and edit trajectories
   * Service in the highToolClientApp.
   */


  function avalHorner(x, n, coeff) {
    var res = coeff[n - 1][1], i;

    for (i = n - 2; i > -1; i -= 1) {
      res = coeff[i][1] + res * (x - coeff[i][0]);
    }
    return res;
  }

  function HermiteInterpolation(intData) {
    var i, j, coeff = [], dx;

    for (i = 0; i < 2; i += 1) {
      coeff[2 * i] = [intData[i][0], intData[i][1]];
      coeff[2 * i + 1] = [intData[i][0], intData[i][1]];
    }

    for (i = 1; i < 4; i += 1) {
      for (j = 3; j >= i; j -= 1) {
        dx = (coeff[j][0] - coeff[j - i][0]);
        if (i === 1 && j % 2 === 1) {
          coeff[j][1] = intData[Math.max(j - 2, 0)][2];
        } else {
          coeff[j][1] = (coeff[j][1] - coeff[j - 1][1]) / dx;
        }
      }
    }
    return coeff;
  }


  function NodeList(initialData, scaleX, scaleY) {
    this.nodes = initialData;

    if (scaleX) {
      this.scaleX = scaleX;
    } else {
      this.scaleX = function (x) { return x; };
    }

    if (scaleY) {
      this.scaleY = scaleY;
    } else {
      this.scaleY = function (y) { return y; };
    }

    this.pointId = 2;
    this.pathId = 0;

    this.nodes[0][2] = (this.nodes[1][1] - this.nodes[0][1]) / (this.nodes[1][0] - this.nodes[0][0]);
    this.nodes[1][2] = this.nodes[0][2];

    this.nodes[0][3] = 0;
    this.nodes[1][3] = 1;
    this.coefficients = [new HermiteInterpolation([this.nodes[0], this.nodes[1]])];

    this.plots = [];
    return;
  }

  NodeList.prototype.addNode = function (nodeData) {
    var i;

    // Check bounds
    if (nodeData[0] < this.nodes[0][0] || nodeData[0] > this.nodes[this.nodes.length - 1][0]) {
      throw new Error('You cannot insert new points outside the initial range.');
    }

    // Seek enter point;
    for (i = 0; nodeData[0] > this.nodes[i + 1][0]; i += 1) {}

    // insert afer node i;
    nodeData[3] = this.pointId;
    this.nodes.splice(i + 1, 0, nodeData);
    this.coefficients.splice(i, 0, new HermiteInterpolation([this.nodes[i], this.nodes[i + 1]]));
    this.updateCoefficients(i + 1);

    this.plots.splice(i, 1, [this.pathId, this.avalSegment(i)]);
    this.plots.splice(i + 1, 0, [this.pathId + 1, this.avalSegment(i + 1)]);

    this.pathId += 2;
    this.pointId += 1;
    return;
  };

  NodeList.prototype.avalPoint = function (x) {
    var i;
    for (i = 0; x > this.nodes[i + 1][0]; i += 1) {}
    return avalHorner(x, 4, this.coefficients[i]);
  };

  NodeList.prototype.avalSegment = function (n) {
    var i, dH, h, minTol = 4, dx, dy,
      tol = 25,
      result = [],
      valTemp = this.nodes[n][1],
      absDiff = 0;

    result[0] = [this.nodes[n][0], this.nodes[n][1]];

    for (i = 1, dH = 1, h = this.nodes[n][0]; h + dH < this.nodes[n + 1][0]; i += 1) {
      valTemp = avalHorner(h + dH, 4, this.coefficients[n]);
      // Set step size targeting between 2 and 5 pixels width using scale functions
      dx = this.scaleX(h + dH) - this.scaleX(result[i - 1][0]);
      dy = this.scaleY(valTemp) - this.scaleY(result[i - 1][1]);
      absDiff = dx * dx + dy * dy;

      if (absDiff > tol) {
        i -= 1;
        dH = dH / 2;
      } else {
        result[i] = [h, valTemp];
        h += dH;
        if (absDiff < minTol) {
          dH = dH * 2;
        }
      }
    }
    result[i] = [this.nodes[n + 1][0], this.nodes[n + 1][1]];
    return result;
  };

  NodeList.prototype.deleteNode = function (n) {
    // Terminal nodes are protected
    if (n === 0 || n === this.nodes.length - 1) {
      return false;
    }

    this.nodes.splice(n, 1);
    this.updateCoefficients(n - 1);
    this.coefficients.splice(n, 1);

    this.pathId += 1;
    this.plots.splice(n - 1, 2, [this.pathId, this.avalSegment(n - 1)]);

    return true;
  };

  NodeList.prototype.reset = function () {
    var i, n = this.nodes.length - 1,
      slope = (this.nodes[n][1] - this.nodes[0][1]) / (this.nodes[n][0] - this.nodes[0][0]);

    for (i = 1; i < n; i += 1) {
      this.deleteNode(1);
    }

    for (i = 0; i < 2; i += 1) {
      this.updatePoint(i, [this.nodes[i][0], this.nodes[i][1], slope]);
    }
    return;
  };

  NodeList.prototype.setScaleFunctions = function (scaleX, scaleY) {
    var i, ln;

    this.scaleX = scaleX;
    this.scaleY = scaleY;

    for (i = 0, ln = this.nodes.length - 1, this.plots = []; i < ln; i += 1, this.pathId += 1) {
      this.plots.push([this.pathId + 1, this.avalSegment(i)]);
    }

    return;
  };

  NodeList.prototype.updateCoefficients = function (n) {
    this.coefficients[n] = new HermiteInterpolation([this.nodes[n], this.nodes[n + 1]]);
    return;
  };

  NodeList.prototype.updatePoint = function (n, newData) {
    this.nodes[n][0] = parseFloat(newData[0]);
    this.nodes[n][1] = parseFloat(newData[1]);
    this.nodes[n][2] = parseFloat(newData[2]);

    if (n !== 0) {
      this.updateCoefficients(n - 1);
      this.plots.splice(n - 1, 1, [this.pathId, this.avalSegment(n - 1)]);
      this.pathId += 1;
    }

    if (n !== this.nodes.length - 1) {
      this.updateCoefficients(n);
      this.plots.splice(n, 1, [this.pathId, this.avalSegment(n)]);
      this.pathId += 1;
    }
    return;
  };

  function GraphService() {
    return;
  }

  GraphService.prototype.createNodeList = function (initialData) {
    return new NodeList(initialData);
  };

  angular.module('highToolClientApp')
    .service('editorGraphService', GraphService);
}());
