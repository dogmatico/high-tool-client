(function () {
  'use strict';

  function CombinedTPMService($q, PackageService, _TPM_PREFIX, _MODEL_DEFAULT_FIRST_COMPUTE_YEAR) {
    this.PackageService = PackageService;
    this.Promise = $q;
    this._TPM_PREFIX = _TPM_PREFIX;
    this._MODEL_DEFAULT_FIRST_COMPUTE_YEAR = _MODEL_DEFAULT_FIRST_COMPUTE_YEAR;

    this.schema = new this.Promise(function (resolve, reject) {
      PackageService.getSchema('policy')
        .then(function (schema) {
          schema.adjList[0].forEach(function (familyIndex) {
            schema.adjList[familyIndex].forEach(function (tpmIndex) {
              PackageService.moveBranch(schema, tpmIndex, PackageService._typeToRoot('policy'), 'uuid');
            });
            PackageService.unlinkNodeFromSchema(schema, familyIndex);
          });
          resolve(schema);
        })
        .catch(reject);
    });

    this.leverList = PackageService.getleverList();
    this.combinations = PackageService.getTPMCombinations();
  }

  Object.defineProperties(CombinedTPMService.prototype, {
    '_findIfAllinArray' : {
      enumerable : false,
      value : function (arr, where) {
        if (!Array.isArray(arr) || !Array.isArray(where)) {
          throw new Error('Argument error: provide 2 arrays');
        }
        var ordArr = arr.slice(0).sort();
        var ordWhere = where.slice(0).sort();
        var j = 0, lnArr = ordArr.length;
        for (var i = 0, ln = ordWhere.length; i < ln && j !== lnArr; i += 1) {
          if (ordArr[j] === ordWhere[i]) {
            j += 1;
          }
        }
        return (j === lnArr);
      }
    },
    'createMinimalSchemaFromRun' : {
      enumerable : true,
      value : function (runData) {
        return new this.Promise(function (resolve, reject) {
          var minSchema = {
            nodeAttributes : [{
              uuid : 'ROOT'
            },{
              uuid : 'TPMS'
            },{
              uuid : 'LEVERS'
            }],
            adjList : [[1,2], [], []]
          };
          this.Promise.all([
            this.schema,
            this.combinations,
            this.leverList
          ])
            .then(function (promises) {
              var schema = promises[0];
              var combinations = promises[1];
              var levers = promises[2];

              var usedTPMs = runData.TPM.map(function (tpmId) {
                return schema.nodeAttributes[schema.dictionary[this._TPM_PREFIX + tpmId]];
              }.bind(this));

              usedTPMs.forEach(function (tpm) {
                minSchema.nodeAttributes.push(tpm);
                minSchema.adjList[1].push(minSchema.nodeAttributes.length - 1);
              });

              var selectedLevers = runData.TPM.reduce(function (acum, tpmId) {
                schema.adjList[schema.dictionary[this._TPM_PREFIX + tpmId]]
                  .forEach(function (index) {
                    if (!acum[schema.nodeAttributes[index].leverId]) {
                      acum[schema.nodeAttributes[index].leverId] = {
                        tpm : [],
                        leverValuesUuid : [],
                        id : schema.nodeAttributes[index].leverId
                      };
                    }
                    acum[schema.nodeAttributes[index].leverId].tpm.push(
                      schema.nodeAttributes[index].parent.substr(this._TPM_PREFIX.length)
                    );
                    acum[schema.nodeAttributes[index].leverId].leverValuesUuid.push(
                      schema.nodeAttributes[index].uuid
                    );
                  }.bind(this), {});
                return acum;
              }.bind(this), {});

              Object.keys(selectedLevers).forEach(function (leverId) {
                var i, ln, found;
                var applicationOrder = selectedLevers[leverId].tpm;
                var aggFun = function (leverList) {
                  var isRelative = leverList[0].target.__is_relative;

                  return (isRelative ?
                    leverList.reduce(function(bounds, lever) {
                      bounds[0] += lever.min - 1;
                      bounds[1] += lever.max - 1;
                      return bounds;
                    }, [0, 0])
                    .map(function (bound) {
                      return bound + 1;
                    }) :
                    leverList.reduce(function(bounds, lever) {
                      bounds[0] += lever.min;
                      bounds[1] += lever.max;
                      return bounds;
                    }, [0, 0])
                  );
                };

                if (combinations[leverId]) {
                  for (i = 0, ln = combinations[leverId].length, found = false; i < ln && !found; i += 1) {
                    found = this._findIfAllinArray(combinations[leverId][i].policies, runData.TPM);
                  }
                   if (i !== ln) {
                     aggFun = combinations[leverId][i].formula;
                     applicationOrder = combinations[leverId][i].policies.slice(0);
                   }
                }
                var schemaObjsList = applicationOrder.map(function (tpmId) {
                  if (!schema.nodeAttributes[schema.dictionary[this._TPM_PREFIX + tpmId + '_' + leverId]]) {
                    return levers.list[levers.dictionary[leverId]];
                  }
                  return schema.nodeAttributes[schema.dictionary[this._TPM_PREFIX + tpmId + '_' + leverId]];
                }.bind(this));

                var bounds = aggFun(schemaObjsList);
                var copySchema = JSON.parse(JSON.stringify(levers.list[levers.dictionary[leverId]]));
                copySchema.min = bounds[0];
                copySchema.max = bounds[1];

                copySchema.values = {
                  startYear : this._MODEL_DEFAULT_FIRST_COMPUTE_YEAR,
                  representationaData : {
                    timeSeries : []
                  },
                  terminalValue : copySchema.baseline
                };

                for (i = 0, ln = runData.parameters.length, found = false; i < ln && !found; i += 1) {
                  if (runData.parameters[i].uuid === leverId) {
                    found = true;
                  }
                }
                if (found) {
                  copySchema.values = runData.parameters[i - 1].values;
                }

                minSchema.nodeAttributes.push(copySchema);
                minSchema.adjList[2].push(minSchema.nodeAttributes.length - 1);
              }.bind(this));
              resolve(minSchema);
            }.bind(this))
            .catch(reject);
        }.bind(this));
      }
    },
    'createCombinedSchema' : {
      enumerable : true,
      value : function (selectedTPMList, selectedLeversList, selectedLeversObj, originalSchema, combinations) {
        var listLeversObjs = [];

        var aggFun = function (leverList) {
          var isRelative = leverList[0].target.__is_relative;

          return (isRelative ?
            leverList.reduce(function(bounds, lever) {
              bounds[0] += lever.min - 1;
              bounds[1] += lever.max - 1;
              return bounds;
            }, [0, 0])
            .map(function (bound) {
              return bound + 1;
            }) :
            leverList.reduce(function(bounds, lever) {
              bounds[0] += lever.min;
              bounds[1] += lever.max;
              return bounds;
            }, [0, 0])
          );
        };

        selectedLeversList.forEach(function (leverIndex) {
          var leverId = originalSchema.nodeAttributes[leverIndex].leverId;
          var applicationOrder = selectedLeversObj[leverId].tpm;
          if (combinations[leverId]) {
            var i, ln, found;
            for (i = 0, ln = combinations[leverId].length, found = false; i < ln && !found; i += 1) {
              found = this._findIfAllinArray(combinations[leverId][i].policies, selectedTPMList);
            }
             if (i !== ln) {
               aggFun = combinations[leverId][i].formula;
               applicationOrder = combinations[leverId][i].policies.slice(0);
             }
          }
          var schemaObjsList = applicationOrder.map(function (tpmId) {
            return originalSchema.nodeAttributes[originalSchema.dictionary[this._TPM_PREFIX + tpmId + '_' + leverId]];
          });
          var bounds = aggFun(schemaObjsList);
          var copySchema = JSON.parse(JSON.stringify(schemaObjsList[0]));
          copySchema.min = bounds[0];
          copySchema.max = bounds[1];
          listLeversObjs.push(copySchema);
        }.bind(this));
         return listLeversObjs;
      }
    }
  });

  CombinedTPMService.$inject = ['$q', 'PackageService', '_TPM_PREFIX', '_MODEL_DEFAULT_FIRST_COMPUTE_YEAR'];

  angular.module('highToolClientApp')
    .service('CombinedTPMService', CombinedTPMService);
}());
