(function () {
  'use strict';


  function PackageService ($http, $q, APIURL) {
    this.$http = $http;
    this.Promise = $q;
    this.APIURL = APIURL;
  }

  Object.defineProperties(PackageService.prototype, {
    '_typeToRoot' : {
      enumerable : false,
      value : function (packageType) {
        var result = null;
        switch (packageType.toLowerCase()) {
        case 'policy':
          result = 'TPMROOT';
          break;
        case 'output':
          result = 'OUROOT';
          break;
        case 'custom':
          result = 'CUSTOMTPMROOT';
          break;
        case 'combined':
          result = 'COMBINEDTPMROOT';
          break;
        case 'TPMRun':
          result = 'TPMROOT';
          break;
        }
        return result;
      }
    },
    'unlinkNodeFromSchema' : {
      enumerable : true,
      value : function (schema, nodeId, idAttribute) {
        idAttribute = idAttribute || 'id';
        var index = (Number.isInteger(nodeId) && schema.nodeAttributes[nodeId] ?
          nodeId :
          schema.dictionary[nodeId][idAttribute]);

        var nodeData = schema.nodeAttributes[index];
        if (nodeData.parent === null) {
          schema.rootNodes = schema.rootNodes.filter(function (rootNode) {
            return (rootNode.localId !== index);
          });
        } else {
          var parentIndex = (Number.isInteger(schema.dictionary[nodeData.parent]) ? schema.dictionary[nodeData.parent] : schema.dictionary[nodeData.parent].id);
          schema.adjList[parentIndex] = schema.adjList[parentIndex].filter(function (parentChildren) {
            return (parentChildren !== index);
          });
        }
      }
    },
    'getSchema' : {
      enumerable : true,
      value : function (rootNodeOrType) {
        var rootNode = this._typeToRoot(rootNodeOrType) || rootNodeOrType;
        if (!rootNode) {
          return new Error('Missing or invalid root node or package type.');
        }
        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/schema/' + rootNode)
            .then(function (response) {
              response.data.dictionary = response.data.nodeAttributes.reduce(function (acum, curr, index) {
                acum[curr.uuid] = index;
                return acum;
              }, {});
              resolve(response.data);
            })
            .catch(function (err) {
              reject(err);
            });
        }.bind(this));
      }
    },
    'getleverList' : {
      enumerable : true,
      value : function () {
        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/schema/lever')
            .then(function (response) {
              resolve({
                list : response.data.leverList,
                dictionary : response.data.leverDictionary
              });
            })
            .catch(reject);
        }.bind(this));
      }
    },
    'getRun' : {
      enumerable : true,
      value : function (runId) {
        return new this.Promise(function (resolve, reject) {
          var retObj = {};
          this.$http.get(this.APIURL + '/api/run/definition/' + runId)
            .then(function (runDefRes) {
              retObj.runData = runDefRes.data;
              return this.getSchema(
                (retObj.runData.rootNode === 'COMBINEDTPMROOT' ? 'policy' : retObj.runData.rootNode)
              );
            }.bind(this))
            .then(function (schema) {
              if (retObj.runData.rootNode === 'COMBINEDTPMROOT') {
                schema.adjList[0].forEach(function (familyIndex) {
                  schema.adjList[familyIndex].forEach(function (tpmIndex) {
                    this.moveBranch(schema, tpmIndex, this._typeToRoot('policy'), 'uuid');
                  }.bind(this));
                  this.unlinkNodeFromSchema(schema, familyIndex);
                }.bind(this));
              }
              retObj.schema = schema;
              resolve(retObj);
            }.bind(this))
            .catch(reject);
        }.bind(this));
      }
    },
    'getPackage' : {
      enumerable : true,
      value : function (packageId, schemaLess) {
        return new this.Promise(function (resolve, reject) {

          this.$http.get(this.APIURL + '/api/package/' + packageId)
            .then(function (resPack) {
              var retObj = resPack.data;

              if (schemaLess) {
                resolve(retObj);
              } else {
                this.$http.get(this.APIURL + '/api/schema/' + this._typeToRoot(retObj.rootNode))
                  .then(function (resSchema) {
                    retObj.schema = resSchema.data;

                    retObj.schema.dictionary = retObj.schema.nodeAttributes.reduce(function (acum, curr, index) {
                      acum[curr.uuid] = curr;
                      acum[curr.uuid].index = index;
                      return acum;
                    }, {});
                    resolve(retObj);
                  })
                  .catch(reject);
              }
              return ;
            }.bind(this))
            .catch(reject);
        }.bind(this));
      }
    },
    'getSpatialAndTemporalDimensions' : {
      enumerable : true,
      value : function () {
        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/schema/tpm/spatialAndTemporalDimensions')
            .then(function (response) {
              resolve(response.data);
            })
            .catch(reject);
        }.bind(this));
      }
    },
    'getTPMCombinations': {
      enumerable : true,
      value : function () {
        function CombinationFormula(type) {
          var retFunc;
          switch (type) {
            case 1:
              /**
               * Limited contribution of subsequent TPM's (that is, the highest policy lever counts for the
               * full 100%, the second for 50, the third for 25% , and so on. That, the contribution of every
               * next policy lever is a factor two lower than the one before). Please note, that the order of the
               * TPM’s in column 1 is not necessarily the order in which this rule should be applied;
               */
              retFunc = function (leverList) {
                console.log(leverList);
                var isRelative = leverList[0].target.__is_relative;

                var bounds = leverList.reduce(function (acum, item) {
                  acum.push([item.min, item.max]);
                  return acum;
                }, [])
                .sort(function (a,b) {
                  return b[1] - a[1];
                });

                if (isRelative) {
                  bounds = bounds.map(function (item) {
                    return [
                      item[0] - 1,
                      item[1] - 1
                    ];
                  });
                }

                var bound = bounds.reduce(function (acum, item, index) {
                  if (index === 0) {
                    acum[0] = item;
                  } else {
                    acum[1] = acum[1] / 2;
                    acum[0][0] += item[0] / acum[1];
                    acum[0][1] += item[1] / acum[1];
                  }
                  return acum;
                }, [[], 1])[0];

                return (isRelative ? [
                  bound[0] + 1,
                  bound[1] + 1
                ] : bound);
              };
              break;
            case 2:
              /* Maximum policy ever value (that is, the highest value of the
               * policy lever is taken);
               **/
              retFunc = function (leverList) {
                return leverList.reduce(function (bounds, lever, index) {
                  if (index === 0) {
                    bounds = [
                      lever.min,
                      lever.max
                    ];
                  } else {
                    if (lever.min < bounds[0]) {
                      bounds[0] = lever.min;
                    }
                    if (lever.max > bounds[1]) {
                      bounds[1] = lever.max;
                    }
                  }
                  return bounds;
                }, []);
              };
              break;
            case 3:
              /**
               * Combined methods (that is, the first two policy lever values are combined by means
               * of method 1 (limited); the result is than combined with the value of the third policy
               * lever with method 2 (maximum). The TPM’s in the first column are ordered correctly to apply this rule.
               */
               retFunc = function (leverList) {
                 var isRelative = leverList[0].target.__is_relative;

                 var bounds = (isRelative ? [
                   leverList[0].min - 1,
                   leverList[0].max - 1
                 ] : [
                   leverList[0].min,
                   leverList[0].max
                 ]);

                 if (leverList.length > 1) {
                   leverList[0].min += leverList[1].min / 2;
                   leverList[0].max += leverList[1].max / 2;
                 }

                 for (var i = 2, ln = leverList.length; i < ln; i += 1) {
                   if (bounds[0] > leverList[i].min) {
                     bounds[0] = leverList[i].min;
                   }
                   if (bounds[1] < leverList[i].max) {
                     bounds[1] = leverList[i].max;
                   }
                 }

                 if (isRelative) {
                   return [bounds[0] + 1, bounds[1] + 1];
                 }
                 return bounds;
               };
              break;
            default:
          }
          return retFunc;
        }

        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/schema/tpm/combinations')
            .then(function (response) {
              resolve(response.data.reduce(function (acum, combination) {
                  if (!acum[combination.policy_lever_id]) {
                    acum[combination.policy_lever_id] = [];
                  }
                  acum[combination.policy_lever_id].push({
                    policies : combination.policies,
                    formula : new CombinationFormula(combination.formula)
                  });
                  return acum;
                }, {}));
            })
            .catch(reject);
        }.bind(this));
      }
    },
    'mixSchemaAndData' : {
      enumerable : true,
      value : function (schema, data, idAttribute) {
        var mixedObj = JSON.parse(JSON.stringify(schema));
        idAttribute = idAttribute || 'id';
        if (data && Array.isArray(data.parameters)) {
          data.parameters.forEach(function (lever) {
            var index = mixedObj.dictionary[lever[idAttribute]] || null;
            if (index && mixedObj.nodeAttributes[index]) {
              mixedObj.nodeAttributes[index].values = lever.values;
            }
          });
        }
        return mixedObj;
      }
    },
    'moveBranch' : {
      enumerable : true,
      value : function (schema, nodeId, newParent, idAttribute) {
        idAttribute = idAttribute || 'id';
        var index = (Number.isInteger(nodeId) && schema.nodeAttributes[nodeId] ?
          nodeId :
          schema.dictionary[nodeId]);
        var nodeData = schema.nodeAttributes[index];
        if (nodeData.parent === null) {
          schema.rootNodes = schema.rootNodes.filter(function (rootNode) {
            return (rootNode.localId !== index);
          });
        } else {
          var adjListIndex = (Number.isInteger(schema.dictionary[nodeData.parent]) ? schema.dictionary[nodeData.parent] : schema.dictionary[nodeData.parent].id);
          schema.adjList[adjListIndex] = schema.adjList[adjListIndex].filter(function (parentChildren) {
            return (parentChildren !== index);
          });
        }

        if (newParent === null) {
          schema.rootNodes.push({
            localId: index,
            userId : nodeData.uuid
          });
        } else {
          schema.adjList[(Number.isInteger(schema.dictionary[newParent]) ? schema.dictionary[newParent] : schema.dictionary[newParent].id)].push(index);
        }
      }
    },
    '_saveCombindedTPM' : {
      enumerable : false,
      value : function (packageId, packageData) {
        return new this.Promise(function (resolve, reject) {
          var payload = {
            rootNode : this._typeToRoot('combined'),
            data : packageData.data
          };

          ['label', 'headline', 'abstract'].forEach(function (key) {
            payload[key] = packageData[key] || 'No ' + key + ' added';
          });

          var request = (packageId && packageId !== 'new' ?
            this.$http.put(this.APIURL + '/api/package/' + packageId, payload) :
            this.$http.post(this.APIURL + '/api/package', payload)
          );

          request.then(function (response) {
            resolve(response.data);
          })
          .catch(reject);
        }.bind(this));
      }
    },
    'savePackage' : {
      enumerable : true,
      value : function (packageId, type, packageData) {
        var retFunc;
        switch (type) {
          case 'combined':
            retFunc = this._saveCombindedTPM.bind(this);
            break;
          default :
          throw new Error('Wrong package type ' + type);
        }
        return retFunc(packageId, packageData);
      }
    }
  });

  PackageService.$inject = ['$http', '$q', 'APIURL'];

  angular.module('highToolClientApp')
    .service('PackageService', PackageService);

}());
