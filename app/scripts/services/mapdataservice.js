/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name highToolClientApp.mapDataService
   * @description
   * # mapDataService
   * Service in the highToolClientApp.
   */

  function MapDataService ($http, $q, dataStructuresService, APIURL) {
    this.Promise = $q;
    this.$http = $http;
    this.dataStructuresService = dataStructuresService;
    this.APIURL = APIURL;
    this._fetchedStatistics = {};
  }

  Object.defineProperties(MapDataService.prototype, {
    'getHyperNetwork' : {
      enumerable : true,
      value : function (mode, schemaId) {
        var url = this.APIURL + '/api/HyperNetwork' + (schemaId ? '/' + schemaId : '');
        return new this.Promise(function (resolve, reject) {
          this.$http.get(url, {
            cache : false,
            params : {
              mode_id : mode
            }
          })
          .then(function (response) {
            resolve(response.data);
          })
          .catch(function (err) {
            reject(err);
          });
        }.bind(this));
      }
    },
    'getMap' : {
      enumerable : true,
      value : function (regionId, admLevel, format) {
        format = format || 'geojson';

        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/map/' + regionId + '/' + admLevel, {
            cache : true,
            params : {
              format : format
            }
          })
          .then(function (response) {
            resolve(response.data);
          })
          .catch(function (err) {
            reject(err);
          });
        }.bind(this));
      }
    },
    'getStatistics' : {
      enumerable : true,
      value : function (id) {
        if (!this._fetchedStatistics[id]) {
          this._fetchedStatistics[id] = new this.Promise(function (resolve, reject) {
            this.$http.get(this.APIURL + '/api/statistics/' + id, {
              cache : true
            })
              .then(function (response) {
                var retObj = {
                  trie : this.dataStructuresService.createTrie(),
                  accumulated : 0,
                  id : response.data.id
                };

                response.data.data.forEach(function (region) {
                  retObj.accumulated += region.value;
                  retObj.trie.add(region.code, region.value);
                });

                resolve(retObj);
              }.bind(this))
              .catch(function (err) {
                reject(err);
              });
          }.bind(this));
        }
        return this._fetchedStatistics[id];
      }
    }
  });

  MapDataService.$inject = ['$http', '$q', 'dataStructuresService', 'APIURL'];

  angular.module('highToolClientApp')
    .service('MapDataService', MapDataService);
}());
