(function () {
  /**
   * @ngdoc service
   * @name highToolClientApp.tablesService
   * @description
   * # Service used to retrieve Baseline values
   * Service in the highToolClientApp.
   */

  'use strict';
  function TablesService ($http, APIURL, $q) {
    this.$http = $http;
    this.APIURL = APIURL;
    this.Promise = $q;

    this.variables = new this.Promise(function (resolve, reject) {
      $http({
          method: 'GET',
          url: APIURL + '/api/tables/variables',
          cache: true
        })
        .then(function (response) {
          resolve(response.data);
        }, reject);
    });
  }

  TablesService.prototype.queryVariables = function (variableList) {
    return new this.Promise(function (resolve, reject) {
      this.$http.get(this.APIURL + '/api/tables/variables', {
        params : {
          variables : variableList
        }
      })
      .then(function (response) {
        resolve(response.data.reduce(function (acum, variableGroup) {
          variableGroup.variables.forEach(function (variable) {
            acum[variable.id] = {
              id : variable.id,
              name : variable.name,
              description : variable.description,
              table : variable.table_id,
              unit : variable.unit_id,
              type : variableGroup.name
            };
          });
          return acum;
        }, {}));
      })
      .catch(function (err) {
        reject(err);
      });
    }.bind(this));
  };

  TablesService.prototype.downloadTable = function (schemaId, tableId, cb) {
    this.$http.get(this.APIURL + '/api/tables/schema/' + schemaId + '/table/' + tableId)
      .then(function (response) {
        cb(null, new Blob([response.data], { type: 'text/csv' }));
      }, function (response) {
        cb({
          data : response.data || 'Request failed',
          status : response.status
        }, null);
      });
  };

  TablesService.prototype.uploadTable = function (schemaId, tableId, sendObj, cb) {
    console.log(sendObj);

    this.$http.post(this.APIURL + '/api/tables/schema/' + schemaId + '/table/' + tableId, {
        filename : sendObj.name,
        encoding : sendObj.data.encoding,
        mime : sendObj.mime,
        data : sendObj.data.load
      })
      .then(function () {
        cb(null, true);
      }, function (response) {
        console.log(response);
        var message = response.statusText || response.data || 'Request failed';
        cb({
          message : 'HTTP Error code ' + (response.status ? response.status : 500) + ' - ' + message
        }, null);
      });
  };

  TablesService.prototype.updateHypernetwork = function (schemaId, modeId, edgesOrNodes, data, cb) {
    var sendDataObj = {};
    sendDataObj[edgesOrNodes] = data;

    this.$http.put(this.APIURL + '/api/HyperNetwork/' + schemaId + '/mode/' + modeId + '/' + edgesOrNodes, sendDataObj)
      .then(function (response) {
        cb(null, response.data);
      })
      .catch(function (err) {
        cb(err, null);
      });
  };

  TablesService.$inject = ['$http', 'APIURL', '$q'];

  angular.module('highToolClientApp')
    .service('TablesService', TablesService);

}());
