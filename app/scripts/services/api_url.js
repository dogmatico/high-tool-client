/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.APIURL
   * @description
   * # APIURL
   * Constant in the highToolClientApp.
   */
  angular.module('highToolClientApp')
    .constant('APIURL', 'https://hightool.mcrit.com');
}());
