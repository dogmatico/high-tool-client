/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.APIURL
   * @description
   * # APIURL
   * Constant in the highToolClientApp.
   */
  angular.module('highToolClientApp')
    .constant('_MODEL_START_YEAR', 2010)
    .constant('_MODEL_DEFAULT_FIRST_COMPUTE_YEAR', 2015)
    .constant('_MODEL_LAST_YEAR', 2050)
    .constant('_MODEL_DEFAULT_LAST_COMPUTE_YEAR', 2050)
    .constant('_TPM_PREFIX', 'TPM_TPM_')
    .constant('_MODEL_RUN_STATUS_CODES', function (code) {
      var result;
      switch (code) {
      case 0:
        result = 'Not created';
        break;
      case 1:
        result = 'Idle';
        break;
      case 10:
        result = 'Paused';
        break;
      case 11:
        result = 'Paused and waiting for Expert Mode';
        break;
      case 2:
        result = 'Active';
        break;
      case 3:
        result = 'Marked for deletion';
        break;
      case 4:
        result = 'Error: Cannot create schema';
        break;
      case 5:
        result = 'Error: Unexpected error while running a model';
        break;
      case 6:
        result = 'Deleted';
        break;
      default:
        result = 'Undefined code';
      }
      return result;
    })
    .constant('_MODEL_COUNTRIES_ORDER', function (country) {
      var priority;

      switch (country) {
      case 'EU28':
      case 'EU28+NO+CH':
        priority = 0;
        break;
      case 'Austria':
      case 'Belgium':
      case 'Bulgaria':
      case 'Cyprus':
      case 'Czech Republic':
      case 'Czech Rep.':
      case 'Germany':
      case 'Denmark':
      case 'Estonia':
      case 'Spain':
      case 'Finland':
      case 'France':
      case 'Greece':
      case 'Croatia':
      case 'Hungary':
      case 'Ireland':
      case 'Italy':
      case 'Lithuania':
      case 'Luxembourg':
      case 'Luxembourg (Grand-Duche)':
      case 'Latvia':
      case 'Malta':
      case 'Netherlands':
      case 'Poland':
      case 'Portugal':
      case 'Romania':
      case 'Sweden':
      case 'Slovenia':
      case 'Slovakia':
      case 'United Kingdom':
        priority = 1;
        break;
      case 'Switzerland':
      case 'Norway':
        priority = 2;
        break;
      case 'Iceland':
      case 'Turkey':
      case 'Azerbaijan':
      case 'Bosnia Herzegovina':
      case 'Andorra':
      case 'Monaco':
      case 'Albania':
      case 'Armenia':
      case 'San Marino':
      case 'Serbia':
      case 'Liechtenstein':
      case 'Montenegro':
      case 'TFYR of Macedonia':
      case 'Belarus':
      case 'Ukraine':
      case 'Moldova':
      case 'Morocco':
      case 'Georgia':
      case 'Iran':
      case 'Uzbekistan':
      case 'Turkmenistan':
      case 'Russian Federation':
      case 'Kazakhstan':
        priority = 3;
        break;
      case 'CIS':
      case 'Antarctica':
      case 'Europe, Rest':
      case 'America Canada':
      case 'America USA':
      case 'America Mexico':
      case 'America Central':
      case 'America Caribbean':
      case 'America South':
      case 'Asia/Pacific Far East':
      case 'Asia/Pacific Southern Asia':
      case 'Asia/Pacific Indian Subcontinent':
      case 'Asia/Pacific Australia/Oceania':
      case 'Africa Nord':
      case 'Africa Central and South':
      case 'Africa East':
      case 'Middle East Mediterranean':
      case 'Middle East East':
      case 'Russia, east of Urals':
      case 'Russia, west of Urals':
        priority = 4;
        break;
      default:
        priority = 5;
      }
      return priority;
    })
    .constant('_UPDATE_MASTER_SERVER_URL', 'https://hightool.mcrit.com');
}());
