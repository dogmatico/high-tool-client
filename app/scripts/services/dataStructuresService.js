/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name highToolClientApp.dataStructuresService
   * @description
   * # Service used to inject useful data structures
   * Service in the highToolClientApp.
   */
  function Trie() {
    this.value = null;
    this.keys = [];
  } 

  Trie.prototype.add = function (uuid, value) {
    var i, ln, letter, node = this;
    node.keys.push(uuid);
    for (i = 0, ln = uuid.length; i < ln; i += 1) {
      letter = uuid[i];
      if (!node[letter]) {
        node[letter] = new Trie();
      }
      node = node[letter];
    }
    node.value = value;
    node.iW = true;
    return;
  };

  Trie.prototype.get = function (uuid) {
    var i, ln, letter, node = this,
      nodeVal = false;
    for (i = 0, ln = uuid.length; i < ln && node !== false; i += 1) {
      letter = uuid[i];
      node = node[letter] || false;
    }
    if (i === ln && node && node.iW) {
      nodeVal = node.value;
    }
    return nodeVal;
  };

  function DataStructureService() {
    this.createTrie = function () {
      return new Trie();
    };
    return;
  }

  angular.module('highToolClientApp')
    .service('dataStructuresService', DataStructureService);
}());
