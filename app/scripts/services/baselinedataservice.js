/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.dataStructuresService
   * @description
   * # Service used to retrieve Baseline values
   * Service in the highToolClientApp.
   */
  function BaselineDataService($http, $q, APIURL, _MODEL_START_YEAR, _MODEL_DEFAULT_FIRST_COMPUTE_YEAR, _MODEL_LAST_YEAR) {
    this.Promise = $q;
    this.$http = $http;
    this.APIURL = APIURL;
    this._MODEL_START_YEAR = _MODEL_START_YEAR;
    this._MODEL_DEFAULT_FIRST_COMPUTE_YEAR = _MODEL_DEFAULT_FIRST_COMPUTE_YEAR;
    this._MODEL_LAST_YEAR = _MODEL_LAST_YEAR;
  }

  Object.defineProperties(BaselineDataService.prototype, {
    'getTimeBaselineOfArray' : {
      enumerable : true,
      value : function (arrayUiid) {
        return new this.Promise(function (resolve, reject) {
          this.$http.get(this.APIURL + '/api/baseline/childrens?roots[]=' + arrayUiid.join('&roots[]='))
            .then(function (response) {
              resolve(response.data);
            })
            .catch(function (err) {
              reject(err);
            });
        }.bind(this));
      }
    },
    'createEmptyTimeSeries' : {
      enumerable : true,
      value : function (startYear, terminalValue, initialValue) {
        initialValue = (initialValue || initialValue === 0 ? initialValue : 1);

        var timeSeries = [
          [this._MODEL_START_YEAR, initialValue, 0],
        ];

        if (startYear !== this._MODEL_DEFAULT_FIRST_COMPUTE_YEAR) {
          timeSeries.push([startYear - 5, initialValue, 0]);
        }

        timeSeries.push([startYear, terminalValue, 0]);

        if (startYear !== this._MODEL_LAST_YEAR) {
          timeSeries.push([this._MODEL_LAST_YEAR, terminalValue, 0]);
        }

        return timeSeries;
      }
    }
  });

  BaselineDataService.$inject = ['$http', '$q', 'APIURL', '_MODEL_START_YEAR', '_MODEL_DEFAULT_FIRST_COMPUTE_YEAR', '_MODEL_LAST_YEAR'];

  angular.module('highToolClientApp')
    .service('BaselineDataService', BaselineDataService);
}());
