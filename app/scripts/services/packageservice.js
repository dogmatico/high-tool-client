/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name highToolClientApp.packageService
   * @description
   * # packageService
   * Service in the highToolClientApp.
   */

  function packageService($http, $sce, $q, dataStructuresService, APIURL, _MODEL_DEFAULT_FIRST_COMPUTE_YEAR) {
    /*jshint validthis:true */
    var currentSchema = null,
      lastMetadata,
      schema = {};

    var Promise = Promise || $q;

    function mapTypeToRoot(packageType) {
      var result = 'Unknown';
      switch (packageType) {
      case 'policy':
      case 'Policy':
        result = 'TPMROOT';
        break;
      case 'framework':
      case 'Framework':
        result = 'FCROOT';
        break;
      case 'scenario':
      case 'Scenario':
        result = 'SCROOT';
        break;
      case 'output':
      case 'Output':
        result = 'OUROOT';
        break;
      case 'custom':
      case 'Custom':
        result = 'CUSTOMTPMROOT';
        break;
      case 'combined':
      case 'Combined':
        result = 'COMBINEDTPMROOT';
        break;
      case 'TPMRun':
        // TODO: change back to correct root
        //result = 'TPMRunROOT';
        result = 'TPMROOT';
        break;
      }
      return result;
    }

    function rootToSchema(rootNode) {
      var schemaType = 'Unknown';
      switch (rootNode) {
      case 'TPROOT':
        schemaType = 'policy';
        break;
      case 'OUROOT':
        schemaType = 'output';
        break;
      case 'FCROOT':
        schemaType = 'framework';
        break;
      case 'TPMROOT':
        schemaType = 'TPMRun';
        break;
      case 'CUSTOMTPMROOT':
        schemaType = 'custom';
        break;
      case 'COMBINEDTPMROOT':
        schemaType = 'combined';
        break;
      }
      return schemaType;
    }

    function getModule(variableName) {
      var regExModule = new RegExp(/(i|o|p){1}_(\w{2})/),
       module2Id = variableName.match(regExModule),
       module3Id;

      if (module2Id && module2Id[2]) {
        switch (module2Id[2]) {
         case 'de':
           module3Id = 'DEM';
           break;
         case 'er':
           module3Id = 'ECR';
           break;
         case 'ev':
           module3Id = 'ENV';
           break;
         case 'fd':
           module3Id = 'FRD';
           break;
         case 'pd':
           module3Id = 'PAD';
           break;
         case 'sa':
           module3Id = 'SAF';
           break;
         case 'vs':
           module3Id = 'VES';
           break;
         default:
           module3Id = 'OTHER';
        }
      } else {
        module3Id = 'OTHER';
      }
      return module3Id;
    }


    this.trieUuid = dataStructuresService.createTrie();

    function populateSchema(schemaContainer, schemaType) {
      var rootNode = mapTypeToRoot(schemaType);
      schemaContainer[schemaType] = new Promise(function (resolve, reject) {
        $http.get(APIURL + '/api/schema/' + rootNode)
          .then(function (response) {
            response.data.dictionary = response.data.nodeAttributes.reduce(function (prev, curr) {
              prev[curr.uuid] = curr.id;
              return prev;
            }, {});
            resolve(response.data);
          })
          .catch(reject);
      });

      schemaContainer[schemaType].then(function (schemaData) {
        var i, ln;
        for (i = 0, ln = schemaData.nodeAttributes.length; i < ln; i += 1) {
          this.trieUuid.add(schemaData.nodeAttributes[i].uuid, i);
        }
      }.bind(this));
    }

    this.deletePackage = function (packageId) {
      return $http.delete(APIURL + '/api/package/' + packageId);
    };

    this.getLastMetadata = function () {
      return lastMetadata;
    };

    this.getLastSchema = function () {
      return schema[currentSchema];
    };

    this.getSchema = function (schemaType, skipVerification) {
      var rootNode = mapTypeToRoot(schemaType);

      if (skipVerification !== true && rootNode === 'Unknown') {
        return Promise.reject('Unkown schema');
      }
      if (!schema[schemaType]) {
        populateSchema.bind(this)(schema, schemaType);
      }
      return schema[schemaType];
    };

    this.fetchPackageData = function (packageId, rootNode) {
      var schemaType = rootToSchema(rootNode);
      return new Promise(function (resolve, reject) {

        function createBaselineNodeList(schemaData) {
          var i, ln, retArr = [];
          for (i = 0, ln = schemaData.nodeAttributes.length; i < ln; i += 1) {
            retArr[i] = {
              uuid : schemaData.nodeAttributes[i].uuid,
              values : {
                terminalValue : schemaData.nodeAttributes[i].baseline,
                representationaData : {
                  timeSeries : []
                },
                startYear : _MODEL_DEFAULT_FIRST_COMPUTE_YEAR
              }
            };
          }
          return retArr;
        }

        schema[schemaType].then(function (schemaData) {
          var retData = {
            abstract : 'new package',
            headline : 'new package',
            label : 'new package',
            rootNode : schemaData.rootNodes[0].userId,
            subpackages : [],
            nodeList : createBaselineNodeList(schemaData),
            data : {}
          };

          if (packageId === 'new') {
            resolve(retData);
          } else {
            $http.get(APIURL + '/api/package/' + packageId)
              .then(function (response) {
                var data = response.data;
                if (data.rootNode === mapTypeToRoot('custom')) {
                  var receivedNodes = data.nodeList,
                    uuidTrie = dataStructuresService.createTrie();
                    data.nodeList = retData.nodeList;

                  data.nodeList.forEach(function (node, index) {
                    uuidTrie.add(node.uuid, index);
                  });

                  receivedNodes.forEach(function (node) {
                    var pos = uuidTrie.get(node.uuid);
                    data.nodeList[pos].values = node.values;
                    if (!data.nodeList[pos].values.representationaData) {
                      data.nodeList[pos].values.representationaData = {
                        timeSeries : []
                      };
                    }

                    if (!data.nodeList[pos].values.startYear) {
                      data.nodeList[pos].values.startYear = _MODEL_DEFAULT_FIRST_COMPUTE_YEAR;
                    }

                    data.nodeList[pos].use = true;
                  });
                }
                resolve(data);
              })
              .catch(reject);
          }
        });
      });
    };

    this.fetchPackageMetadata = function (id) {
      return new Promise(function (resolve, reject) {
        $http.get(APIURL + '/api/package/' + id + '/metadata')
          .then(function (response) {
            lastMetadata = {
              abstract : response.data.abstract,
              headline : response.data.headline,
              label : response.data.label,
              rootNode : response.data.rootNode,
              subpackages : response.data.subpackages
            };
            resolve(response.data);
          })
          .catch(reject);
      });
    };

    this.fecthScenarioData = function (id) {
      return new Promise(function (resolve) {
        this.fetchPackageMetadata(id)
          .then(function (metaData) {
            var i, ln, returnPromises = [
              $q.resolve(metaData)
            ];
            for (i = 0, ln = metaData.subpackages.length; i < ln; i += 1) {
              returnPromises.push(this.fetchPackageMetadata(metaData.subpackages[i]));
            }
            return Promise.all(returnPromises);
          }.bind(this))
          .then(function (allScenarioData) {
            var i, ln, packageType, retObject = {
              id : allScenarioData[0].id,
              headline : allScenarioData[0].headline,
              abstract : allScenarioData[0].abstract,
              selectedPackages : {
                framework : null,
                policy: null
              }
            };
            for (i = 1, ln = allScenarioData.length; i < ln; i += 1) {
              switch (allScenarioData[i].rootNode) {
              case 'FCROOT':
                packageType = 'framework';
                break;
              case 'TPROOT':
              case 'CUSTOMTPMROOT':
                packageType = 'policy';
                break;
              default:
                packageType = null;
              }
              if (packageType) {
                retObject.selectedPackages[packageType] = {
                  id : allScenarioData[i].id,
                  author: allScenarioData[i].author,
                  headline: allScenarioData[i].headline,
                  abstract: $sce.trustAsHtml(allScenarioData[i].abstract)
                };
              }
            }
            resolve(retObject);
          })
          .catch(function (err) {
            console.log(err);
            resolve(null);
          });
      }.bind(this));
    };

    this.listPackages = function (packageType, onlyOwned) {
      var ownedOption = '';
      if (onlyOwned && onlyOwned === true) {
        ownedOption = 'own/';
      }
      switch (packageType) {
      case 'framework':
      case 'policy':
      case 'output':
      case 'scenario':
        break;
      default:
        packageType = 'all';
      }
      return new Promise(function (resolve, reject) {
        $http.get(APIURL + '/api/package/' + ownedOption + 'type/' + packageType)
          .then(function (reponse) {
            resolve(reponse.data);
          })
          .catch(reject);
      });
    };

    this.mapRootToType = function (rootUuid) {
      var result = 'Unknown';
      switch (rootUuid) {
      case 'TPROOT':
        result = 'Policy';
        break;
      case 'FCROOT':
        result = 'Framework';
        break;
      case 'SCROOT':
        result = 'Scenario';
        break;
      case 'OUROOT':
        result = 'Output';
        break;
      case 'CUSTOMTPMROOT':
        result = 'Custom';
        break;
      case 'COMBINEDTPMROOT':
        result = 'Combined';
        break;
      }

      if (result === 'Unknown' && rootUuid && rootUuid.search(/^(TPM)/) !== -1) {
        result = 'Single TPM';
      }
      return result;
    };

    this.mapTypeToRoot = mapTypeToRoot;

    this.newPackage = function (data) {
      return new Promise(function (resolve, reject) {
        $http.post(APIURL + '/api/package/', data)
          .then(function (response) {
            resolve(response.data);
          })
          .catch(reject);
      });
    };

    this.getModule = getModule;

    this.fetchDoneRun = function () {
      return new Promise(function (resolve, reject) {
        $http.get(APIURL + '/api/run/done')
          .then(function (response) {
            resolve(response.data);
          })
          .catch(reject);
      });
    };

    this.updatePackage = function (id, newData) {
      return new Promise(function (resolve, reject) {
        $http.put(APIURL + '/api/package/' + id, newData)
          .then(function (response) {
            resolve(response.data);
          })
          .catch(reject);
      });
    };

    this.getReadyForExpertMode = function () {
      return new Promise(function (resolve, reject) {
        $http.get(APIURL + '/api/run/')
          .then(function (response) {
            resolve(response.data.filter(function (runInfo) {
              return (runInfo.step === 'EXPERTMODE');
            }));
          })
          .catch(function (err) {
            reject(err);
          });
      });
    };

  }
  packageService.$inject = ['$http', '$sce', '$q', 'dataStructuresService', 'APIURL', '_MODEL_DEFAULT_FIRST_COMPUTE_YEAR'];

  angular.module('highToolClientApp')
    .service('packageService', packageService);
}());
