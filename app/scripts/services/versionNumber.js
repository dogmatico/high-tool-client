/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.APIURL
   * @description
   * # APIURL
   * Constant in the highToolClientApp.
   */
  angular.module('highToolClientApp')
    .constant('VERSION_NUMBER', '1.21.1');
}());
