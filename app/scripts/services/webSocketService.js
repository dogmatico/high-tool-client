/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc service
   * @name highToolClientApp.WebSocketService
   * @description
   * # Creates/destroys websockets binded to authentication messages
   * Service in the highToolClientApp.
   */

  function WebSocketService($window, $q, $rootScope, userAuthFactory) {
    if ($window.io) {
      this.socket = $window.io.socket || $window.io.Socket || {
        on : function () { return null; },
        off : function () { return null; },
        request : function () { return null; }
      };
    } else {
      this.socket = {
        on : function () { return null; },
        off : function () { return null; },
        request : function () { return null; }
      };
    }

    this.user = userAuthFactory;

    this.login();
  }

  Object.defineProperties(WebSocketService.prototype, {
    '__addAuthorization' : {
      value : function (request) {
        if (!request) {
          request = {};
        }
        if (this.user.isAuthenticated()) {
          if (!request.headers) {
            request.headers = {};
          }
          request.headers.Authorization = 'Bearer ' + this.user.getToken();
        }
        return request;
      }
    },
    'login' : {
      enumerable : true,
      value : function () {
        var request = this.__addAuthorization({
          method : 'get',
          url : '/api/notification'
        });
        this.socket.request(request, function (err) {
          if (err) {
            console.log(err);
            if (err.status === 401 || err.status === 403) {
              this.user.logout();
            }
          }
        }.bind(this));
      }
    },
    'logout' : {
      enumerable : true,
      value : function () {
        var request = {
          method : 'delete',
          url : '/api/notification'
        };
        this.socket.request(request);
      }
    }
  });


  WebSocketService.prototype.post = function (URL, data, params) {
    return this.$q(function (resolve) {
      this.request({
        method: 'post',
        url: this.APIURL + URL,
        data : data,
        params: params,
        headers: {
          Authorization : 'Bearer ' + this.$window.localStorage.getItem('JWT')
        }
      }, function (data, jwr) {
        resolve({data: data, jwr : jwr});
      });
    }.bind(this));
  };

  WebSocketService.$inject = ['$window', '$q', '$rootScope', 'userAuthFactory'];

  angular.module('highToolClientApp')
    .service('WebSocketService', WebSocketService);
}());
