/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name highToolClientApp.mapDataService
   * @description
   * # mapDataService
   * Service in the highToolClientApp.
   */

  function userAuthFactory($rootScope, $window, $q, APIURL) {
    var claims,
      userAuthenticated = false;

    function login(email, password) {
      var userCredentials = {
        email : email,
        password : password
      };
      return $q(function (resolve, reject) {
        var request = new XMLHttpRequest(),
          URL = APIURL + '/login/';
        userAuthenticated = false;

        request.open('POST', URL, true);
        request.setRequestHeader('Content-type', 'application/json');
        request.setRequestHeader('Cache-Control', 'no-cache');
        request.onreadystatechange = function () {
          var serverData;
          if (this.readyState === 4) {
            if (this.status === 200) {
              serverData = JSON.parse(this.response);
              if (serverData.success !== true) {
                $rootScope.$broadcast('authLogout');
                reject(serverData.message);
              } else {
                $window.localStorage.setItem('JWT', serverData.token);
                $rootScope.$broadcast('authSuccess');
                userAuthenticated = true;
                resolve(serverData);
              }
              resolve(JSON.parse(this.response));
            } else {
              $rootScope.$broadcast('authLogout');
              reject({message : this.statusText});
            }
          }
        };
        request.send(JSON.stringify(userCredentials));
      });
    }

    function getClaims() {
      var token = $window.localStorage.getItem('JWT');
      if (token) {
        claims = JSON.parse($window.atob(token.split('.')[1]));
      } else {
        claims = null;
      }
      return claims;
    }

    function getToken() {
      return $window.localStorage.getItem('JWT');
    }

    function isAdmin() {
      claims = getClaims();
      if (claims) {
        return claims.isAdmin;
      }
      return false;
    }

    function isAuthenticated() {
      return userAuthenticated;
    }

    function isTokenInTime() {
      var currentTime = Math.floor((new Date()).getTime() / 1000);
      userAuthenticated = false;
      if ($window.localStorage.getItem('JWT')) {
        claims = getClaims();
        if (claims.exp < currentTime) {
          $window.localStorage.removeItem('JWT');
          $rootScope.$broadcast('authLogout');
        } else {
          userAuthenticated = true;
        }
      } else {
        $rootScope.$broadcast('authLogout');
      }
      return userAuthenticated;
    }

    function logout() {
      userAuthenticated = false;
      $window.localStorage.removeItem('JWT');
      $rootScope.$broadcast('authLogout');
      return;
    }

    isTokenInTime();

    return {
      isAdmin : isAdmin,
      isAuthenticated : isAuthenticated,
      isTokenInTime : isTokenInTime,
      getClaims : getClaims,
      getToken : getToken,
      login : login,
      logout: logout
    };
  }

  userAuthFactory.$inject = ['$rootScope', '$window', '$q', 'APIURL'];

  angular.module('highToolClientApp')
    .factory('userAuthFactory', userAuthFactory);
}());
