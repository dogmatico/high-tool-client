(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:ExportTablesCtrl
   * @description
   * # ExportTablesCtrl
   * Controller of the highToolClientApp
   */

  function ExportTablesCtrl($scope, $stateParams, TablesService, $http, APIURL) {
    $scope.downloadTable = function (tableId) {
      TablesService.downloadTable($scope.schemata.selected.id, tableId, function (err, blob) {
        if (err) {
          console.log(err);
          return;
        }
        var url = URL.createObjectURL(blob);

        var downloadClick = document.createElement('a');
        downloadClick.download = tableId + '.csv';
        downloadClick.setAttribute('href', url);
        document.body.appendChild(downloadClick);
        downloadClick.click();
        downloadClick.parentNode.removeChild(downloadClick);
      });
    };

    $http.get(APIURL + '/api/run/')
      .then(function (response) {
        $scope.schemata = {
          available : [{
            label : 'HIGH-TOOL Baseline',
            id : 'high_tool',
            status : 'Blocked'
          }].concat(response.data).map(function (item) {
            item.description = item.label + '(' + item.id + ') --- Status : ' + item.status;
            return item;
          })
        };

        $scope.schemata.selected = $scope.schemata.available[0];

        if ($stateParams && $stateParams.runId) {
          var index = 0;
          for (var i = 1, ln = $scope.schemata.available.length; i < ln && !index; i += 1) {
            if (parseInt($scope.schemata.available[i].id, 10) === parseInt($stateParams.runId, 10)) {
              index = i;
            }
          }
          $scope.schemata.selected = $scope.schemata.available[index];
        }
      })
      .catch(function (err) {
        console.log(err);
      });


    TablesService.variables
      .then(function (variableList) {
        $scope.tables = [
          {
            name : 'All',
            variables : variableList.reduce(function (prev, curr) {
              return prev.concat(curr.variables);
            }, [])
          }
        ];
        $scope.tables = $scope.tables.concat(variableList);
      })
      .catch(function (err) {
        console.log(err);
      });

    return;
  }

  ExportTablesCtrl.$inject = ['$scope', '$stateParams', 'TablesService', '$http', 'APIURL'];

  angular.module('highToolClientApp')
    .controller('ExportTablesCtrl', ExportTablesCtrl);
}());
