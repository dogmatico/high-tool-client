(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name highToolClientApp.controller:runOptionsCtrl
   * @description
   * # Used in the scenario editor
   * Controller of the highToolClientApp
   */

  function customPackageCtrl($scope, $rootScope, $sce, $uibModal, $state, $stateParams, packageService, packageData, schema, spatialAndTemporalDimensions) {

    $scope.activeFamily = 0;
    $scope.activeFirstLevel = 0;
    $scope.schema = schema;
    $scope.abstract = packageData.abstract;
    $scope.headline = packageData.headline;
    $scope.label = packageData.label;
    $scope.package = {
      values : [],
      subpackages : []
    };

    $scope.hideTime = function (schemaItem) {
      if (schemaItem.target && schemaItem.target.__parameters &&
        schemaItem.target.__parameters.length && spatialAndTemporalDimensions[schemaItem.target.__parameters[0]]) {
        return !spatialAndTemporalDimensions[schemaItem.target.__parameters[0]].temporal;
      }
      return false;
    };

    $scope.hideSpatial = function (schemaItem) {
      return (schemaItem.is_geo_active_n0 == 0 && schemaItem.is_geo_active_n2 == 0);
    };

    $scope.isNew = ($stateParams.packageId === 'new');

    $scope.orderByLabel = function(localId) {
      return schema.nodeAttributes[localId].label;
    };

    for (var i = 0, ln = packageData.nodeList.length; i < ln; i += 1) {
      $scope.package.values[packageService.trieUuid.get(packageData.nodeList[i].uuid)] = packageData.nodeList[i].values;
    }

    $scope.setActiveFamily = function (index) {
      $scope.activeFamily = parseInt(index, 10);
    };
    $scope.setPreviousActiveFamily = function () {
      $scope.activeFamily = ($scope.activeFamily + $scope.schema.adjList[0].length  - 1) % ($scope.schema.adjList[0].length);
    };
    $scope.setNextActiveFamily = function () {
      $scope.activeFamily = ($scope.activeFamily + 1) % ($scope.schema.adjList[0].length);
    };

    $rootScope.$on('savePackage', function(newPackage) {
      $scope.save(newPackage);
    });

    $scope.save = function (newPackage) {
      var editor = $uibModal.open({
        templateUrl: '/views/partials/modalSaveNewPackage.tmpl.html',
        scope: $scope,
        controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
          };
          $scope.modalTitle = 'Update package metadata';
          $scope.ok = function () {
            $uibModalInstance.close({
              label : $scope.label,
              headline : $scope.headline,
              abstract : $scope.abstract
            });
          };
        }]
      });

      editor.result
        .then(function (runOptions) {
          var affectedInputs = [];
          if (runOptions) {
            $scope.schema.adjList[0].forEach(function (moduleId) {
              $scope.schema.adjList[moduleId].forEach(function (leverId) {
                if ($scope.package.values[leverId].use) {
                  var objToPush = {
                    label : $scope.schema.nodeAttributes[leverId].shortLabel,
                    uuid : $scope.schema.nodeAttributes[leverId].uuid,
                    target : $scope.schema.nodeAttributes[leverId].target,
                    values : $scope.package.values[leverId]
                  };

                  if (($scope.schema.nodeAttributes[leverId].max === 0 || $scope.schema.nodeAttributes[leverId].max) &&
                  ($scope.schema.nodeAttributes[leverId].min === 0 || $scope.schema.nodeAttributes[leverId].min)) {
                    objToPush.min = $scope.schema.nodeAttributes[leverId].min;
                    objToPush.max = $scope.schema.nodeAttributes[leverId].max;
                  }

                  affectedInputs.push(objToPush);
                }
              });
            });
            var saveAction = (function (packageId, packageData) {
              if (packageId === 'new' || newPackage === true) {
                return packageService.newPackage(packageData);
              }
              return packageService.updatePackage(packageId, packageData);
            }($stateParams.packageId, {
              label : runOptions.label,
              headline : runOptions.headline,
              abstract : runOptions.abstract,
              rootNode : $scope.schema.nodeAttributes[0].uuid,
              nodeList : affectedInputs
            }));

            return saveAction;

          }
        })
        .then(function (result) {
          $state.go('.', {packageId : result.id});
        })
        .catch(function (err) {
          console.error(err);
        });
    };
  }

  customPackageCtrl.$inject = ['$scope', '$rootScope', '$sce', '$uibModal', '$state', '$stateParams', 'packageService', 'packageData', 'schema', 'spatialAndTemporalDimensions'];
  angular.module('highToolClientApp')
    .controller('customPackageCtrl',customPackageCtrl);
}());
