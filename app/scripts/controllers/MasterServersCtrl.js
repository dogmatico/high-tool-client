/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:usersAdministrationCtrl
   * @description
   * # Controls user administration
   * Controller of the highToolClientApp
   */
  function MasterServersCtrl($scope, $http, $state, $uibModal, userAuthFactory, APIURL) {
    $scope.isAdmin = userAuthFactory.isAdmin();
    if (!userAuthFactory.isAdmin()) {
      $state.go('authenticated.main');
    }

    $http.get(APIURL + '/api/repositories')
      .success(function (data) {
        $scope.serverList = data;
      });

    $scope.addServer = function () {
      var editor =  $uibModal.open({
        templateUrl : '/views/partials/modalAddServer.tmpl.html',
        controller : 'ModalAddServer',
        size : 'lg',
        scope : $scope
      });

      editor.result
      .then(function (newServer) {
        $http.post(APIURL + '/api/repositories', {
          host : newServer.host,
          port : newServer.port,
          description : newServer.description
        })
          .then(function (data) {
            $scope.serverList.push(data);
          });
      });
    };
    return;
  }

  MasterServersCtrl.$inject = ['$scope', '$http', '$state', '$uibModal', 'userAuthFactory', 'APIURL'];
  angular.module('highToolClientApp')
    .controller('MasterServersCtrl', MasterServersCtrl);
}());
