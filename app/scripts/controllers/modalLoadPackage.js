/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name highToolClientApp.controller:ModalLoadPackageCtrl
   * @description
   * # ModalLoadPackageCtrl
   * Controller of the highToolClientApp
   */

  function modalControllerLoad($scope, $uibModalInstance, packageService, title, packageList, options) {
    $scope.packageList = packageList.map(function (item) {
      item.type = packageService.mapRootToType(item.rootNode);
      item.updatedAt = new Date(item.updatedAt);
      return item;
    });

    $scope.title = title;

    $scope.options = options;

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
    $scope.isSelected = function (index) {
      return $scope.selectedPackage === index;
    };
    $scope.ok = function ok() {
      if ($scope.selectedPackage === null) {
        $uibModalInstance.dismiss('cancel');
      } else {
        if ($scope.options && $scope.options.length) {
          $uibModalInstance.close([
            packageList[$scope.selectedPackage],
            $scope.options.map(function (item) {
              delete item.label;
              return item;
            })
          ]);
        } else {
          $uibModalInstance.close(packageList[$scope.selectedPackage]);
        }
      }
    };

    $scope.selectedPackage = null;
    $scope.toggleSelectedPackage = function (index) {
      $scope.selectedPackage = ($scope.selectedPackage === index ? null : index);
    };
    return;
  }

  modalControllerLoad.$inject = ['$scope', '$uibModalInstance', 'packageService', 'title', 'packageList', 'options'];

  angular.module('highToolClientApp')
    .controller('ModalLoadPackageCtrl', modalControllerLoad);
}());
