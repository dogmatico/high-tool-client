/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:usersAdministrationCtrl
   * @description
   * # Controls user administration
   * Controller of the highToolClientApp
   */
  function ModulesCtrl($scope, $q, $http, userAuthFactory, WebSocketService, APIURL, _UPDATE_MASTER_SERVER_URL) {
    $scope.isAdmin = userAuthFactory.isAdmin();

    $scope.messages = [];
    $scope.closeMsg = function (index) {
      $scope.messages.splice(index, 1);
    };


    function updateModuleList() {
      $q.all([
        $http.get(_UPDATE_MASTER_SERVER_URL + '/api/modules'),
        $http.get(APIURL + '/api/modules')
      ])
      .then(function (responses) {
        $scope.modulesList = responses[0].data.installed.map(function (item) {
          item.masterVersion = item.version;
          item.installedVersion = 'N/A';
          delete item.version;
          return item;
        });

        responses[1].data.installed.forEach(function (item) {
          var i, ln, found;
          for (i = 0, ln = $scope.modulesList.length, found = false; i < ln && !found; i += 1) {
            if ($scope.modulesList[i].application === item.application) {
              found = true;
              $scope.modulesList[i].installedVersion = item.version;
            }
          }
          if (!found) {
            $scope.modulesList[i] = {
              application : item.application,
              masterVersion : 'N/A',
              installedVersion : item.version
            };
          }
        });
      })
      .catch(function (errorResponse) {
        $scope.messages.push({
          type : 'warning',
          title : 'Error ' + errorResponse.status,
          body : errorResponse.data
        });
      });
    }

    updateModuleList();
    WebSocketService.socket.on('module-update', updateModuleList);
    $scope.$on('$destroy', function () {
      WebSocketService.socket.off('module-update', updateModuleList);
    });

    $scope.disableUpdate = function (module) {
      if (!userAuthFactory.isAdmin()) {
        return true;
      }
      if (module.masterVersion === 'N/A') {
        return true;
      }
      if (module.installedVersion === 'N/A') {
        return false;
      }

      var masterVersion, installedVersion;
      if (module.application === 'Economic Module') {
        masterVersion = module.masterVersion.match(/(\d+)/)[1];
        installedVersion = module.installedVersion.match(/(\d+)/)[1];
        return masterVersion <= installedVersion;
      }

      masterVersion = module.masterVersion.split('.');
      installedVersion = module.installedVersion.split('.');

      var maxLn = Math.max(masterVersion.length, installedVersion.length);
      for (var i = 0; i < maxLn; i += 1) {
        if (!masterVersion[i]) {
          masterVersion[i] = 0;
        }
        if (!installedVersion[i]) {
          installedVersion[i] = 0;
        }

        if (masterVersion[i] > installedVersion[i]) {
          return false;
        }
      }
      return true;
    };

    $scope.updateModule = function (module) {
      var moduleId;
      switch (module.application) {
        case 'Demography Module':
          moduleId = 'DEM';
          break;
        case 'Economic Module':
          moduleId = 'ECR';
          break;
        case 'Environment Module':
          moduleId = 'ENV';
          break;
        case 'Freight Demand Module':
          moduleId = 'FRD';
          break;
        case 'Passenger Module':
          moduleId = 'PAD';
          break;
        case 'Safety Module':
          moduleId = 'SAF';
          break;
        case 'Vehstock Module':
          moduleId = 'VES';
          break;
        default:
          moduleId = null;
      }
      if (moduleId) {
        $http.put(APIURL + '/api/modules/1/' + moduleId)
          .then(function () {
            $scope.messages.push({
              type : 'success',
              title : 'Success',
              body : 'Module ' + module.application + ' added to update queue'
            });
          })
          .catch(function (response) {
            $scope.messages.push({
              type : 'warning',
              title : 'Error ' + response.status,
              body : response.data
            });
          });
      }
    };

    return;
  }

  ModulesCtrl.$inject = ['$scope', '$q', '$http', 'userAuthFactory', 'WebSocketService', 'APIURL', '_UPDATE_MASTER_SERVER_URL'];
  angular.module('highToolClientApp')
    .controller('ModulesCtrl', ModulesCtrl);
}());
