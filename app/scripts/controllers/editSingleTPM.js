(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:CompleteOutputCtrl
   * @description
   * # CompleteoutputCtrl
   * Controller of the highToolClientApp
   */

  function editSingleTPMCtrl($scope, $http, $state, $stateParams, $rootScope, APIURL, packageService, dataStructuresService, d3, schema, packageData, spatialAndTemporalDimensions) {
    function restoreKeyDefauls() {
      d3.select(window).on('keydown', function () { return true; });
      d3.select(window).on('keyup', function () { return true; });
      return;
    }

    $scope.hideTime = function (schemaItem) {
      if (schemaItem.target && schemaItem.target.__parameters &&
        schemaItem.target.__parameters.length && spatialAndTemporalDimensions[schemaItem.target.__parameters[0]]) {
        return !spatialAndTemporalDimensions[schemaItem.target.__parameters[0]].temporal;
      }
      return false;
    };

    $scope.hideSpatial = function (schemaItem) {
      return (schemaItem.is_geo_active_n0 || schemaItem.is_geo_active_n2);
    };

    $scope.packageData = packageData;
    function getSubSchema(rootNode, originalSchema, values) {
      var retObj = {
        rootNodes: [{
          localId : 0,
          userId : originalSchema.nodeAttributes[rootNode].uuid
        }],
        adjList : [],
        nodeAttributes: [],
        uuidTrie : dataStructuresService.createTrie(),
        shortLabelTrie : dataStructuresService.createTrie()
      };

      function DFS(node, originalSchema, accum) {
        var i, ln,
        numOfNodes = accum.nodeAttributes.length;

        accum.uuidTrie.add(originalSchema.nodeAttributes[node].uuid, numOfNodes);
        accum.shortLabelTrie.add(originalSchema.nodeAttributes[node].shortLabel, numOfNodes);

        accum.adjList.push([]);
        accum.nodeAttributes.push(originalSchema.nodeAttributes[node]);

        for (i = 0, ln = originalSchema.adjList[node].length; i < ln; i += 1) {
          DFS(originalSchema.adjList[node][i], originalSchema, accum);
          accum.adjList[numOfNodes].push(accum.uuidTrie.get(originalSchema.nodeAttributes[originalSchema.adjList[node][i]].uuid));
        }

      }
      DFS(rootNode, originalSchema, retObj);

      if (values) {
        values.forEach(function (node) {
          retObj.nodeAttributes[retObj.shortLabelTrie.get(node.label)].values = JSON.parse(JSON.stringify(node.values));
        });
      }
      return retObj;
    }

    $scope.savePackage = function (e, isNew) {
      var sendData = {
        label : packageData.label,
        headline : packageData.label,
        abstract : packageData.abstract,
        rootNode : $scope.schema.nodeAttributes[0].uuid,
        nodeList : []
      };
      $scope.schema.adjList[0].forEach(function (localId) {
        var objToPush = {
          label : $scope.schema.nodeAttributes[localId].shortLabel,
          target : $scope.schema.nodeAttributes[localId].target,
          uuid : $scope.schema.nodeAttributes[localId].uuid,
          values : $scope.schema.nodeAttributes[localId].values
        };

        if (($scope.schema.nodeAttributes[localId].max === 0 || $scope.schema.nodeAttributes[localId].max) &&
        ($scope.schema.nodeAttributes[localId].min === 0 || $scope.schema.nodeAttributes[localId].min)) {
          objToPush.min = $scope.schema.nodeAttributes[localId].min;
          objToPush.max = $scope.schema.nodeAttributes[localId].max;
        }

        sendData.nodeList.push(objToPush);
      });

      var request = (isNew ? $http.post(APIURL + '/api/package', sendData) : $http.put(APIURL + '/api/package/' + $stateParams.packageId, sendData));

      request
      .then(function (response) {
        console.log(response.data);
        $state.go('authenticated.packageEditor.single_tpm', {packageId : response.data.id}, {replace : true});
      })
      .catch(function (err) {
        console.log(err);
      });
    };

    $rootScope.$on('savePackage', $scope.savePackage);


    $scope.schema = getSubSchema(schema.dictionary[packageData.rootNode], schema, packageData.nodeList);

    $scope.package = {
      values : $scope.schema.nodeAttributes.reduce(function (prev, curr) {
        if (curr.values) {
          prev.push(curr.values);
        } else {
          prev.push({});
        }
        return prev;
      }, []),
      subpackages : []
    };
    return;
  }

  editSingleTPMCtrl.$inject = ['$scope', '$http', '$state', '$stateParams', '$rootScope', 'APIURL', 'packageService', 'dataStructuresService', 'd3', 'schema', 'packageData', 'spatialAndTemporalDimensions'];

  angular.module('highToolClientApp')
    .controller('editSingleTPMCtrl', editSingleTPMCtrl);
}());
