(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name highToolClientApp.controller:runOptionsCtrl
   * @description
   * # Used in the scenario editor
   * Controller of the highToolClientApp
   */

  function singleTPMOption($scope, $uibModal, $state, $http, APIURL, packageService, packageData, schema, _TPM_PREFIX, spatialAndTemporalDimensions) {
    $scope.activeFamily = 0;
    $scope.activeFirstLevel = 0;
    $scope.schema = schema;
    $scope.showDescription = {};
    $scope.abstract = packageData.abstract;
    $scope.headline = packageData.headline;
    $scope.label = packageData.label;
    $scope.orderFunction = function (localId) {
      return schema.nodeAttributes[localId].label;
    };
    $scope.package = {
      values : [],
      subpackages : []
    };

    $scope.hideTime = function (schemaItem) {
      if (schemaItem.target && schemaItem.target.__parameters &&
        schemaItem.target.__parameters.length && spatialAndTemporalDimensions[schemaItem.target.__parameters[0]]) {
        return !spatialAndTemporalDimensions[schemaItem.target.__parameters[0]].temporal;
      }
      return false;
    };

    $scope.hideSpatial = function (schemaItem) {
      return (schemaItem.is_geo_active_n0 == 0 && schemaItem.is_geo_active_n2 == 0);
    };


    for (var i = 0, ln = packageData.nodeList.length; i < ln; i += 1) {
      $scope.package.values[packageService.trieUuid.get(packageData.nodeList[i].uuid)] = packageData.nodeList[i].values;
    }

    $scope.setActiveFamily = function (index) {
      $scope.activeFamily = parseInt(index, 10);
    };
    $scope.setPreviousActiveFamily = function () {
      $scope.activeFamily = ($scope.activeFamily + $scope.schema.adjList[0].length  - 1) % ($scope.schema.adjList[0].length);
    };
    $scope.setNextActiveFamily = function () {
      $scope.activeFamily = ($scope.activeFamily + 1) % ($scope.schema.adjList[0].length);
    };

    $scope.saveTPM = function (TPM) {
      var editor = $uibModal.open({
        templateUrl : '/views/partials/modalTPMsetOptions.tmpl.html',
        controller : 'modalTPMSetOptionsCtrl',
        size : 'lg',
        resolve : {
          title : function () {return 'Save new single TPM package'; },
          okButtonText : function () {return 'Save'; }
        }
      });
      editor.result
        .then(function (runOptions) {
          var affectedInputs = [];
          if(runOptions) {
            $scope.schema.adjList[TPM].forEach(function (localId) {
              var objToPush = {
                label : $scope.schema.nodeAttributes[localId].shortLabel,
                target : $scope.schema.nodeAttributes[localId].target,
                uuid : $scope.schema.nodeAttributes[localId].uuid,
                values : $scope.package.values[localId]
              };

              if (($scope.schema.nodeAttributes[localId].max === 0 || $scope.schema.nodeAttributes[localId].max) &&
              ($scope.schema.nodeAttributes[localId].min === 0 || $scope.schema.nodeAttributes[localId].min)) {
                objToPush.min = $scope.schema.nodeAttributes[localId].min;
                objToPush.max = $scope.schema.nodeAttributes[localId].max;
              }

              affectedInputs.push(objToPush);
            });
            $http.post(APIURL + '/api/package', {
              label : runOptions.label,
              headline : runOptions.headline,
              abstract : runOptions.abstract,
              rootNode : $scope.schema.nodeAttributes[TPM].uuid,
              nodeList : affectedInputs
            })
            .then(function (response) {
              $state.go('authenticated.packageEditor.single_tpm', {packageId : response.data.id}, {replace : true});
            })
            .catch(function (err) {
              console.log(err);
              throw new Error(err);
            });
          }
        });
    };

  }

  singleTPMOption.$inject = ['$scope', '$uibModal', '$state', '$http', 'APIURL', 'packageService', 'packageData', 'schema', '_TPM_PREFIX', 'spatialAndTemporalDimensions'];
  angular.module('highToolClientApp')
    .controller('singleTPMOption', singleTPMOption);
}());
