/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:Modald3timeCtrl
   * @description
   * # Used to draw trajectories
   * Controller of the highToolClientApp
   */

  function ModalAddServer($scope, $uibModalInstance) {

    $scope.newServer = {
      host : 'localhost',
      port : '80',
      description : ''
    };

    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    $scope.addServer = function () {
      $uibModalInstance.close($scope.newServer);
    };

    return;
  }

  ModalAddServer.$inject = ['$scope', '$uibModalInstance'];

  angular.module('highToolClientApp')
    .controller('ModalAddServer', ModalAddServer);
}());
