/*jslint browser: true, indent: 2*/
/*global angular, confirm, alert*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:usersAdministrationCtrl
   * @description
   * # Controls user administration
   * Controller of the highToolClientApp
   */
  function DBAdministrationCtrl($scope, $rootScope, $http, userAuthFactory, WebSocketService, NotificationAPIService, APIURL) {
    $scope.userSchemas = [];
    $http.get(APIURL + '/api/dataStock/mySchemata')
      .success(function (data) {
        $scope.userSchemas = data;
      });

    // Listen
    function updateDBStatus(obj) {
      console.log(obj);
      var  i, ln;
      NotificationAPIService.sendNotification('Database (' + obj.id + '): ' + obj.data.alias + ' has been updated.');
      for (i = 0, ln = $scope.userSchemas.length; i < ln; i += 1) {
        console.log(($scope.userSchemas[i].id === obj.id));
        if ($scope.userSchemas[i].id === obj.id) {
          $scope.userSchemas[i].status = obj.data.status;
          $scope.$apply();
          break;
        }
      }
    }

    WebSocketService.socket.on('htmodelruns', updateDBStatus);
    $rootScope.$on('$stateChangeStart', function () {
      WebSocketService.socket.off('htmodelruns', updateDBStatus);
    });


    $scope.addOwnSchema = function () {
      if ($scope.userSchemas.length < $scope.userQuota) {
        $scope.userSchemas.push({
          id: 'new',
          alias: 'New schema',
          status: 'Not created'
        });
      } else {
        alert('You cannot create additional Schemas. Contact the administrator to increase your quota.');
      }
    };
    $scope.createSchema = function (index) {
      WebSocketService.post('/api/dataStock/mySchemata', {
        alias: $scope.userSchemas[index].alias
      })
        .then(function (response) {
          $scope.userSchemas[index].id = response.data.id;
        });
    };
    $scope.deleteSchema = function (index) {
      if ($scope.userSchemas[index].id === 'new') {
        $scope.userSchemas.splice(index, 1);
      } else {
        if (confirm('The whole schema will be deleted.\n')) {
          $http.delete(APIURL + '/api/dataStock/schema/' + $scope.userSchemas[index].id)
            .then(function () {
              $scope.userSchemas.splice(index, 1);
            })
            .catch(function (err) {
              console.log(err);
            });
        }
      }
    };
    $scope.edited = function (index, schemaGroup) {
      $scope[schemaGroup][index].edited = true;
    };
    $scope.updateSchema = function (index) {
      $http.put(APIURL + '/api/dataStock/schema/' + $scope.userSchemas[index].id, {
        alias: $scope.userSchemas[index].alias
      })
        .catch(function (err) {
          console.log(err);
        });
    };


    $scope.userId = userAuthFactory.getClaims().id;
    $http.get(APIURL + '/api/user/' + $scope.userId + '/schemaQuota')
      .success(function (data) {
        $scope.userQuota = data.schemaQuota;
      });
    $scope.isAdmin = userAuthFactory.getClaims().isAdmin;

    return;
  }

  DBAdministrationCtrl.$inject = ['$scope', '$rootScope', '$http', 'userAuthFactory', 'WebSocketService', 'NotificationAPIService', 'APIURL'];
  angular.module('highToolClientApp')
    .controller('DBAdministrationCtrl', DBAdministrationCtrl);
}());
