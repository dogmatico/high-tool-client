/*jslint browser: true, indent: 2*/
/*global angular, alert, confirm*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:userNavCtrl
   * @description
   * # Used in the scenario editor
   * Controller of the highToolClientApp
   */

  function userNavController($scope, $rootScope, $uibModal, $stateParams, $state, $http, packageService, userAuthFactory, APIURL, VERSION_NUMBER) {
    $scope.authenticated = function () {
      return userAuthFactory.isAuthenticated();
    };

    $scope.todo = function () {
      alert('TODO');
    };

    $scope.VERSION_NUMBER = VERSION_NUMBER;
    $scope.state = $state;
    $scope.deletePackage = function (packageType) {
      var modalLoad = $uibModal.open({
        templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
        size: 'lg',
        controller: 'ModalLoadPackageCtrl',
        resolve : {
          packageList : function () {return packageService.listPackages(packageType, true); },
          title : function () {return 'Select the package to delete'; },
          options : function () { return null; }
        }
      });

      modalLoad.result.then(function (packageData) {
        if (packageData && confirm('Are you sure that you want to delete the selected item?')) {
          packageService.deletePackage(packageData.id)
            .then(function () {
              if ($stateParams.packageId && $stateParams.packageId === packageData.id) {
                $state.go('authenticated.main');
              }
            })
            .catch(function (err) {
              alert('Error: ' + err.message);
            });
        }
      });
    };
    $scope.isNewPackage = function () {
      return ($stateParams.packageId === 'new');
    };
    $scope.loadScenario = function () {
      var modalLoad = $uibModal.open({
        templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
        size: 'lg',
        scope: $scope,
        controller: 'ModalLoadPackageCtrl',
        resolve : {
          packageList : function () {return packageService.listPackages('scenario'); },
          title : function () {return 'Load a scenario'; },
          options : function () { return null; }
        }
      });

      modalLoad.result.then(function (packageData) {
        $state.go('authenticated.scenario', {packageId: packageData.id});
      });
    };

    $scope.readyForExpertMode = function () {
      var modalLoad = $uibModal.open({
        templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
        size: 'lg',
        scope: $scope,
        controller: 'ModalLoadPackageCtrl',
        resolve : {
          packageList : function () { return packageService.getReadyForExpertMode(); },
          title : function () { return 'Select the run that you want to edit it\'s associated Data Stock'; },
          options : function () { return null; }
        }
      });

      modalLoad.result.then(function (packageData) {
        $state.go('authenticated.run.expertMode', {runId: packageData.id});
      });
    };

    $scope.runPackage = function (packageType) {
      var modalLoad = $uibModal.open({
        templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
        size: 'lg',
        scope: $scope,
        controller: 'ModalLoadPackageCtrl',
        resolve : {
          packageList : function () { return packageService.listPackages(packageType); },
          title : function () { return 'Select the element to add to the run queue'; },
          options : function () {
            return [{
              id : 'expertMode',
              label : 'Expert Mode',
              value : false
            }];
          }
        }
      });

      // Modal returns [packageData, options]
      modalLoad.result.then(function (result) {
        $http.post(APIURL + '/api/run/' + packageType + '/' + result[0].id, {
          options : result[1]
          })
          .then(function () {
            $state.go('authenticated.modelRuns');
          })
          .catch(function (err) {
            console.error(err);
            alert(err);
          });
      });
    };

    $scope.loadOutput = function () {
      var modalLoad = $uibModal.open({
        templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
        size: 'lg',
        controller: 'ModalLoadPackageCtrl',
        resolve : {
          packageList : function () { return packageService.fetchDoneRun(); },
          title : function () { return 'Select the output'; },
          options : function () { return null; }
        }
      });

      modalLoad.result.then(function (runData) {
        if (runData) {
          $state.go('authenticated.output', {runId : runData.id});
        }
      });
    };

    $scope.loadPackage = function (packageType) {
        var modalLoad = $uibModal.open({
          templateUrl: '/views/partials/modalLoadPackage.tmpl.html',
          size: 'lg',
          controller: 'ModalLoadPackageCtrl',
          resolve : {
            packageList : function () { return packageService.listPackages(packageType); },
            title : function () { return 'Select a package'; },
            options : function () { return null; }
          }
        });

        modalLoad.result.then(function (packageData) {
          if (packageData) {
            $state.go('authenticated.packageEditor.' + packageData.type.toLowerCase().replace(/\s/g, '_'), {packageId : packageData.id});
          }
        });
    };

    $scope.logout = userAuthFactory.logout;
    $scope.$on('authSuccess', function () {
      $scope.userClaims = userAuthFactory.getClaims();
    });

    $scope.package = packageService.packageData;

    $scope.saveNewPackage = function () {
      $rootScope.$broadcast('savePackage', true);
    };

    $scope.toDoAlert = function () {
      alert('This option is not yet available.');
      return;
    };

    $scope.updatePackage = function () {
      $rootScope.$broadcast('savePackage', false);
    };

    $scope.userClaims = userAuthFactory.getClaims();

    return;
  }

  userNavController.$inject = ['$scope', '$rootScope', '$uibModal', '$stateParams', '$state', '$http', 'packageService', 'userAuthFactory', 'APIURL', 'VERSION_NUMBER'];

  angular.module('highToolClientApp')
    .controller('userNavCtrl', userNavController);
}());
