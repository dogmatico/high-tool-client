/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:loginCtrl
   * @description
   * # Controls Login Form
   * Controller of the highToolClientApp
   */
  function loginCtrl($scope, $state, userAuthFactory) {
    $scope.alerts = [];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };
    $scope.email = '';
    $scope.submitLogin = function () {
      $scope.alerts.splice(0, $scope.alerts.length);
      console.log($scope.email);
      userAuthFactory.login($scope.email, $scope.password)
        .then(function (data) {
          if (data.success === true) {
            $state.go('authenticated.instructions');
          } else {
            $scope.alerts.push('Login failed: ' + data.message);
          }
          $scope.$apply();
        })
        .catch(function (err) {
          $scope.alerts.push('Login failed: ' + err);
          $scope.$apply();
        });
    };
    $scope.password = '';
    return;
  }

  loginCtrl.$inject = ['$scope', '$state', 'userAuthFactory'];
  angular.module('highToolClientApp')
    .controller('loginCtrl', loginCtrl);
}());
