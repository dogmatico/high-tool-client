/*jslint browser: true, indent: 2*/
/*global angular*/
(function () {
  'use strict';
  /**
   * @ngdoc function
   * @name highToolClientApp.controller:modalTPMSetOptionsCtrl
   * @description
   * # ModalLoadPackageCtrl
   * Controller of the highToolClientApp
   */

  function modalTPMSetOptionsCtrl($scope, $uibModalInstance, title, okButtonText) {
    $scope.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
    $scope.ok = function ok() {
      $uibModalInstance.close($scope.runOptions);
    };
    $scope.runOptions = {
      label: '',
      abstract : '',
      expertMode : false
    };

    $scope.title = title;
    $scope.okButtonText = okButtonText;

    return;
  }

  modalTPMSetOptionsCtrl.$inject = ['$scope', '$uibModalInstance', 'title', 'okButtonText'];

  angular.module('highToolClientApp')
    .controller('modalTPMSetOptionsCtrl', modalTPMSetOptionsCtrl);
}());
