/*jslint browser: true, indent: 2*/
/*global angular, confirm*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:usersAdministrationCtrl
   * @description
   * # Controls user administration
   * Controller of the highToolClientApp
   */
  function ModelRunsCtrl($scope, $http, userAuthFactory, APIURL, WebSocketService, _MODEL_RUN_STATUS_CODES) {
    $scope.isAdmin = userAuthFactory.getClaims().isAdmin;

    $scope.userId = userAuthFactory.getClaims().id;

    $scope.pauseResume = function (pauseOrResume, runId, index, list) {
      var apiEndPoint = APIURL + '/api/run/' + pauseOrResume + '/' + runId;
      $http.post(apiEndPoint)
        .then(function (response) {
          response.data.step = {
            id : response.data.step,
            description : response.data.step
          };

          list[index] = response.data;

        })
        .catch(function (err) {
          console.log(err);
        });
    };


    $scope.deleteRun = function(index, schema) {
      if (confirm('Are you sure that you want to delete this run?')) {
        $http.delete(APIURL + '/api/run/' + schema.id)
          .then(function () {
            schema.markedDeletion = true;
          })
          .catch(function (err) {
            console.log(err);
          });
        }
    };
    $scope.cancelDelete = function(index, schema) {
      $http.post(APIURL + '/api/run/cancelDelete/' + schema.id)
        .then(function () {
          schema.markedDeletion = false;
        })
        .catch(function (err) {
          console.log(err);
        });
    };

    $http.get(APIURL + '/api/run/')
      .then(function (response) {
          function matchIdOwner(item) {
            return $scope.userId === item.owner.id;
          }
          $scope.userSchemas = response.data
            .filter(matchIdOwner)
            .map(function (item) {
              item.step = {
                id : item.step,
                description : item.step
              };
              return item;
            });
          $scope.otherUserSchemas = response.data
            .filter(function (item) {return !matchIdOwner(item);
            })
            .map(function (item) {
              item.step = {
                id : item.step,
                description : item.step
              };
              return item;
            });
      })
      .catch(function (err) {
        console.log(err);
      });

    $http.get(APIURL + '/api/run/steps')
      .then(function (response) {
        $scope.steps = response.data.map(function (step) {
          return {
            id : step,
            description : step
          };
        });
        $scope.stepsDict = response.data.reduce(function (acum, step, index) {
          acum[step] = index;
          return acum;
        }, {});
      })
      .catch(function (err) {
        console.log(err);
      });

    $scope.changeStep = function (runId, stepId, index, owned) {
      $http.put(APIURL + '/api/run/' + runId, {
        step : stepId
      })
        .then(function (response) {
          response.data.step = {
            id : response.data.step,
            description : response.data.step
          };

          if (owned) {
            $scope.userSchemas[index] = response.data.step;
          } else {
            $scope.otherUserSchemas[index] = response.data.step;
          }
        })
        .catch(function (err) {
          console.log(err);
        });
    };

    function updatedDataStock(msg) {
      msg.runInfo.step = {
        id : msg.runInfo.step,
        description : msg.runInfo.step
      };
      msg.runInfo.status = _MODEL_RUN_STATUS_CODES(msg.runInfo.status);

      var mapFunction = function (item) {
        return (item.id == msg.runInfo.id ? msg.runInfo : item);
      };

      $scope.userSchemas = $scope.userSchemas.map(mapFunction);
      $scope.otherUserSchemas = $scope.otherUserSchemas.map(mapFunction);

      if (msg.runInfo.step === 'NEW') {
        if (msg.runInfo.step.owner === $scope.userId) {
          $scope.userSchemas.push(msg.runInfo.step);
        } else {
          $scope.otherUserSchemas.push(msg.runInfo.step);
        }
      }

      $scope.$apply();
    }

    WebSocketService.socket.on('datastock-update', updatedDataStock);
    $scope.$on('$destroy', function () {
      WebSocketService.socket.off('datastock-update', updatedDataStock);
    });
  }

  ModelRunsCtrl.$inject = ['$scope', '$http', 'userAuthFactory', 'APIURL', 'WebSocketService', '_MODEL_RUN_STATUS_CODES'];
  angular.module('highToolClientApp')
    .controller('ModelRunsCtrl', ModelRunsCtrl);
}());
