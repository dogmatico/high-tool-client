(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:CompleteOutputCtrl
   * @description
   * # CompleteoutputCtrl
   * Controller of the highToolClientApp
   */

   function CombinedTPMCtrl($scope, $state, $stateParams, $uibModal, BaselineDataService, PackageService, CombinedTPMService, _TPM_PREFIX, leverList, schema, combinations, packageData, spatialAndTemporalDimensions) {

     $scope.packageId = $stateParams.packageId;
     $scope.packageLabel = packageData.label || 'No label added';
     $scope.schema = schema;
     $scope.selectedTPMList = [];
     $scope.allNoSelected = true;
     $scope.selectedTPMs = schema.adjList[0].reduce(function (acum) {
       acum.push(false);
       return acum;
     }, []);

     $scope.hideTime = function (schemaItem) {
       if (schemaItem.target && schemaItem.target.__parameters &&
         schemaItem.target.__parameters.length && spatialAndTemporalDimensions[schemaItem.target.__parameters[0]]) {
         return !spatialAndTemporalDimensions[schemaItem.target.__parameters[0]].temporal;
       }
       return false;
     };

     $scope.hideSpatial = function (schemaItem) {
       return (schemaItem.is_geo_active_n0 == 0 && schemaItem.is_geo_active_n2 == 0);
     };

     $scope.savePackage = function (isNew) {
       var editor = $uibModal.open({
         templateUrl: '/views/partials/modalSaveNewPackage.tmpl.html',
         scope: $scope,
         controller: ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
           $scope.cancel = function () {
             $uibModalInstance.dismiss('cancel');
           };
           $scope.modalTitle = (isNew ? 'Set package data' : 'Update package metadata');
           $scope.ok = function () {
             $uibModalInstance.close({
               label : $scope.label,
               headline : $scope.headline,
               abstract : $scope.abstract
             });
           };
         }]
       });

       editor.result.then(function (packageMetadata) {
         packageMetadata.data = {
           selectedTPM : $scope.selectedTPMList.map(function (localIndex) {
             return schema.nodeAttributes[localIndex].uuid.substr(_TPM_PREFIX.length);
           }),
           values : $scope.minimalSchema.reduce(function (acum, lever) {
             acum[lever.leverId] = lever.values;
             return acum;
           }, {})
         };

         PackageService.savePackage((!isNew && $stateParams.packageId ? $stateParams.packageId : null), 'combined', packageMetadata)
          .then(function (newData) {
            $state.go('.', {packageId : newData.id});
          });
       });
     };


     $scope.updatedValue = function (nodeIndex, useNode, recoverSavedData) {
       if (useNode) {
         $scope.selectedTPMList.push(nodeIndex);
       } else {
         var i, ln;
         for (i = 0, ln = $scope.selectedTPMList.length; i < ln && $scope.selectedTPMList[i] !== nodeIndex; i += 1) {
           // Empty block;
         }
         if (i < ln) {
           $scope.selectedTPMList.splice(i, 1);
         }
       }

       CombinedTPMService.createMinimalSchemaFromRun({
         TPM : $scope.selectedTPMList.map(function (localIndex) {
           return schema.nodeAttributes[localIndex].uuid.substr(_TPM_PREFIX.length);
         }),
         parameters : []
       }).then(function (minimalSchema) {
         $scope.minimalSchema = minimalSchema.adjList[2].map(function (index) {
           if (recoverSavedData && packageData && packageData.data) {
             minimalSchema.nodeAttributes[index].values = packageData.data.values[minimalSchema.nodeAttributes[index].leverId];
           }
           return minimalSchema.nodeAttributes[index];
         });
       });
     };

     if (packageData && packageData.data) {
       for (var i = 0, j = 0, nTPM = $scope.schema.adjList[0].length, nSelTPM = packageData.data.selectedTPM.length; i < nTPM && j < nSelTPM; i += 1) {
         if ($scope.schema.adjList[0][i] === $scope.schema.dictionary[_TPM_PREFIX + packageData.data.selectedTPM[j]]) {
           j += 1;
           $scope.selectedTPMs[i] = true;
           $scope.updatedValue($scope.schema.adjList[0][i], true, true);
         }
       }
     }
   }

   CombinedTPMCtrl.$inject = ['$scope', '$state', '$stateParams', '$uibModal', 'BaselineDataService', 'PackageService', 'CombinedTPMService', '_TPM_PREFIX', 'leverList', 'schema', 'combinations', 'packageData', 'spatialAndTemporalDimensions'];

   angular.module('highToolClientApp')
     .controller('CombinedTPMCtrl', CombinedTPMCtrl);
}());
