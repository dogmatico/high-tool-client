(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:ViewReferenceCtrl
   * @description
   * # ViewReferenceCtrl
   * Controller of the highToolClientApp
   */

  function ViewReferenceCtrl($scope, $http, APIURL) {
    // Demography results
    $scope.toggleViewEuropopAss = [];
    $scope.toggleViewEuropopRef = [];
    $scope.ageCohortsGroups = ['0-15', '16-64', '+65', 'Total'];


    $http({
        method: 'GET',
        url: APIURL + '/api/assumptions/eureference/DEM',
        cache: true
      })
      .then(function (response) {
        $scope.DEMAssumptions = response.data.data;
        $scope.DEMParams = response.data.parameters.reduce(function (acum, curr) {
          acum[curr.id] = curr;
          return acum;
        }, {});
      })
      .catch(function (response) {
        console.log(response);
      });

    $http({
        method: 'GET',
        url: APIURL + '/api/assumptions/eureference/ECR',
        cache: true
      })
      .then(function (response) {
        $scope.ECRAssumptions = response.data;
      })
      .catch(function (response) {
        console.log(response);
      });
    return;
  }

  ViewReferenceCtrl.$inject = ['$scope', '$http', 'APIURL'];

  angular.module('highToolClientApp')
    .controller('ViewReferenceCtrl', ViewReferenceCtrl);
}());
