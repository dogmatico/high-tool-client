(function () {
  'use strict';

  function ownDataCtrl($scope, $http, APIURL, userAuthFactory) {
    $scope.userId = userAuthFactory.getClaims().id;

    $scope.alerts = [];
    $scope.closeAlert = function(index) {
     $scope.alerts.splice(index, 1);
   };

    $scope.editData = function () {
      $http.put(APIURL + '/api/user/' + $scope.userId + '/data', {
        name : $scope.user.name,
        email : $scope.user.email,
      })
      .then(function () {
        $scope.alerts.push({type : 'success', msg :'Data updated.'});
      })
      .catch(function (response) {
        $scope.alerts.push({
          type : 'danger',
          msg : 'Error (' + response.status + '): ' + response.data
        });
      });
    };

    $scope.newPassword = function () {
      if ($scope.password1 !== $scope.password2) {
        $scope.alerts.push({type : 'danger', msg :'Passwords don\'t match.'});
      } else {
        $http.put(APIURL + '/api/user/' + $scope.userId + '/data', {
          password : $scope.password1
        })
        .then(function () {
          $scope.alerts.push({type : 'success', msg :'Password updated.'});
        })
        .catch(function (response) {
          $scope.alerts.push({
            type : 'danger',
            msg : 'Error (' + response.status + '): ' + response.data
          });
        });
      }
    };

    function checkPasswordValidity() {
      if ($scope.password1 !== $scope.password2) {
        document.getElementById('password').setCustomValidity('Passwords must match.');
      } else {
        document.getElementById('password').setCustomValidity('');
      }
    }

    document.getElementById('password').addEventListener('change', checkPasswordValidity, false);
    document.getElementById('confirmPassword').addEventListener('change', checkPasswordValidity, false);

    $http.get(APIURL + '/api/user/' + $scope.userId)
      .then(function (response) {
        $scope.user = {
          name : response.data.name,
          email : response.data.email
        };
      });
  }

  ownDataCtrl.$inject = ['$scope', '$http', 'APIURL', 'userAuthFactory'];

  angular.module('highToolClientApp')
    .controller('ownDataCtrl', ownDataCtrl);

}());
