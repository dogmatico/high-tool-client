(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:CompleteOutputCtrl
   * @description
   * # CompleteoutputCtrl
   * Controller of the highToolClientApp
   */

  function CompleteOutputCtrl($scope, $http, $q, $stateParams, $uibModal, APIURL, packageService, PackageService, CombinedTPMService, countryPriority, outputParameters, spatialAndTemporalDimensions) {
    $scope.runId = $stateParams.runId;
    function sortByCountryProperty(a, b) {
      var aPriority = countryPriority(a.country),
        bPriority = countryPriority(b.country);

      if (aPriority === bPriority) {
        return a.country - b.country;
      }
      return aPriority - bPriority;
    }

    function sortByCountryLiteral(a, b) {
      var aPriority = countryPriority(a),
        bPriority = countryPriority(b);

      if (aPriority === bPriority) {
        return a - b;
      }
      return aPriority - bPriority;
    }

    $scope.hideTime = function (schemaItem) {
      if (schemaItem.target && schemaItem.target.__parameters &&
        schemaItem.target.__parameters.length && spatialAndTemporalDimensions[schemaItem.target.__parameters[0]]) {
        return !spatialAndTemporalDimensions[schemaItem.target.__parameters[0]].temporal;
      }
      return false;
    };

    $scope.hideSpatial = function (schemaItem) {
      return (schemaItem.is_geo_active_n0 == 0 && schemaItem.is_geo_active_n2 == 0);
    };

    $scope.orderByLabel = function(localId) {
      return ($scope.schema && $scope.schema.nodeAttributes ? $scope.schema.nodeAttributes[localId].label : localId);
    };
    $scope.orderByLabelMin = function(localId) {
      return ($scope.minimalSchema && $scope.minimalSchema.nodeAttributes ? $scope.minimalSchema.nodeAttributes[localId].label : localId);
    };

    $scope.getKeys = function (obj, capitalizeFirst) {
      var retArray;
      if (obj) {
        retArray = Object.keys(obj);
        if (capitalizeFirst) {
          retArray =  retArray.map(function (key) {
            if (key === 'slight' || key === 'serious') {
              key += ' Injuries';
            }
            var keyArr = key.split('');
            if (keyArr && keyArr[0]) {
              keyArr[0] = keyArr[0].toUpperCase();
            }
            return keyArr.join('');
          });
        }
      }
      return retArray;
    };

    $scope.orderCountryKeys = function (obj) {
      return Object.keys(obj).sort(sortByCountryLiteral);
    };

    $scope.isCombined = (outputParameters.runData.rootNode === 'COMBINEDTPMROOT');

    switch (outputParameters.runData.rootNode) {
      case 'COMBINEDTPMROOT':
        CombinedTPMService.createMinimalSchemaFromRun(outputParameters.runData)
          .then(function (minimalSchema) {
            $scope.minimalSchema = minimalSchema;
          });
        break;
      case 'CUSTOMTPMROOT':
        var parentsKeys = outputParameters.runData.parameters.reduce(function (acum, lever) {
          var parent = outputParameters.schema.nodeAttributes[outputParameters.schema.dictionary[lever.uuid]].parent;
          if (!acum[parent]) {
            acum[parent] = true;
          }
          return acum;
        }, {});

        Object.keys(parentsKeys).forEach(function (parentId) {
          PackageService.moveBranch(outputParameters.schema, parentId, null, 'uuid');
        });

        //Delete CUSTOMTPMROOT
        outputParameters.schema.rootNodes.splice(0, 1);
        $scope.schema = PackageService.mixSchemaAndData(outputParameters.schema, outputParameters.runData, 'uuid');

        // Unlink empty nodes
        $scope.schema.rootNodes.forEach(function (rootNode) {
          $scope.schema.adjList[rootNode.localId] = $scope.schema.adjList[rootNode.localId].filter(function (childrenId) {
            return !!$scope.schema.nodeAttributes[childrenId].values;
          });
        });

        break;
      default:
        $scope.schema = PackageService.mixSchemaAndData(outputParameters.schema, outputParameters.runData, 'uuid');
    }

    // Demography results
    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/DEM')
      .success(function (data) {
        var i, j, k, ln, ln2;
        // Population by Country
        $scope.firstYear = parseInt(data[0].years[0], 10);
        $scope.lastYear = parseInt(data[0].years[data[0].years.length - 1], 10);
        $scope.popNumberYears = data[0].years[0].length;
        ln = data[0].years.length;
        $scope.maxPopVariation = 0;
        data[0].data
          .sort(sortByCountryProperty)
          .unshift(data[0].data.reduce(function (a,b) {
            if (countryPriority(b.country) === 1) {
              for (i = 0; i < ln; i += 1) {
                a.population[i] += b.population[i];
                a.labour[i] += b.labour[i];
              }
            }
            return a;
          }, {
            country: 'EU28 + NO + CH',
            population : data[0].years.reduce(function (a) {
              a.push(0);
              return a;
            }, []),
            labour : data[0].years.reduce(function (a) {
              a.push(0);
              return a;
            }, [])
            }
          ));
        data[0].data.map(function (a) {
          a.relative = {
            population : Math.round(100000 * (Math.log(a.population[a.population.length - 2]) - Math.log(a.population[0])) / ($scope.lastYear - $scope.firstYear)) / 1000,
            labour : Math.round(100000 * (Math.log(a.labour[a.labour.length - 2]) - Math.log(a.labour[0])) / ($scope.lastYear - $scope.firstYear)) / 1000
          };
          $scope.maxPopVariation = Math.max(Math.abs(a.relative), $scope.maxPopVariation);
          return a;
        });
        $scope.countriesPopulationLabour = data[0].data;
        $scope.showCountriesPop = false;
        $scope.toggleCountriesPop = function () {
          $scope.showCountriesPop = !$scope.showCountriesPop;
        };

        // Population by Age Cohort
        // Reduce the number of groups to fit in the screen
        var joinFactor = 4,
          oldIntegerDivision = Math.floor(data[1].ageCohorts.length / joinFactor);
        $scope.ageCohortsGroups = [];
        for (i = 0, ln = oldIntegerDivision * joinFactor; i < ln; i += joinFactor) {
          if(i + joinFactor >= data[1].ageCohorts.length - 1) {
            $scope.ageCohortsGroups.push((i === data[1].ageCohorts.length - 1 ? '' : '=>') + data[1].ageCohorts[i].split('-')[0]);
          } else {
            $scope.ageCohortsGroups.push(data[1].ageCohorts[i].split('-')[0] + '-' + data[1].ageCohorts[i + joinFactor - 1].split('-')[1]);
          }
        }
        // Transform data to fit the new age cohorts.
        $scope.ageCohortsPopulation = [];
        data[1].data.forEach(function (currentYear) {
          $scope.ageCohortsPopulation.push({
            year : currentYear.year,
            population : $scope.ageCohortsGroups.reduce(function (a) {
              a.push(0);
              return a;
            }, [])
          });
          for (i = 0, k = 0, ln = $scope.ageCohortsGroups.length; i < ln; i += 1, k += 1) {
            for (j = i * joinFactor, ln2 = Math.min(currentYear.population.length, (i + 1) * joinFactor); j < ln2; j += 1) {
              $scope.ageCohortsPopulation[$scope.ageCohortsPopulation.length - 1].population[k] += currentYear.population[j];
            }
          }
        });

        // Population by gender
        $scope.byGenderPopulation = data[2];
      });

    // Economy and Resources results
    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/ECR')
      .success(function (data) {
        $scope.GVABySectorsAndContries = data[0].sort(sortByCountryProperty)
          .map(function (item) {
            var lastYear, firstYear;
            Object.keys(item.gva).forEach(function (key) {
              var keyToInt =  parseInt(key, 10);
              if (!lastYear || lastYear < keyToInt) {
                lastYear = keyToInt;
              }
              if (!firstYear || firstYear > keyToInt) {
                firstYear = keyToInt;
              }
            });
            if (lastYear === firstYear) {
              item.avgYear = 0;
            } else {
              item.avgYear = Math.log(item.gva[lastYear][3] / item.gva[firstYear][3]) / (lastYear - firstYear);
            }
            return item;
          });

        $scope.gvaAvrNUTS0 = $scope.GVABySectorsAndContries.reduce(function (prev, curr) {
          prev.push([
            curr.code,
            curr.avgYear * 100
          ]);
          return prev;
        }, []);

        $scope.GDPCapitaCountry = data[2].sort(sortByCountryProperty);
        $scope.GDPCapitaCountryFirstYear = data[2][0].values[0].year;
        $scope.GDPCapitaCountryLastYear = data[2][0].values[data[2][0].values.length - 1].year;

        $scope.gvaSectorsBarData = {
          tags: ['Primary', 'Secondary', 'Tertiary'],
          items: (function () {
            var retArr = [];
            Object.keys(data[1]).forEach(function (key) {
              var values = [], i;
              for (i = 0; i < 3; i += 1) {
                values[i] = 100 * (data[1][key][i] / data[1][key][3]);
              }
              retArr.push({
                label : key,
                values : values
              });
            });
            return retArr;
          }())
        };
        $scope.GVABySectors = data[1];
      });

    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/VES')
      .success(function (data) {
        var years;

        $scope.VESFuel = data[0];
        $scope.VESMode = data[1];

        if (data && data[0] && data[0][0]) {
          years = Object.keys(data[0][0].values).sort();
          $scope.VESFirstYear = years[0];
          $scope.VESLastYear = years[years.length - 1];

          $scope.VESKeysTable = {
            fuel : $scope.getKeys(data[0][0].values[$scope.VESLastYear]).filter(function (item) {
              return (item !== 'total');
            }),
            mode : $scope.getKeys(data[1][0].values[$scope.VESLastYear]).filter(function (item) {
              return (item !== 'total');
            })
          };
        }
      });

    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/PAD')
      .success(function (data) {
        function aggregateEU(prev, curr) {
          if (countryPriority(curr.country) === 1) {
            Object.keys(curr.values).forEach(function (year) {
              Object.keys(curr.values[year]).forEach(function (mode) {
                if (!prev.values[year]) {
                  prev.values[year] = {};
                }
                if (!prev.values[year][mode]) {
                  prev.values[year][mode] = 0;
                }
                prev.values[year][mode] += curr.values[year][mode];
              });
            });
          }
          return prev;
        }

        if(data && data[0]) {
          $scope.PADFirstYear = data[0][0].year;
          $scope.PADLastYear = data[0][data[0].length - 1].year;
        }

        [1, 3, 5].forEach(function (i) {
          data[i].push(data[i].reduce(aggregateEU, {
            country: 'EU28 + NO + CH',
            code : 'EU28',
            values : {}
          }));
          data[i].sort(sortByCountryProperty);
        });


        $scope.PADTables = {
          pkm : {
            EU28 : data[0],
            countries : data[1]
          },
          vkm : {
            EU28 : data[2],
            countries : data[3]
          },
          trips : {
            EU28 : data[4],
            countries : data[5]
          }
        };

        data[6].unshift({
          country: 'EU28 + NO + CH',
          code : 'EU28',
          values : (function () {
            var val = {};
            Object.keys($scope.PADTables.pkm.countries[0].values).forEach(function (year) {
              val[year] = {};
              Object.keys($scope.PADTables.pkm.countries[0].values[year]).forEach(function (mode) {
                if (mode !== 'total') {
                  val[year][mode] = $scope.PADTables.pkm.countries[0].values[year][mode] / $scope.PADTables.vkm.countries[0].values[year][mode];
                }
              });
            });
            return val;
          }())
        });
        $scope.PADTables.occupation = data[6].sort(sortByCountryProperty);

        Object.keys(data[7].data).forEach(function (mode) {
          data[7].data[mode].EU28 = [];
          Object.keys(data[7].data[mode])
            .filter(function (country) {
              return (countryPriority(country) === 1);
            })
            .forEach(function (country) {
              data[7].data[mode][country].forEach(function (row) {
                var index = (parseInt(row[0], 10) - 2010) / 5,
                  i;

                if (!data[7].data[mode].EU28[index]) {
                  data[7].data[mode].EU28[index] = [row[0]];
                  for (i = 0; i < data[7].bands.length; i += 1) {
                    data[7].data[mode].EU28[index].push(0);
                  }
                }

                for (i = 1; i <= data[7].bands.length; i += 1) {
                  data[7].data[mode].EU28[index][i] += row[i];
                }

              });
            });


        });

        $scope.PADTables.distanceBands = [{
          unit : data[7].unit,
          bands : data[7].bands,
          data : data[7].data
        }];

      });

    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/FRD')
      .then(function (response) {
        $scope.FREModes = ['Road', 'Rail', 'IWW', 'Short-Sea', 'Sea'];
        var FREKeys = $scope.FREModes.map(function (item) {
          return item.toLowerCase()
        });

        var countryData = response.data[0].data;
        var EU28Agg = response.data[1].data[0];

        function EU28ReduceFact(magnitude, keys) {
          this.magnitude = magnitude;
          this.keys = keys;
        }

        EU28ReduceFact.prototype.reduce = function (acum, curr, index, array) {
          var nextLine = [curr.year].concat(new Array(this.keys.length * 2).fill(0));

          curr.data.forEach(function (mode) {
            var insertionIndex = this.keys.indexOf(mode.mode);
            if (insertionIndex !== -1) {
              nextLine[2 * insertionIndex + 1] = mode[this.magnitude] / 1000000000;
              nextLine[2 * insertionIndex + 2] = mode[this.magnitude + '_share'];
            }
          }.bind(this));
          acum.push(nextLine);
          return acum;
        };

        var redEU28tkm = new EU28ReduceFact('tkm', FREKeys);
        $scope.FRDEU28tkm = EU28Agg.values.reduce(redEU28tkm.reduce.bind(redEU28tkm), []);

        function CountryReduceFact(magnitude, keys) {
          this.magnitude = magnitude;
          this.keys = keys;
        }

        CountryReduceFact.prototype.reduce = function (acum, curr, index, array) {
          var nextLine = [curr.country].concat(new Array(this.keys.length * 2).fill(0));

          var year2050 = curr.values.filter(function (yearEntry) {
            return (Number.parseInt(yearEntry.year, 10) === 2050);
          });
          year2050 = (year2050.length ? year2050[0].data : []);

          year2050.forEach(function (mode) {
            var insertionIndex = this.keys.indexOf(mode.mode);
            if (insertionIndex !== -1) {
              nextLine[2 * insertionIndex + 1] = mode[this.magnitude] / 1000000000;
              nextLine[2 * insertionIndex + 2] = mode[this.magnitude + '_share'];
            }
          }.bind(this));

          acum.push(nextLine);
          return acum;
        };

        $scope.FRDCountry = {};

        ['tkm', 'vkm'].forEach(function (magnitude) {
          var countryReducetkm = new CountryReduceFact(magnitude, FREKeys);
          $scope.FRDCountry[magnitude] = countryData.reduce(countryReducetkm.reduce.bind(countryReducetkm), []);
        });

        $scope.FRDCountryLoadFactor = countryData.reduce(function (acum, curr, index, array) {
          var nextLine = [curr.country].concat(new Array(FREKeys.length * 2).fill(0));

          var year2050 = curr.values.filter(function (yearEntry) {
            return (Number.parseInt(yearEntry.year, 10) === 2050);
          });
          year2050 = (year2050.length ? year2050[0].data : []);

          year2050.forEach(function (mode) {
            var insertionIndex = FREKeys.indexOf(mode.mode);
            if (insertionIndex !== -1) {
              nextLine[insertionIndex + 1] = mode.loadFactor;

            }
          }.bind(this));

          acum.push(nextLine);
          return acum;
        }, []);
      });


    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/SAF')
      .success(function (data) {
        var years;

        if (data && data.length) {
          data = data.map(function (item) {
            if (item.tableContents && item.tableContents.length) {
              years = Object.keys(data[0].tableContents[0].values).sort();
              item.lastYear = years[years.length - 1];
              item.firstYear = years[0];
            }
            return item;
          })
          .filter(function (item) {
            return item.tableContents.length > 1;
          });
        }
        $scope.SAFInjuryTables = data;
      });

    $http.get(APIURL + '/api/run/results/' + $stateParams.runId + '/ENV/emissions')
      .success(function (data) {
        var years;

        if (data && data[0] && data[0].values) {
          years = Object.keys(data[0].values).sort();
          $scope.ENVEmissionsFirstYear = years[0];
          $scope.ENVEmissionsLastYear = years[years.length - 1];
        }
        data.unshift(data.reduce(function (prev, curr) {
          if (countryPriority(curr.country) === 1) {
            Object.keys(curr.values).forEach(function (year) {
              if (!prev.values[year]) {
                prev.values[year] = {};
              }
              Object.keys(curr.values[year]).forEach(function (emission) {
                if (!prev.values[year][emission]) {
                  prev.values[year][emission] = 0;
                }
                prev.values[year][emission] += curr.values[year][emission];
              });
            });
          }
          return prev;
        }, {
          country: 'EU28 + NO + CH',
          code : 'EU28',
          values : {}
        }));
        $scope.ENVEmissions = data.sort(sortByCountryProperty);
      });

    $scope.downloading = false;
    $scope.downloadXLS = function () {
      $scope.downloading = true;
      $http({
          url: APIURL + '/api/run/results/' + $stateParams.runId + '/exportToXLS',
          method: 'GET',
          responseType: 'arraybuffer',
          headers: {
              'Accept': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
          }
      })
        .then(function(result){
          $scope.downloading = false;
          var blob = new Blob([result.data], {
              type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            }),
            url = URL.createObjectURL(blob),
            downloadClick = document.createElement('a');

            downloadClick.download = 'output_run_' + $stateParams.runId + '.xlsx';
            downloadClick.setAttribute('href', url);
            document.body.appendChild(downloadClick);
            downloadClick.click();
            downloadClick.parentNode.removeChild(downloadClick);
        })
        .catch(function(err){
          $scope.downloading = false;
          console.error(err);
        });

    };



    $scope.viewBars = function(data, title) {
      $uibModal.open({
        templateUrl : '/views/partials/modalD3ModalSplit.tmpl.html',
        controller : ['$scope', '$uibModalInstance', 'title', 'data', function ($scope, $uibModalInstance, title, data) {
          $scope.data = data;
          $scope.title = title;
          $scope.close = function () {
            $uibModalInstance.dismiss();
          };
          return;
        }],
        size : 'lg',
        scope : $scope,
        resolve: {
          title: function () { return title; },
          data: function () {console.log(data); return data; }
        }
      });
      return;
    };

  }

  CompleteOutputCtrl.$inject = ['$scope', '$http', '$q', '$stateParams', '$uibModal', 'APIURL', 'packageService', 'PackageService', 'CombinedTPMService', 'countryPriority', 'outputParameters', 'spatialAndTemporalDimensions'];

  angular.module('highToolClientApp')
    .controller('CompleteOutputCtrl', CompleteOutputCtrl);
}());
