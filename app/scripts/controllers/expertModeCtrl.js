(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:ExportTablesCtrl
   * @description
   * # ExportTablesCtrl
   * Controller of the highToolClientApp
   */

  function ExpertModeCtrl($scope, $stateParams, $state, $http, APIURL, TablesService, runInfo) {
    $scope.files = [];

    $scope.runInfo = runInfo;
    $scope.goToCompleteOutput = function () {
      $state.go('authenticated.output', {
        runId : runInfo.id
      });
    };

    $scope.togglePause = function () {
      var url = APIURL + '/api/run/' + ($scope.runInfo.paused ? 'resume' : 'pause') + '/' + runInfo.id;

      $http.post(url)
        .then(function () {
          $scope.runInfo.paused = !$scope.runInfo.paused;
          if (runInfo.step === 'EXPERTMODE') {
            $state.go('authenticated.modelRuns');
          }
        })
        .catch(function (err) {
          console.log(err);
        });
    };

    $scope.downloadTable = function (tableId) {
      TablesService.downloadTable($stateParams.runId, tableId, function (err, blob) {
        if (err) {
          console.log(err);
          return;
        }
        var url = URL.createObjectURL(blob);

        var downloadClick = document.createElement('a');
        downloadClick.download = tableId + '.csv';
        downloadClick.setAttribute('href', url);
        document.body.appendChild(downloadClick);
        downloadClick.click();
        downloadClick.parentNode.removeChild(downloadClick);
      });
    };

    TablesService.variables
      .then(function (variableList) {
        $scope.tables = [
          {
            name : 'All',
            variables : variableList.reduce(function (prev, curr) {
              return prev.concat(curr.variables);
            }, [])
          }
        ];
        $scope.tables = $scope.tables.concat(variableList);
      })
      .catch(function (err) {
        console.log(err);
      });

    $scope.closeUpload = function (fileList, index) {
      fileList.splice(index, 1);
    };


    $scope.uploadTable = function (fileList, index) {
      var fileData = fileList[index];
      fileData.status = 'uploading';
      TablesService.uploadTable(runInfo.id, fileData.name, fileData, function (err) {
        if (err) {
          fileData.status = 'error';
          console.log(err);
          fileData.error = err;
        } else {
          fileData.status = 'uploaded';
        }
      });
    };

    $scope.uploadAllTablesInQueue = function (fileList) {
      fileList.forEach(function (file, index, arr) {
        if (!file.error) {
          $scope.uploadTable(arr, index);
        }
      });
    };
  }

  ExpertModeCtrl.$inject = ['$scope', '$stateParams', '$state', '$http', 'APIURL', 'TablesService', 'runInfo'];

  angular.module('highToolClientApp')
    .controller('ExpertModeCtrl', ExpertModeCtrl);
}());
