/*jslint browser: true, indent: 2*/
/*global angular, confirm, alert*/
(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name highToolClientApp.controller:usersAdministrationCtrl
   * @description
   * # Controls user administration
   * Controller of the highToolClientApp
   */
  function usersAdministrationCtrl($scope, $state, $http, $uibModal, userAuthFactory, APIURL) {
    if (!userAuthFactory.isAdmin()) {
      $state.go('authenticated.main');
    }

    $scope.userId = userAuthFactory.getClaims().id;

    $http.get(APIURL + '/api/user')
      .success(function (data) {
        $scope.userList = data;
      });

    $scope.deleteUser = function (index) {
      if (confirm('Are you sure that you want to delete this user?\nId: ' + $scope.userList[index].id + '\nE-mail: ' + $scope.userList[index].email)) {
        $http.delete(APIURL + '/api/user/' + $scope.userList[index].id)
          .success(function () {
            $scope.userList.splice(index, 1);
          })
          .error(function (data) {
            alert(data.err);
          });
      }
    };

    $scope.addUser = function () {
      var editor =  $uibModal.open({
        templateUrl : '/views/partials/modalNewUser.tmpl.html',
        controller : ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.password = '';
          $scope.name = '';
          $scope.email = '';
          $scope.validatePassword = function (password) {
            if (password.length < 8) {
              return false;
            }
            return true;
          };
          $scope.ok = function () {
            if ($scope.validatePassword($scope.password) && ($scope.password === $scope.confirmPassword)) {
              $uibModalInstance.close({
                password : $scope.password,
                name : $scope.name,
                email : $scope.email
              });
            }
          };
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
        }],
        size : 'mg',
        scope : $scope
      });

      editor.result
        .then(function (newData) {
          $http.post(APIURL + '/api/user', newData)
            .success(function (data) {
              $scope.userList.push(data);
            })
            .catch(function (data) {
              alert(data.err);
            });
        });
    };

    $scope.changePassword = function (index) {
      var editor =  $uibModal.open({
        templateUrl : '/views/partials/modalNewPassword.tmpl.html',
        controller : ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
          $scope.password = '';
          $scope.validatePassword = function (password) {
            if (password.length < 8) {
              return false;
            }
            return true;
          };
          $scope.ok = function () {
            $uibModalInstance.close($scope.password);
          };
          $scope.cancel = function () {
            $uibModalInstance.dismiss();
          };
        }],
        size : 'sg',
        scope : $scope
      });

      editor.result
        .then(function (newPassword) {
          $http.put(APIURL + '/api/user/' + $scope.userList[index].id + '/data', {
            password : newPassword
          })
            .catch(function (data) {
              alert(data.err);
            });
        });
    };

    $scope.edited = function (index) {
      $scope.userList[index].edited = true;
    };

    $scope.setAdmin = function (index, value) {
      if ($scope.userId !== $scope.userList[index].id) {
        $http.put(APIURL + '/api/user/' + $scope.userList[index].id + '/setAdmin', {
          isAdmin : value
        })
          .success(function () {
            $scope.userList[index].isAdmin = value;
          })
          .catch(function (data) {
            alert(data.err);
          });
      }
    };

    $scope.updateUser = function (index) {
      $http.put(APIURL + '/api/user/' + $scope.userList[index].id + '/data', {
        name : $scope.userList[index].name,
        email : $scope.userList[index].email,
      })
        .then(function () {
          $scope.userList[index].edited = false;
        })
        .catch(function (data) {
          alert(data.err);
        });
    };
    return;
  }

  usersAdministrationCtrl.$inject = ['$scope', '$state', '$http', '$uibModal', 'userAuthFactory', 'APIURL'];
  angular.module('highToolClientApp')
    .controller('usersAdministrationCtrl', usersAdministrationCtrl);
}());
